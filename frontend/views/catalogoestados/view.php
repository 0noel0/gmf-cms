<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Catalogoestados */

$this->title = $model->idcatalogoestados;
$this->params['breadcrumbs'][] = ['label' => 'Catalogoestados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogoestados-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idcatalogoestados], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idcatalogoestados], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcatalogoestados',
            'nombre',
            'estado',
        ],
    ]) ?>

</div>
