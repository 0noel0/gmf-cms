<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Catalogoestados */

$this->title = 'Create Catalogoestados';
$this->params['breadcrumbs'][] = ['label' => 'Catalogoestados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogoestados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
