<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Catalogoestados */

$this->title = 'Update Catalogoestados: ' . $model->idcatalogoestados;
$this->params['breadcrumbs'][] = ['label' => 'Catalogoestados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcatalogoestados, 'url' => ['view', 'id' => $model->idcatalogoestados]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="catalogoestados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
