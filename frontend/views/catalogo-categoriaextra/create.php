<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoCategoriaextra */

$this->title = 'Create Catalogo Categoriaextra';
$this->params['breadcrumbs'][] = ['label' => 'Catalogo Categoriaextras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogo-categoriaextra-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
