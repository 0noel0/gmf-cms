<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoCategoriaextra */

$this->title = 'Update Catalogo Categoriaextra: ' . $model->idcatalogocategoriaextra;
$this->params['breadcrumbs'][] = ['label' => 'Catalogo Categoriaextras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcatalogocategoriaextra, 'url' => ['view', 'id' => $model->idcatalogocategoriaextra]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="catalogo-categoriaextra-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
