<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoCategoriaextra */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalogo-categoriaextra-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idrestaurante')->textInput() ?>

    <?= $form->field($model, 'requerido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'multiple')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
