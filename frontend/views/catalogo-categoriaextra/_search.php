<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoCategoriaextraSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalogo-categoriaextra-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idcatalogocategoriaextra') ?>

    <?= $form->field($model, 'idrestaurante') ?>

    <?= $form->field($model, 'requerido') ?>

    <?= $form->field($model, 'multiple') ?>

    <?= $form->field($model, 'nombre') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
