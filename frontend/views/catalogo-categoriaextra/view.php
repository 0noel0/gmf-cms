<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoCategoriaextra */

$this->title = $model->idcatalogocategoriaextra;
$this->params['breadcrumbs'][] = ['label' => 'Catalogo Categoriaextras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogo-categoriaextra-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idcatalogocategoriaextra], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idcatalogocategoriaextra], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcatalogocategoriaextra',
            'idrestaurante',
            'requerido',
            'multiple',
            'nombre',
            'estado',
        ],
    ]) ?>

</div>
