<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoExtra */

$this->title = 'Update Catalogo Extra: ' . $model->idcatalogo_extra;
$this->params['breadcrumbs'][] = ['label' => 'Catalogo Extras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcatalogo_extra, 'url' => ['view', 'id' => $model->idcatalogo_extra]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="catalogo-extra-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
