<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\OrdenUsuario */

$this->title = 'Create Orden Usuario';
$this->params['breadcrumbs'][] = ['label' => 'Orden Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orden-usuario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
