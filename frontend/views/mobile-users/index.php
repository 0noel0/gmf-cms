<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MobileUsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mobile Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mobile-users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mobile Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idmobileusr',
            'name',
            'last_name',
            'email:email',
            'password',
            // 'idcountry',
            // 'phone',
            // 'mobile',
            // 'creation',
            // 'pin_cp',
            // 'tokenSesion',
            // 'tokenPush:ntext',
            // 'primerRegistro',
            // 'idDispositivo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
