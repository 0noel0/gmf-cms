<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Sucursal */

$this->title = 'Update Sucursal: ' . $model->idsucursal;
$this->params['breadcrumbs'][] = ['label' => 'Sucursals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idsucursal, 'url' => ['view', 'id' => $model->idsucursal]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sucursal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
