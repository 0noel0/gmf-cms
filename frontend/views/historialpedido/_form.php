<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Historialpedido */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="historialpedido-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idordenUsuario')->textInput() ?>

    <?= $form->field($model, 'idusuario')->textInput() ?>

    <?= $form->field($model, 'idtipousuario')->textInput() ?>

    <?= $form->field($model, 'idestadoanterior')->textInput() ?>

    <?= $form->field($model, 'idestadonuevo')->textInput() ?>

    <?= $form->field($model, 'fechacreacion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
