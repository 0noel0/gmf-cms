<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\HistorialpedidoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="historialpedido-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idhistorialpedido') ?>

    <?= $form->field($model, 'idordenUsuario') ?>

    <?= $form->field($model, 'idusuario') ?>

    <?= $form->field($model, 'idtipousuario') ?>

    <?= $form->field($model, 'idestadoanterior') ?>

    <?php // echo $form->field($model, 'idestadonuevo') ?>

    <?php // echo $form->field($model, 'fechacreacion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
