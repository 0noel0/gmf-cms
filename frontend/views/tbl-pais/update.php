<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblPais */

$this->title = 'Update Tbl Pais: ' . $model->id_pais;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Pais', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pais, 'url' => ['view', 'id' => $model->id_pais]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-pais-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
