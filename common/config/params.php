<?php
return [
    'adminEmail' => 'noel@i-moves.com',
    'supportEmail' => 'noel@i-moves.com',
    'user.passwordResetTokenExpire' => 3600,
    'jwtExpire' => 86400*14,//14 date
    'jwtSecret' => "abc123",
    'fcm' => [
        'apiKey' => 'abc'
    ]
];
