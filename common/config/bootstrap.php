<?php
Yii::setAlias('@approot', dirname(dirname(__DIR__)));
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@uploadPath', '@frontend/web/uploads');

//Url para subir imagenes
Yii::setAlias('@uploadPathLogo', '@frontend/web/uploads/img/restaurants');
Yii::setAlias('@uploadPathFotografia', '@frontend/web/uploads/img/galleries');
Yii::setAlias('@uploadPathPlato', '@frontend/web/uploads/img/products');

//configurar URLBASE
Yii::setAlias('@web', 'http://dev.gmf.technology/gmf');
Yii::setAlias('@urlImage', '@web/frontend/web');
Yii::setAlias('uploadUrl', '@web/frontend/web/uploads');
