<?php
return [
    'components' => [
        //Db
        'db' => [
            'class' => 'yii\db\Connection',
            /*
            'dsn' => 'mysql:host=74.86.23.42;dbname=grupoava_mobile',
            'username' => 'grupoava_remoto',
            'password' => '1001.0110',
            */
            'dsn' => 'mysql:host=desarrollo.gmf.technology;dbname=grupoava_mobile',
            'username' => 'grupoava',
            'password' => 'gmf.10010110',
            
            'charset' => 'utf8',
        ],
        //Email
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',            
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'noreplythanhle@gmail.com',
                'password' => '!@#noreply!@#',
                'port' => '587',
                'encryption' => 'tls',
            ],
            'useFileTransport' => false,
        ],
        // Login Social Facebook
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '130855937379431',
                    'clientSecret' => '41a53c6af542ae93e4c680ee512e8cd6',
                    'title' => "Sign in using Facebook",
                ],
            // etc.
            ],
        ],
    ],
];
