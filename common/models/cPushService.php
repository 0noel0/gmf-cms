<?php
namespace common\models;

class cPushService {
   
    // get ios pushtoken
    function getAndroidPush(){
        try{

			$rs = (new \yii\db\Query())
               ->select(['tokenPush'])
               ->from('mobile_users')
               ->where(['idDispositivo' => 1])
               ->andWhere(['<>','tokenPush', ''])->all();

            
            if ( count($rs) > 0 ){
				$i = 0;
            	foreach ($rs as $value) {
            		$info[$i]	= $value['tokenPush'];
            		$i++;
            	}   
                
                return $info;
            } else {
                return false;
            }
        }catch(Exception $e){
            return false;
        }
    }

  
    function getIdAndroid(){
        try{

			$rs = (new \yii\db\Query())
               ->select(['idmobileusr'])
               ->from('mobile_users')
               ->where(['idDispositivo' => 1])
               ->all();

            if ( count($rs) > 0 ){
				$i =0;
            	foreach ($rs as $value) {
            		$info[$i]	= $value['idmobileusr'];
            		$i++;
            	}   
                
                return $info;
            } else {
                return false;
            }
        }catch(Exception $e){
            return false;
        }
    }
	
	function getIOSPush(){
        try{

			$rs = (new \yii\db\Query())
               ->select(['tokenPush'])
               ->from('mobile_users')
               ->where(['idDispositivo' => 1])
               ->andWhere(['<>','tokenPush', ''])->all();

            if ( count($rs) > 0 ){
				$i =0;
            	foreach ($rs as $value) {
            		$info[$i]	= $value['tokenPush'];
            		$i++;
            	}   
                
                return $info;
            } else {
                return false;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function getIdIOS(){
        try{

			$rs = (new \yii\db\Query())
               ->select(['idmobileusr'])
               ->from('mobile_users')
               ->where(['idDispositivo' => 2])
               ->all();

            if ( count($rs) > 0 ){
				$i =0;
            	foreach ($rs as $value) {
            		$info[$i]	= $value['idmobileusr'];
            		$i++;
            	}   
                
                return $info;
            } else {
                return false;
            }
        }catch(Exception $e){
            return false;
        }
    }

	//PUSH GENERAL - SPECIAL OFFERS
	function PushAndroidOffers($array,$titulo,$mensaje,$fecha,$idN){
        try{
            // Mensaje ya esta todo arriba
            $idkeyserver="AAAA1OW5Zak:APA91bFjQIKOsDPIJT5bWd6KqCjMxC3IC4a6N-SyYRXopS3K_she1NRZJjbGp35G6oAVHAYBzqYDXAL9Gi4OnIq04nUArFRytG1xBJ3fHkQu_9ccryGKZoArHQo7VwgZKhnmmFObdX23";
            // Set POST variables
            $url = "https://fcm.googleapis.com/fcm/send";
            //QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA
            /*$data->repetida = $repetida;
                    $data->tipo 	  = 5;
                    $data->idAlerta = $idAlerta;*/
            $fields = array(
                'registration_ids'  => $array,
                'data'              => array("titulo"=>$titulo, "mensaje"=>$mensaje, "tipo"=>1, "fecha"=>$fecha, "idNotificacion"=>$idN),
            );

            $headers = array(
                'Authorization: key=' . $idkeyserver, 
                'Content-Type: application/json'
            );

            // Open connection
            $ch = curl_init();

            if (FALSE === $ch)
                throw new Exception('failed to initialize');


            // Set the url, number of POST vars, POST data
            curl_setopt( $ch, CURLOPT_URL, $url );

            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            // Execute post
            $result = curl_exec($ch);
            //var_dump('RESULT!!!'.json_encode( $fields ));

           //var_dump($result);
            if (FALSE === $result)
                throw new Exception(curl_error($ch), curl_errno($ch));
            // Close connection
            curl_close($ch);
            return true;
        }catch(Exception $e){
            return false;
        }
    }

	function PushIOS($array,$titulo,$mensaje,$fecha){
        try{
			//var_dump($array);
			//var_dump($badge);
            // Mensaje ya esta todo arriba
            $idkeyserver="AAAA1OW5Zak:APA91bFjQIKOsDPIJT5bWd6KqCjMxC3IC4a6N-SyYRXopS3K_she1NRZJjbGp35G6oAVHAYBzqYDXAL9Gi4OnIq04nUArFRytG1xBJ3fHkQu_9ccryGKZoArHQo7VwgZKhnmmFObdX23";
            // Set POST variables
            $url = 'https://gcm-http.googleapis.com/gcm/send';
            //$array="ej6x2i_PGgs:APA91bHXJEDd31CiEMc5DHQUqVzLfqWJc5del6AlWjq8xeizDwE2AgidVtc7hhpRyCdA3w6jQQ_c1kjTyoCzujBdoLfR99fOOd_8yn-uPGvS2L03nu4aOTQz-DXQ9T4qXtSWiR4N-DAU";
            //$array = 'ej6x2i_PGgs:APA91bHXJEDd31CiEMc5DHQUqVzLfqWJc5del6AlWjq8xeizDwE2AgidVtc7hhpRyCdA3w6jQQ_c1kjTyoCzujBdoLfR99fOOd_8yn-uPGvS2L03nu4aOTQz-DXQ9T4qXtSWiR4N-DAU';
            //QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA

            //$array = 'mWa2XfdKxHU:APA91bFtu2bvCR2y9-RXhB0F-VSGdCdTmtEdAwKS2_EixvYAzN8V4LHzERo-EqaomJIrfiVXuMJIBna6buMiSigWk0gc9_03TGP6UgrXHpOfzwWBluvpXhNczYl40sQZhExE3S8DWgkl';

            $fields = (object)  array(
                //'registration_ids'  => ($array),
                //'data'              => array("titulo"=>"prueba Global", "mensaje" => "prueba Global", "tipo"=>2),
                'registration_ids' => array($array),
				'content_available' => false,
				'priority' =>  "high",
				'notification' => (object) array("body" =>$titulo, "title"=> $mensaje,  "tipo"=>1, "fecha"=>$fecha, "sound"=>"default"),
				//'data' => (object) array("idNotificacion"=>$idNotificacion)
            );

            $headers = array(
                'Authorization: key=' . $idkeyserver,
                'Content-Type: application/json'
            );

            //var_dump(json_encode($fields));
            // Open connection
            $ch = curl_init();

            if (FALSE === $ch)
                throw new Exception('failed to initialize');


            // Set the url, number of POST vars, POST data
            curl_setopt( $ch, CURLOPT_URL, $url );

            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            // Execute post
            $result = curl_exec($ch);
			
			//var_dump($result);
			
            if (FALSE === $result)
                throw new Exception(curl_error($ch), curl_errno($ch));
            // Close connection
            curl_close($ch);
            return $result;
        }catch(Exception $e){
            return false;
        }
    }
	
	function PushAndroidGlobal($array,$titulo,$mensaje,$fecha){
        try{
			//$array = "APA91bFH_imy4DOZX3_xBh9226rfZU9UhojHHYbHVVrmccJ3oKUgjVFS5qlkqGvyV5xQmh8cNzPwhrXp2ooDvpG0fPYNers0II2GBumC3hB44r6mcKazJCQGfp3dNjyWFNmPksLJf4e-";
            // key firebase
            $idkeyserver="AIzaSyBfdSltHg9k7w0ZumxP0ui-c22CReVUVmo";
            // Set POST variables
            $url = "https://fcm.googleapis.com/fcm/send";
			
           
            $fields = array(
                'registration_ids'  => $array,
                'data'              => array("titulo"=>$titulo, "mensaje"=>$mensaje, "tipo"=>1, "fecha"=>$fecha)
            );

            $headers = array(
                'Authorization: key=' . $idkeyserver, 
                'Content-Type: application/json'
            );

            // Open connection
            $ch = curl_init();

            if (FALSE === $ch)
                throw new Exception('failed to initialize');


            // Set the url, number of POST vars, POST data
            curl_setopt( $ch, CURLOPT_URL, $url );

            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            // Execute post
            $result = curl_exec($ch);
            //var_dump('RESULT!!!'.json_encode( $fields ));

           var_dump($result);
            if (FALSE === $result)
                throw new Exception(curl_error($ch), curl_errno($ch));
            // Close connection
            curl_close($ch);
            return true;
        }catch(Exception $e){
            return false;
        }
    }
	
	function PushIOSGlobal($array,$titulo,$mensaje,$fecha){
        try{
			//var_dump($array);
			//var_dump($badge);
            // Mensaje ya esta todo arriba
            $idkeyserver="AIzaSyBfdSltHg9k7w0ZumxP0ui-c22CReVUVmo";
            // Set POST variables
            $url = 'https://gcm-http.googleapis.com/gcm/send';
            //$array="ej6x2i_PGgs:APA91bHXJEDd31CiEMc5DHQUqVzLfqWJc5del6AlWjq8xeizDwE2AgidVtc7hhpRyCdA3w6jQQ_c1kjTyoCzujBdoLfR99fOOd_8yn-uPGvS2L03nu4aOTQz-DXQ9T4qXtSWiR4N-DAU";
            //$array = 'ej6x2i_PGgs:APA91bHXJEDd31CiEMc5DHQUqVzLfqWJc5del6AlWjq8xeizDwE2AgidVtc7hhpRyCdA3w6jQQ_c1kjTyoCzujBdoLfR99fOOd_8yn-uPGvS2L03nu4aOTQz-DXQ9T4qXtSWiR4N-DAU';
            //QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA

            //$array = 'mWa2XfdKxHU:APA91bFtu2bvCR2y9-RXhB0F-VSGdCdTmtEdAwKS2_EixvYAzN8V4LHzERo-EqaomJIrfiVXuMJIBna6buMiSigWk0gc9_03TGP6UgrXHpOfzwWBluvpXhNczYl40sQZhExE3S8DWgkl';

            $fields = (object)  array(
                //'registration_ids'  => ($array),
                //'data'              => array("titulo"=>"prueba Global", "mensaje" => "prueba Global", "tipo"=>2),
                'registration_ids' => array($array),
				'content_available' => false,
				'priority' =>  "high",
				'notification' => (object) array("body" =>$titulo, "title"=> $mensaje, "tipo"=>1, "fecha"=>$fecha, "sound"=>"default"),
				//'data' => (object) array("idNotificacion"=>$idNotificacion)
            );

            $headers = array(
                'Authorization: key=' . $idkeyserver,
                'Content-Type: application/json'
            );

            //var_dump(json_encode($fields));
            // Open connection
            $ch = curl_init();

            if (FALSE === $ch)
                throw new Exception('failed to initialize');


            // Set the url, number of POST vars, POST data
            curl_setopt( $ch, CURLOPT_URL, $url );

            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            // Execute post
            $result = curl_exec($ch);
			
			//var_dump($result);   
			
            if (FALSE === $result)
                throw new Exception(curl_error($ch), curl_errno($ch));
            // Close connection
            curl_close($ch);
            return $result;
        }catch(Exception $e){
            return false;
        }
    }

}
?>