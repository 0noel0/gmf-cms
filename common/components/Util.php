<?php
namespace common\components;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Util
 *
 * @author thanhlbk
 */
use Yii;
class Util {
    //put your code here
    public static function getUrlImage($image) {
        $link;
        if($image){
           $link = Yii::$app->urlManagerFrontend->createUrl('/uploads') . '/' . $image;
        }
        else{
            $link = Yii::$app->urlManagerFrontend->createUrl('/uploads') . '/' .'no-image.jpg';
        }
        return $link;
    }

    //url de logo 
    public static function getUrlLogo($image) {
        $link;
        if($image){
           $link = Yii::$app->urlManagerFrontend->createUrl('/uploads/img/restaurants') . '/' . $image;
        }
        else{
            $link = Yii::$app->urlManagerFrontend->createUrl('/uploads') . '/' .'no-image.jpg';
        }
        return $link;
    }
    //url de logo 
    public static function getUrlFotografia($image) {
        $link;
        if($image){
           $link = Yii::$app->urlManagerFrontend->createUrl('/uploads/img/galleries') . '/' . $image;
        }
        else{
            $link = Yii::$app->urlManagerFrontend->createUrl('/uploads') . '/' .'no-image.jpg';
        }
        return $link;
    }

    //url de plato 
    public static function getUrlPlato($image) {
        $link;
        if($image){
           $link = Yii::$app->urlManagerFrontend->createUrl('/uploads/img/products') . '/' . $image;
        }
        else{
            $link = Yii::$app->urlManagerFrontend->createUrl('/uploads') . '/' .'no-image.jpg';
        }
        return $link;
    }
    
    public static function uploadFile($file, $fileName) {
        if ($file) {
            $uploadPath = \Yii::getAlias('@uploadPath');            
            $file->saveAs(Yii::getAlias($uploadPath . '/' . $fileName));
            return true;
        }
        return false;
    }
    //upload logo
    public static function uploadLogo($file, $fileName) {
        if ($file) {
            $uploadPath = \Yii::getAlias('@uploadPathLogo');            
            $file->saveAs(Yii::getAlias($uploadPath . '/' . $fileName));
            return true;
        }
        return false;
    }

    //upload logo
    public static function uploadFotografia($file, $fileName) {
        if ($file) {
            $uploadPath = \Yii::getAlias('@uploadPathFotografia');            
            $file->saveAs(Yii::getAlias($uploadPath . '/' . $fileName));
            return true;
        }
        return false;
    }

     //upload plato
    public static function uploadPlato($file, $fileName) {
        if ($file) {
            $uploadPath = \Yii::getAlias('@uploadPathPlato');            
            $file->saveAs(Yii::getAlias($uploadPath . '/' . $fileName));
            return true;
        }
        return false;
    }


    public static function deleteFile($fileName) {
        $uploadPath = \Yii::getAlias('@uploadPath');  
        @unlink(Yii::getAlias($uploadPath . '/' . $fileName));
    }

    //Delete logo
    public static function deleteLogo($fileName) {
        $uploadPath = \Yii::getAlias('@uploadPathLogo');  
        @unlink(Yii::getAlias($uploadPath . '/' . $fileName));
    }
    //Delete Fotografia
    public static function deleteFotografia($fileName) {
        $uploadPath = \Yii::getAlias('@uploadPathFotografia');  
        @unlink(Yii::getAlias($uploadPath . '/' . $fileName));
    }
    //Delete Plato
    public static function deletePlato($fileName) {
        $uploadPath = \Yii::getAlias('@uploadPathPlato');  
        @unlink(Yii::getAlias($uploadPath . '/' . $fileName));
    }
    
}
