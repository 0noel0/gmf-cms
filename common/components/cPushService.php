<?php
namespace common\components;


use Yii;
class PushService 
{
    //Constructor
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
    // get ios pushtoken
    function getAndroidPush(){
        try{
            $sql 	= "SELECT u.tokenPush FROM mobile_users AS u WHERE idDispositivo=1 AND u.tokenPush <>'';";
            $i = 0;
            $rs		= $this->DATA->Execute($sql);
            if ( $rs->RecordCount() > 0 ){
                while ( !$rs->EOF ) {
                    $info[$i]		= $rs->fields['pushToken'];
                    $i++;
                    $rs->MoveNext();
                }
                $rs->Close();
                return $info;
            } else {
                return false;
            }
        }catch(Exception $e){
            return false;
        }
    }
	
	function getIdAndroid(){
        try{
            $sql 	= "SELECT u.idmobileusr FROM mobile_users AS u
			          WHERE dispositivo=1;";
            $i = 0;
            $rs		= $this->DATA->Execute($sql);
            if ( $rs->RecordCount() > 0 ){
                while ( !$rs->EOF ) {
                    $info[$i]		= $rs->fields['idmobileusr'];
                    $i++;
                    $rs->MoveNext();
                }
                $rs->Close();
                return $info;
            } else {
                return false;
            }
        }catch(Exception $e){
            return false;
        }
    }
	
	function getIOSPush(){
        try{
            $sql 	= "SELECT u.tokenPush FROM mobile_users AS u WHERE idDispositivo=2 AND u.tokenPush <>'';";
            $i = 0;
            $rs		= $this->DATA->Execute($sql);
            if ( $rs->RecordCount() > 0 ){
                while ( !$rs->EOF ) {
                    $info[$i]		= $rs->fields['pushToken'];
                    $i++;
                    $rs->MoveNext();
                }
                $rs->Close();
                return $info;
            } else {
                return false;
            }
        }catch(Exception $e){
            return false;
        }
    }

   
    function nuevoDetalle($params)
    {
        $sql = "INSERT INTO notificacion_usuario_atencion (id_atencion,id_usuario_web,id_notificacion) VALUES (?,?,?);";

        $save = $this->DATA->Execute($sql, $params);
        if ($save){
            return true;
        } else {
            return false;
        }
    }
    function nuevaNotificacion($params)
    {
        $sql = "INSERT INTO notificacion (titulo,descripcion,estadoPush) VALUES (?,?,?);";

        $save = $this->DATA->Execute($sql, $params);
        $id   = $this->DATA->Insert_ID();
        if ($save){
            return $id;
        } else {
            return false;
        }
    }

    function notificacion($params)
    {
        $sql = "INSERT INTO notificacion (titulo,mensaje) VALUES (?,?);";

        $save = $this->DATA->Execute($sql, $params);
        //$id   = $this->DATA->Insert_ID();
        if ($save){
            return true;
        } else {
            return false;
        }
    }

 	
	function getAndroidPushId($idUser){
        try{
            $sql 	= "SELECT u.push_token FROM usuario_app AS u WHERE idusuario_app=? AND dispositivo=1;";
            $i = 0;
            $rs		= $this->DATA->Execute($sql,$idUser);
            if ( $rs->RecordCount() > 0 ){
                while ( !$rs->EOF ) {
                    $info[$i]		= $rs->fields['push_token'];
                    $i++;
                    $rs->MoveNext();
                }
                $rs->Close();
                return $info;
            } else {
                return false;
            }
        }catch(Exception $e){
            return false;
        }
    }
	
	
	//PUSH GENERAL - SPECIAL OFFERS
	function PushAndroidOffers($array,$titulo,$mensaje,$fecha){
        try{
            // Mensaje ya esta todo arriba
            $idkeyserver="AAAA4bh4Px8:APA91bHzTbG331sRIc6Z8Ilcm-7NNfqrb5aU2FQYoaEMLHmh2kCNwdNGcUmWD7XDIpA6AHXVi-dvXo6P-Lm5aOV6_SNM2-TNzrRp6KKyKPT9qsMeqmj8hoIB0vuV05Iyap2TR5sPpWWK";
            // Set POST variables
            $url = "https://fcm.googleapis.com/fcm/send";
            //QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA
            /*$data->repetida = $repetida;
                    $data->tipo 	  = 5;
                    $data->idAlerta = $idAlerta;*/
            $fields = array(
                'registration_ids'  => $array,
                'data'              => array("titulo"=>$titulo, "mensaje"=>$mensaje, "tipo"=>1, "fecha"=>$fecha),
            );

            $headers = array(
                'Authorization: key=' . $idkeyserver, 
                'Content-Type: application/json'
            );

            // Open connection
            $ch = curl_init();

            if (FALSE === $ch)
                throw new Exception('failed to initialize');


            // Set the url, number of POST vars, POST data
            curl_setopt( $ch, CURLOPT_URL, $url );

            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            // Execute post
            $result = curl_exec($ch);
            //var_dump('RESULT!!!'.json_encode( $fields ));

           //var_dump($result);
            if (FALSE === $result)
                throw new Exception(curl_error($ch), curl_errno($ch));
            // Close connection
            curl_close($ch);
            return true;
        }catch(Exception $e){
            return false;
        }
    }

	function PushIOS($array,$titulo,$mensaje,$id,$fecha){
        try{
			//var_dump($array);
			//var_dump($badge);
            // Mensaje ya esta todo arriba
            $idkeyserver="AAAA4bh4Px8:APA91bHzTbG331sRIc6Z8Ilcm-7NNfqrb5aU2FQYoaEMLHmh2kCNwdNGcUmWD7XDIpA6AHXVi-dvXo6P-Lm5aOV6_SNM2-TNzrRp6KKyKPT9qsMeqmj8hoIB0vuV05Iyap2TR5sPpWWK";
            // Set POST variables
            $url = 'https://gcm-http.googleapis.com/gcm/send';
            //$array="ej6x2i_PGgs:APA91bHXJEDd31CiEMc5DHQUqVzLfqWJc5del6AlWjq8xeizDwE2AgidVtc7hhpRyCdA3w6jQQ_c1kjTyoCzujBdoLfR99fOOd_8yn-uPGvS2L03nu4aOTQz-DXQ9T4qXtSWiR4N-DAU";
            //$array = 'ej6x2i_PGgs:APA91bHXJEDd31CiEMc5DHQUqVzLfqWJc5del6AlWjq8xeizDwE2AgidVtc7hhpRyCdA3w6jQQ_c1kjTyoCzujBdoLfR99fOOd_8yn-uPGvS2L03nu4aOTQz-DXQ9T4qXtSWiR4N-DAU';
            //QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA

            //$array = 'mWa2XfdKxHU:APA91bFtu2bvCR2y9-RXhB0F-VSGdCdTmtEdAwKS2_EixvYAzN8V4LHzERo-EqaomJIrfiVXuMJIBna6buMiSigWk0gc9_03TGP6UgrXHpOfzwWBluvpXhNczYl40sQZhExE3S8DWgkl';

            $fields = (object)  array(
                //'registration_ids'  => ($array),
                //'data'              => array("titulo"=>"prueba Global", "mensaje" => "prueba Global", "tipo"=>2),
                'registration_ids' => array($array),
				'content_available' => false,
				'priority' =>  "high",
				'notification' => (object) array("body" =>$titulo, "title"=> $mensaje,  "tipo"=>1, "id"=>$id, "fecha"=>$fecha, "sound"=>"default"),
				//'data' => (object) array("idNotificacion"=>$idNotificacion)
            );

            $headers = array(
                'Authorization: key=' . $idkeyserver,
                'Content-Type: application/json'
            );

            //var_dump(json_encode($fields));
            // Open connection
            $ch = curl_init();

            if (FALSE === $ch)
                throw new Exception('failed to initialize');


            // Set the url, number of POST vars, POST data
            curl_setopt( $ch, CURLOPT_URL, $url );

            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            // Execute post
            $result = curl_exec($ch);
			
			//var_dump($result); 
			
            if (FALSE === $result)
                throw new Exception(curl_error($ch), curl_errno($ch));
            // Close connection
            curl_close($ch);
            return $result;
        }catch(Exception $e){
            return false;
        }
    }
	
	
}
?>