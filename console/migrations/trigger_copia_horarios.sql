DELIMITER &&

create trigger actualiza_horarios_entre_bases after insert on grupoava_2015.tbl_restaurantes_horarios 
for each row 
begin 
insert into grupoava_mobile.tbl_restaurantes_horarios(id_horario, dias, hora_desde, hora_hasta, dia_cerrado, id_restaurante) VALUES (NEW.id_horario, NEW.dias, NEW.hora_desde, NEW.hora_hasta, NEW.dia_cerrado, NEW.id_restaurante);
END &&