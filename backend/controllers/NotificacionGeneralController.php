<?php

namespace backend\controllers;

use Yii;
use backend\models\NotificacionGeneral;
use backend\models\NotificacionGeneralSearch;
use backend\models\NotificacionesHasUsuario;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\cPushService;


/**
 * NotificacionGeneralController implements the CRUD actions for NotificacionGeneral model.
 */
class NotificacionGeneralController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NotificacionGeneral models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificacionGeneralSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NotificacionGeneral model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NotificacionGeneral model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NotificacionGeneral();

        if ($model->load(Yii::$app->request->post())) {
            $model->estado = 1;
            if ($model->save()) {
                    
        //codigo push service
        
            $fecha =('d-m-Y');   

            $oPush = new cPushservice();
            
            $msg  = $model->descripcion;
            $title = $model->titulo;

            $AndroidUser    = $oPush->getAndroidPush();
            $idAndroid      = $oPush->getIdAndroid();
            
            $vPushIOS       = $oPush->getIOSPush();
            $idIOS          = $oPush->getIdIOS();

                foreach ($idAndroid AS $id){
                    
                    $notificacionHasUser = new NotificacionesHasUsuario();
                    $notificacionHasUser->idmobile_users = $id;
                    $notificacionHasUser->idnotificacion_general = $model->idnotificacion_general;
                    $notificacionHasUser->estado = 1;

                    $notificacionHasUser->save();
                }

                foreach ($idIOS AS $id){
                    
                    $notificacionHasUser = new NotificacionesHasUsuario();
                    $notificacionHasUser->idmobile_users = $id;
                    $notificacionHasUser->idnotificacion_general = $model->idnotificacion_general;
                    $notificacionHasUser->estado = 1;

                    $notificacionHasUser->save();
                }
            
            
            try{   
                if($AndroidUser){
                    $v1 = true;
                    try{
                        $cantidadU = count($AndroidUser);
                        if($AndroidUser){
                            $i = 0;
                            $y = 0;
                            $arraysend = array();
                            foreach($AndroidUser AS $token){

                                $i++;
                                if($y<=500){
                                    array_push($arraysend, $token);
                                    $y++;
                                }else{
                                    $v = $oPush->PushAndroidOffers($arraysend,$title,$msg,$model->idnotificacion_general,$fecha);
                                    $arraysend= array();
                                    $y = 1;
                                }
                                if($i==$cantidadU){

                                    $v = $oPush->PushAndroidOffers($arraysend,$title,$msg,$model->idnotificacion_general,$fecha);
                                
                                }
                            }
                        }
                    }catch(Exception $e){
                        echo $e->getMessage();
                    }
                }

                if($vPushIOS){
                    try{
                        $oPush->PushIOS($vPushIOS,$title,$msg,$fecha,$model->idnotificacion_general);
            
                    }catch(Exception $e){
                        
                        echo $e->getMessage();
                    }

                }
                //echo 'done';
            }catch(Exception $e){
                echo "error_msg";
            }
        
        //End

            return $this->redirect(['view', 'id' => $model->idnotificacion_general]);

            }
        
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
            
        }
    }

    /**
     * Updates an existing NotificacionGeneral model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idnotificacion_general]);
        } else {
            echo $model;
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NotificacionGeneral model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NotificacionGeneral model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NotificacionGeneral the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NotificacionGeneral::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
