<?php

namespace backend\controllers;

use Yii;
use backend\models\MenusPlatosMovil;
use backend\models\MenusPlatosMovilSearch;
use backend\models\TblMenusCategorias;
use backend\models\CategoriaPlatoSearch;
use backend\models\CatalogoCategoriaextra;
use backend\models\CategoriaPlato;
use backend\models\Extra;
use backend\models\CatalogoExtra;
use backend\models\TblRestaurantes;
use backend\components\BaseController;
use common\models\User;
use common\components\Util;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * MenusPlatosMovilController implements the CRUD actions for MenusPlatosMovil model.
 */
class MenusPlatosMovilController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view','index'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','update', 'detele'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN],
            ]
        ];
        return $behaviors;
    }

    /**
     * Lists all MenusPlatosMovil models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenusPlatosMovilSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MenusPlatosMovil model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MenusPlatosMovil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new MenusPlatosMovil();
        $modelCatalogo = new CatalogoCategoriaextra();

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $id])->one();
        
        $searchModelFilter = new MenusPlatosMovilSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['id_restaurante'=>$id]);

        if ($model->load(Yii::$app->request->post())) {

              //Cargamos los archivos en variables temporales
                $model->_fotografia = UploadedFile::getInstance($model, '_fotografia');

                //validamos que si se haya subido las imagenes por parte del usuario
                if ($model->_fotografia) {
                    $model->fotografia = Yii::$app->security->generateRandomString() . '.' . $model->_fotografia->extension;
                }
                
              if ($model->save()) {

                //validamos el registro del nombre de la fotografia
                  if (!empty($model->fotografia)) {
                    Util::uploadPlato($model->_fotografia, $model->fotografia);
                }
                //Guardando otros modelos
                $modelCatalogo = $_POST['CatalogoCategoriaextra'];

                if (isset($modelCatalogo)) {

                    foreach ($modelCatalogo as $Catalogo) {

                        if (is_array($Catalogo)) {

                            foreach ($Catalogo as $item) {

                                //Seleccionamos el catálogo a copiar
                                $dataCatalogo = CatalogoCategoriaextra::find()->where(['idcatalogocategoriaextra' => $item])->one();
                            
                                $modelCategoriaPlato = new CategoriaPlato();

                                //Copiamos el catálogo
                                $modelCategoriaPlato->nombre = $dataCatalogo->nombre;
                                $modelCategoriaPlato->estado = $dataCatalogo->estado;
                                $modelCategoriaPlato->orden = $dataCatalogo->orden;
                                $modelCategoriaPlato->multiple = $dataCatalogo->multiple;
                                $modelCategoriaPlato->requerido = $dataCatalogo->requerido;
                                $modelCategoriaPlato->id_restaurante = $dataCatalogo->idrestaurante;
                                $modelCategoriaPlato->id_plato_movil = $model->id_plato_movil;

                                //Guardamos el catálogo
                                $modelCategoriaPlato->save();

                                //Seleccionamos los extras del catalogo
                                $dataExtra = CatalogoExtra::find()->where(['idcatalogocategoriaextra' => $item])->All();

                                foreach ($dataExtra as $extra) {
                                    
                                    $modelExtra = new Extra();

                                    //Copiamos el extra 
                                    $modelExtra->nombre = $extra->nombre;
                                    $modelExtra->estado = $extra->estado;
                                    $modelExtra->precio = $extra->precio;
                                    $modelExtra->idcategoria_plato = $modelCategoriaPlato->idcategoria_plato;
                                    $modelExtra->id_plato_movil = $model->id_plato_movil;
                                    $modelExtra->id_restaurante = $model->id_restaurante;

                                    $modelExtra->save();

                                }
                            
                            }

                        }
                    
                    }

                }

                return $this->redirect(['update', 'id' => $model->id_plato_movil, 'idrestaurante' => $model->id_restaurante]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'modelCatalogo' => $modelCatalogo,
                'restaurante' => $restaurante,
            ]);
        }
    }

    /**
     * Updates an existing MenusPlatosMovil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $idrestaurante)
    {
        $model = $this->findModel($id);
        $modelCatalogo = new CatalogoCategoriaextra();

        $searchModel = new CategoriaPlatoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['id_plato_movil'=>$id]);

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $idrestaurante])->one();        

        if ($model->load(Yii::$app->request->post())) {

              //Cargamos los archivos en variables temporales
                $model->_fotografia = UploadedFile::getInstance($model, '_fotografia');

                //validamos que si se haya subido las imagenes por parte del usuario
                if ($model->_fotografia) {
                    $model->fotografia = Yii::$app->security->generateRandomString() . '.' . $model->_fotografia->extension;
                }
                
              if ($model->save()) {

                //validamos el registro del nombre de la fotografia
                  if (!empty($model->fotografia)) {
                    Util::uploadPlato($model->_fotografia, $model->fotografia);
                }
                //Guardando otros modelos
                $modelCatalogo = $_POST['CatalogoCategoriaextra'];

                if (isset($modelCatalogo)) {

                    foreach ($modelCatalogo as $Catalogo) {

                        if (is_array($Catalogo)) {

                            foreach ($Catalogo as $item) {

                                //Seleccionamos el catálogo a copiar
                                $dataCatalogo = CatalogoCategoriaextra::find()->where(['idcatalogocategoriaextra' => $item])->one();
                            
                                $modelCategoriaPlato = new CategoriaPlato();

                                //Copiamos el catálogo
                                $modelCategoriaPlato->nombre = $dataCatalogo->nombre;
                                $modelCategoriaPlato->estado = $dataCatalogo->estado;
                                $modelCategoriaPlato->multiple = $dataCatalogo->multiple;
                                $modelCategoriaPlato->requerido = $dataCatalogo->requerido;
                                $modelCategoriaPlato->id_restaurante = $dataCatalogo->idrestaurante;
                                $modelCategoriaPlato->id_plato_movil = $model->id_plato_movil;

                                //Guardamos el catálogo
                                $modelCategoriaPlato->save();

                                //Seleccionamos los extras del catalogo
                                $dataExtra = CatalogoExtra::find()->where(['idcatalogocategoriaextra' => $item])->All();

                                foreach ($dataExtra as $extra) {
                                    
                                    $modelExtra = new Extra();

                                    //Copiamos el extra 
                                    $modelExtra->nombre = $extra->nombre;
                                    $modelExtra->estado = $extra->estado;
                                    $modelExtra->precio = $extra->precio;
                                    $modelExtra->idcategoria_plato = $modelCategoriaPlato->idcategoria_plato;
                                    $modelExtra->id_plato_movil = $model->id_plato_movil;
                                    $modelExtra->id_restaurante = $model->id_restaurante;

                                    $modelExtra->save();

                                }
                            
                            }

                        }
                    
                    }

                }

                return $this->redirect(['update', 'id' => $model->id_plato_movil, 'idrestaurante' => $model->id_restaurante]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'modelCatalogo' => $modelCatalogo,
                'restaurante' => $restaurante,
            ]);
        }
    }

    /**
     * Deletes an existing MenusPlatosMovil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MenusPlatosMovil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MenusPlatosMovil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MenusPlatosMovil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionShowcat()
    {
        
        $idrestaurant = $_POST['idres'];
        
        $totalCatsByRestaurant = TblMenusCategorias::find()->where(['id_restaurante' => $idrestaurant])->count();
        echo '<div class="form-group field-menusplatosmovil-id_categoria required has-success">
              <label class="control-label" for="menusplatosmovil-id_categoria">Categoría</label>
              <select id="menusplatosmovil-id_categoria" class="form-control" name="MenusPlatosMovil[id_categoria]">';
        if ($totalCatsByRestaurant > 0) {
            $catsByRestaurant =TblMenusCategorias::find()->where(['id_restaurante' => $idrestaurant])->all();
             foreach ($catsByRestaurant as $oneCatRes){
                echo "<option value='" . $oneCatRes->id_categoria . "'>" . $oneCatRes->nombre . "</option>";
            }
        echo '</select>
              <div class="help-block"></div>
              </div>';
        }         
    }
}
