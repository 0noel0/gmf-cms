<?php

namespace backend\controllers;

use Yii;
use backend\models\CategoriaPlato;
use backend\models\CategoriaPlatoSearch;
use backend\models\MenusPlatosMovil;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\ExtraSearch;

/**
 * CategoriaPlatoController implements the CRUD actions for CategoriaPlato model.
 */
class CategoriaPlatoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CategoriaPlato models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoriaPlatoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CategoriaPlato model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CategoriaPlato model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new CategoriaPlato();
        $searchModelFilter = new CategoriaPlatoSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['id_restaurante'=>$id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idcategoria_plato]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing CategoriaPlato model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $searchModelFilter = new ExtraSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        //Filtro por Plato y por restaurante de la categoria de extras
        $dataProvider->query->andWhere(['idcategoria_plato'=>$id, 'id_restaurante'=> $model->id_restaurante]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idcategoria_plato]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing CategoriaPlato model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CategoriaPlato model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CategoriaPlato the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CategoriaPlato::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionShowplatos(){
        
        $idrestaurant = $_POST['idres'];
        
        $totalPlatos = MenusPlatosMovil::find()->where(['id_restaurante' => $idrestaurant])->count();
        echo '<div class="form-group field-categoriaplato-id_plato_movil">
                <label class="control-label" for="categoriaplato-id_plato_movil">Plato App Móvil</label>
                <select id="categoriaplato-id_plato_movil" class="form-control" name="CategoriaPlato[id_plato_movil]">';
        if ($totalPlatos > 0) {
            $platos =MenusPlatosMovil::find()->where(['id_restaurante' => $idrestaurant])->all();
             foreach ($platos as $plato){
                echo "<option value='" . $plato->id_plato_movil . "'>" . $plato->nombre . "</option>";
            }
        echo '</select>
              <div class="help-block"></div>
              </div>';
        }
    }
}
