<?php

namespace backend\controllers;

use Yii;
use backend\models\CatalogoCategoriaextra;
use backend\models\CatalogoCategoriaextraSearch;
use backend\models\CatalogoExtraSearch;
use backend\models\CatalogoExtra;
use backend\models\TblRestaurantes;
use backend\components\BaseController;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CatalogoCategoriaextraController implements the CRUD actions for CatalogoCategoriaextra model.
 */
class CatalogoCategoriaextraController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view','index'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','update', 'detele'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN],
            ]
        ];
        return $behaviors;
    }

    /**
     * Lists all CatalogoCategoriaextra models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CatalogoCategoriaextraSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CatalogoCategoriaextra model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CatalogoCategoriaextra model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new CatalogoCategoriaextra();


        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $id])->one();
        $searchModelFilter = new CatalogoCategoriaextraSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idrestaurante'=>$id]);

        //$extras = (new \yii\db\Query())
        //        ->select(['idcatalogocategoriaextra', 'nombre'])
        //        ->from('catalogo_categoriaextra')
        //        ->where(['idrestaurante' => $id])
        //        ->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'restaurante' => $restaurante,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing CatalogoCategoriaextra model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $idrestaurante)
    {
        $model = $this->findModel($id);

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $idrestaurante])->one();


        $searchModelFilter = new CatalogoExtraSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idcatalogocategoriaextra'=>$id]);

        //Total de Extras del catalogo
        $totExtra = CatalogoExtra::find()->where(['idcatalogocategoriaextra' => $id])->all();
        
        $countExtra = count($totExtra);     


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->idcatalogocategoriaextra, 'idrestaurante' => $model->idrestaurante]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'countExtra' => $countExtra,
                'restaurante' => $restaurante
            ]);
        }
    }

    /**
     * Deletes an existing CatalogoCategoriaextra model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CatalogoCategoriaextra model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CatalogoCategoriaextra the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CatalogoCategoriaextra::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
