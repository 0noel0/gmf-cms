<?php

namespace backend\controllers;

use Yii;
use backend\models\DetalleCategoriaPlato;
use backend\models\DetalleCategoriaPlatoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\MenusPlatosMovil;
use backend\models\CategoriaPlato;

//use backend\models\TblRestaurantes;
//use backend\models\TblMenusCategorias;

/**
 * DetalleCategoriaPlatoController implements the CRUD actions for DetalleCategoriaPlato model.
 */
class DetalleCategoriaPlatoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DetalleCategoriaPlato models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DetalleCategoriaPlatoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DetalleCategoriaPlato model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = DetalleCategoriaPlato::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException;
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new DetalleCategoriaPlato model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*BEGIN Create original*/
    public function actionCreate()
    {
        $model = new DetalleCategoriaPlato();
        $model2 = new CategoriaPlato();
                
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->iddetalle_categoria_plato]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'model2' => $model2,
            ]);
        }
    }
    /*END Create original*/
    
    /**
     * Updates an existing DetalleCategoriaPlato model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //$model = $this->findModel($id);
        $model = DetalleCategoriaPlato::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->iddetalle_categoria_plato]);
        } else {
            return $this->renderPartial('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DetalleCategoriaPlato model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DetalleCategoriaPlato model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DetalleCategoriaPlato the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionShowplatos(){
        
        $idrestaurant = $_POST['idres'];
        
        $totalPlatos = MenusPlatosMovil::find()->where(['id_restaurante' => $idrestaurant])->count();
        echo '<div class="form-group field-detallecategoriaplato-id_plato_movil required has-success">
              <label class="control-label" for="detallecategoriaplato-id_plato_movil">Plato App Móvil</label>
              <select id="detallecategoriaplato-id_plato_movil" class="form-control" name="DetalleCategoriaPlato[id_plato_movil]">
              <option value="NULL">Seleccione</option>';
        if ($totalPlatos > 0) {
            $platos =MenusPlatosMovil::find()->where(['id_restaurante' => $idrestaurant])->all();
             foreach ($platos as $plato){
                echo "<option value='" . $plato->id_plato_movil . "'>" . $plato->nombre . "</option>";
            }
        echo '</select>
              <div class="help-block"></div>
              </div>';
        }         
    }
}
