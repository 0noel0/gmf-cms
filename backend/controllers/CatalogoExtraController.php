<?php

namespace backend\controllers;

use Yii;
use backend\models\CatalogoExtra;
use backend\models\CatalogoExtraSearch;
use backend\models\CatalogoCategoriaextra;
use backend\models\TblRestaurantes;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * CatalogoExtraController implements the CRUD actions for CatalogoExtra model.
 */
class CatalogoExtraController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CatalogoExtra models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CatologoExtraSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CatalogoExtra model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CatalogoExtra model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id, $idrestaurante)
    {
        $model = new CatalogoExtra();

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $idrestaurante])->one();

        $catalogo = CatalogoCategoriaextra::find()->where(['idcatalogocategoriaextra' => $id])->one();

        $searchModelFilter = new CatalogoExtraSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idcatalogocategoriaextra'=>$id]);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create', 'id' => $model->idcatalogocategoriaextra, 'idrestaurante' => $restaurante->id_restaurante]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'restaurante' => $restaurante,
                'catalogo' => $catalogo,
            ]);
        }
    }

    /**
     * Updates an existing CatalogoExtra model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $idrestaurante,$idcatalogo)
    {
        $model = $this->findModel($id);

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $idrestaurante])->one();

        $catalogo = CatalogoCategoriaextra::find()->where(['idcatalogocategoriaextra' => $idcatalogo])->one();

        $searchModelFilter = new CatalogoExtraSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idcatalogocategoriaextra'=>$idcatalogo]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create', 'id' => $model->idcatalogo_extra, 'idrestaurante' => $restaurante->id_restaurante]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'restaurante' => $restaurante,
                'catalogo' => $catalogo,
            ]);
        }
    }

    /**
     * Deletes an existing CatalogoExtra model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CatalogoExtra model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CatalogoExtra the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CatalogoExtra::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
