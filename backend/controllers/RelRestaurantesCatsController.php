<?php

namespace backend\controllers;

use Yii;
use backend\models\RelRestaurantesCats;
use backend\models\RelRestaurantesCatsSearch;
use backend\models\TblRestaurantes;
use backend\components\BaseController;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RelRestaurantesCatsController implements the CRUD actions for RelRestaurantesCats model.
 */
class RelRestaurantesCatsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view','index'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','update', 'detele'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN],
            ]
        ];
        return $behaviors;
    }

    /**
     * Lists all RelRestaurantesCats models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RelRestaurantesCatsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RelRestaurantesCats model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RelRestaurantesCats model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new RelRestaurantesCats();
        $searchModel = new RelRestaurantesCatsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['id_restaurante'=>$id]);

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $id])->one();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create', 'id' => $restaurante->id_restaurante]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'restaurante' => $restaurante,
            ]);
        }
    }

    /**
     * Updates an existing RelRestaurantesCats model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_relacion]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RelRestaurantesCats model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $restaurante)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['create', 'id' => $restaurante]);
    }

    /**
     * Finds the RelRestaurantesCats model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RelRestaurantesCats the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RelRestaurantesCats::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
