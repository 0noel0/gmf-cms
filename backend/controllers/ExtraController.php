<?php

namespace backend\controllers;

use Yii;
use backend\models\Extra;
use backend\models\ExtraSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\CategoriaPlato;
use backend\models\MenusPlatosMovil;
use yii\Helpers\Url;

/**
 * ExtraController implements the CRUD actions for Extra model.
 */
class ExtraController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Extra models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExtraSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Extra model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Extra model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Extra();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idextra]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Extra model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->idextra]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Extra model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Extra model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Extra the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Extra::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

     public function actionShowplatos(){
        
        $idrestaurant = $_POST['idres'];
        
        $totalPlatos = MenusPlatosMovil::find()->where(['id_restaurante' => $idrestaurant])->count();
        echo '<div class="form-group field-extra-id_plato_movil">
                <label class="control-label" for="extra-id_plato_movil">Plato App Móvil</label>
                <select id="extra-id_plato_movil" class="form-control" name="Extra[id_plato_movil]" onchange="$.post(&quot;/gmf/backend/web/index.php?r=extra/showcatsextra&quot;,{idplato: $(this).val()}, function(data){
                $(&quot;#renderCatsExtra&quot;).html(data);
                })">';
        if ($totalPlatos > 0) {
            $platos =MenusPlatosMovil::find()->where(['id_restaurante' => $idrestaurant])->all();
             foreach ($platos as $plato){
                echo "<option value='" . $plato->id_plato_movil . "'>" . $plato->nombre . "</option>";
            }
        echo '</select>
              <div class="help-block"></div>
              </div>';
        } 
    }

    public function actionShowcatsextra(){
        
        $idplato = $_POST['idplato'];
        
        $totalPlatos = CategoriaPlato::find()->where(['id_plato_movil' => $idplato])->count();
        echo '<div class="form-group field-extra-idcategoria_plato">
                <label class="control-label" for="extra-idcategoria_plato">Categoría de Extra</label>
                <select id="extra-idcategoria_plato" class="form-control" name="Extra[idcategoria_plato]">';
        if ($totalPlatos > 0) {
            $platos =CategoriaPlato::find()->where(['id_plato_movil' => $idplato])->all();
             foreach ($platos as $plato){
                echo "<option value='" . $plato->idcategoria_plato . "'>" . $plato->nombre . "</option>";
            }
        echo '</select>
              <div class="help-block"></div>
              </div>';
        }         
    }
}

