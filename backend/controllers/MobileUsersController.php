<?php

namespace backend\controllers;

use Yii;
use backend\models\MobileUsers;
use backend\models\MobileUsersSearch;
use backend\models\OrdenUsuarioSearch;
use backend\models\OrdenUsuario;
use backend\components\BaseController;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MobileUsersController implements the CRUD actions for MobileUsers model.
 */
class MobileUsersController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view','index'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','update', 'detele'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN],
            ]
        ];
        return $behaviors;
    }
    /**
     * Lists all MobileUsers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MobileUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MobileUsers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MobileUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MobileUsers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idmobileusr]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MobileUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            return $this->redirect(['view', 'id' => $model->idmobileusr]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MobileUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MobileUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MobileUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MobileUsers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPush()
    {
        $searchModel = new MobileUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('push', [
            'searchModel' => $searchModel,
            'dataModel' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MobileUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    
    public function actionNotificacionEspecifica()
    {
        $model = new OrdenUsuario();

         $searchModelFilter = new OrdenUsuarioSearch();
         $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post())) {
            
            $rs = OrdenUsuario::find()->where(['between', 'fechaInicio', $model->fechaInicio,$model->fechaFinal])->all();

            $searchModelFilter = new OrdenUsuarioSearch();
            $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
            $dataProvider->query->where(['between', 'fechaInicio', $model->fechaInicio,$model->fechaFinal])->all();
            

            return $this->render('notificacion-especifica', [
                'model' => $model,
                'fechaInicio' => $model->fechaInicio,
                'fechaFinal' => $model->fechaFinal,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('notificacion-especifica', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'fechaInicio' => "2017-08-24",
                'fechaFinal' => "2017-08-24",
            ]);
        }
    }


}
