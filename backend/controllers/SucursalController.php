<?php

namespace backend\controllers;

use Yii;
use backend\models\Sucursal;
use backend\models\SucursalSearch;
use backend\models\TblRestaurantes;
use backend\components\BaseController;
use common\models\User;
use common\components\Util;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SucursalController implements the CRUD actions for Sucursal model.
 */
class SucursalController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view','index'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','update', 'detele'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN],
            ]
        ];
        return $behaviors;
    }

    /**
     * Lists all Sucursal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SucursalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sucursal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sucursal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Sucursal();  

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $id])->one();

        $searchModelFilter = new SucursalSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['id_restaurante'=>$id]);
		
        if ($model->load(Yii::$app->request->post())) {
			//$model->setPassword($model->password_hash);
            $model->password_hash = sha1($model->password_hash);
			if($model->save()){
				 return $this->redirect(['create', 'id' => $model->id_restaurante]);
			}
        } else {
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'restaurante' => $restaurante,
            ]);
        }
    }

    /**
     * Updates an existing Sucursal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $restaurante)
    {
        $model = $this->findModel($id);

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $restaurante])->one();

        $searchModelFilter = new SucursalSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['id_restaurante'=>$restaurante]);

        
        if ($model->load(Yii::$app->request->post())) {
            //$model->setPassword($model->password_hash);
            $model->password_hash = sha1($model->password_hash);
            if($model->save()){
                 return $this->redirect(['create', 'id' => $model->id_restaurante]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'restaurante' => $restaurante,
            ]);
        }
    }

    /**
     * Deletes an existing Sucursal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['create', 'id' => $model->id_restaurante]);
    }

    /**
     * Finds the Sucursal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sucursal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sucursal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
