<?php

namespace backend\controllers;

use Yii;
use backend\models\Historialpedido;
use backend\models\HistorialpedidoSearch;
use backend\models\Historial;
use backend\models\HistorialSearch;
use backend\models\HistoricoPerdidas;
use backend\models\HistoricoPerdidasSearch;
use backend\models\OrdenesRechazadasRestauranteSearch;
use backend\models\HistorialRechazadoRestauranteSearch;
use backend\models\HistorialAprobadasRestaurantesSearch;
use backend\models\Historial2minOrdenesSearch;
use backend\models\OrdenUsuario;
use backend\components\BaseController;
use backend\models\OrdenUsuarioSearch;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HistorialpedidoController implements the CRUD actions for Historialpedido model.
 */
class HistorialpedidoController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view','index','historial', 'pendientes', 'aprobados-restaurante', 'rechazados-restaurante', 'recibidos-motorista', 'entregados', 'ordenes-perdidas', '2minutos', 'detalle'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','update', 'detele'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN],
            ]
        ];
        return $behaviors;
    }
    /**
     * Lists all Historialpedido models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HistorialpedidoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Historialpedido model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Historialpedido model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Historialpedido();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idhistorialpedido]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Historialpedido model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idhistorialpedido]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Historialpedido model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Historialpedido model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Historialpedido the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Historialpedido::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

       /**
     * Lists all Historialpedido models.
     * @return mixed
     */
    public function actionHistorial()
    {

        //$start = $_REQUEST['start'];
        //$end = $_REQUEST['end'];

        //Solicitado 1
        //$searchModel = new HistorialpedidoSearch();
        $searchModel = new HistorialSearch();

        $searchModelRechazadas = new HistorialRechazadoRestauranteSearch();

        $searchModelPerdidas = new OrdenUsuarioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idestadonuevo'=>1])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Aprobado  por restaurante 7
        $dataProvider7 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider7->query->andWhere(['idestadonuevo'=>7])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending   

          //Recibido por motorista 11
        $dataProvider11 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider11->query->andWhere(['idestadonuevo'=>11])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending  

        //Entregao 12
        $dataProvider12 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider12->query->andWhere(['idestadonuevo'=>12])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Perdido 12
        $dataProvider13 = $searchModelPerdidas->search(Yii::$app->request->queryParams);
        $dataProvider13->query->andWhere(['estadoPedido'=>13])->orderBy(['fechaInicio'=>SORT_DESC]); // sort descending

         //Rechazado por restaurante 8
        $dataProvider8 = $searchModelRechazadas->search(Yii::$app->request->queryParams);
        $dataProvider8->query->where('TIMESTAMPDIFF(MINUTE,fechaCreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        $model = new Historialpedido();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['historial&start='.$model->Inicio.'&end='.$model->Fin]);
        } else {
            return $this->render('historial', [
                'model' => $model,
                'searchModel' => $searchModel,
                'searchModelPerdidas' => $searchModelPerdidas,
                'dataProvider' => $dataProvider,
                'dataProvider7' => $dataProvider7,
                'dataProvider11' => $dataProvider11,
                'dataProvider12' => $dataProvider12,
                'dataProvider13' => $dataProvider13,
                'dataProvider8' => $dataProvider8,
            ]);
        }   
    }
     public function actionPendientes()
    {

        //$start = $_REQUEST['start'];
        //$end = $_REQUEST['end'];

        //Solicitado 1
        //$searchModel = new HistorialpedidoSearch();
        $searchModel = new HistorialSearch();

        $searchModelRechazadas = new HistorialRechazadoRestauranteSearch();

        $searchModelPerdidas = new OrdenUsuarioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idestadonuevo'=>1])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Aprobado  por restaurante 7
        $dataProvider7 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider7->query->andWhere(['idestadonuevo'=>7])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending   

          //Recibido por motorista 11
        $dataProvider11 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider11->query->andWhere(['idestadonuevo'=>11])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending  

        //Entregao 12
        $dataProvider12 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider12->query->andWhere(['idestadonuevo'=>12])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Perdido 12
        $dataProvider13 = $searchModelPerdidas->search(Yii::$app->request->queryParams);
        $dataProvider13->query->andWhere(['estadoPedido'=>13])->orderBy(['fechaInicio'=>SORT_DESC]); // sort descending

         //Rechazado por restaurante 8
        $dataProvider8 = $searchModelRechazadas->search(Yii::$app->request->queryParams);
        $dataProvider8->query->where('TIMESTAMPDIFF(MINUTE,fechaCreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        $model = new Historialpedido();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['historial&start='.$model->Inicio.'&end='.$model->Fin]);
        } else {
            return $this->render('pendientes', [
                'model' => $model,
                'searchModel' => $searchModel,
                'searchModelPerdidas' => $searchModelPerdidas,
                'dataProvider' => $dataProvider,
                'dataProvider7' => $dataProvider7,
                'dataProvider11' => $dataProvider11,
                'dataProvider12' => $dataProvider12,
                'dataProvider13' => $dataProvider13,
                'dataProvider8' => $dataProvider8,
            ]);
        }   
    }
     public function actionAprobadosRestaurante()
    {

        //$start = $_REQUEST['start'];
        //$end = $_REQUEST['end'];

        //Solicitado 1
        //$searchModel = new HistorialpedidoSearch();
        $searchModel = new HistorialSearch();

        $searchModelRechazadas = new HistorialRechazadoRestauranteSearch();

        $searchModelAprobadas = new HistorialAprobadasRestaurantesSearch();



        $searchModelPerdidas = new OrdenUsuarioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idestadonuevo'=>1])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Aprobado  por restaurante 7
        $dataProvider7 = $searchModelAprobadas->search(Yii::$app->request->queryParams);
        $dataProvider7->query->where('TIMESTAMPDIFF(MINUTE,fechacreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending   

          //Recibido por motorista 11
        $dataProvider11 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider11->query->andWhere(['idestadonuevo'=>11])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending  

        //Entregao 12
        $dataProvider12 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider12->query->andWhere(['idestadonuevo'=>12])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Perdido 12
        $dataProvider13 = $searchModelPerdidas->search(Yii::$app->request->queryParams);
        $dataProvider13->query->andWhere(['estadoPedido'=>13])->orderBy(['fechaInicio'=>SORT_DESC]); // sort descending

         //Rechazado por restaurante 8
        $dataProvider8 = $searchModelRechazadas->search(Yii::$app->request->queryParams);
        $dataProvider8->query->where('TIMESTAMPDIFF(MINUTE,fechaCreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        $model = new Historialpedido();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['historial&start='.$model->Inicio.'&end='.$model->Fin]);
        } else {
            return $this->render('aprobados-restaurante', [
                'model' => $model,
                'searchModel' => $searchModel,
                'searchModelPerdidas' => $searchModelPerdidas,
                'dataProvider' => $dataProvider,
                'dataProvider7' => $dataProvider7,
                'dataProvider11' => $dataProvider11,
                'dataProvider12' => $dataProvider12,
                'dataProvider13' => $dataProvider13,
                'dataProvider8' => $dataProvider8,
            ]);
        }   
    }
     public function actionRechazadosRestaurante()
    {

        //$start = $_REQUEST['start'];
        //$end = $_REQUEST['end'];

        //Solicitado 1
        //$searchModel = new HistorialpedidoSearch();
        $searchModel = new HistorialSearch();

        $searchModelRechazadas = new HistorialRechazadoRestauranteSearch();

        $searchModelPerdidas = new OrdenUsuarioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idestadonuevo'=>1])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Aprobado  por restaurante 7
        $dataProvider7 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider7->query->andWhere(['idestadonuevo'=>7])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending   

          //Recibido por motorista 11
        $dataProvider11 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider11->query->andWhere(['idestadonuevo'=>11])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending  

        //Entregao 12
        $dataProvider12 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider12->query->andWhere(['idestadonuevo'=>12])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Perdido 12
        $dataProvider13 = $searchModelPerdidas->search(Yii::$app->request->queryParams);
        $dataProvider13->query->andWhere(['estadoPedido'=>13])->orderBy(['fechaInicio'=>SORT_DESC]); // sort descending

         //Rechazado por restaurante 8
        $dataProvider8 = $searchModelRechazadas->search(Yii::$app->request->queryParams);
        $dataProvider8->query->where('TIMESTAMPDIFF(MINUTE,fechaCreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        $model = new Historialpedido();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['historial&start='.$model->Inicio.'&end='.$model->Fin]);
        } else {
            return $this->render('rechazados-restaurante', [
                'model' => $model,
                'searchModel' => $searchModel,
                'searchModelPerdidas' => $searchModelPerdidas,
                'dataProvider' => $dataProvider,
                'dataProvider7' => $dataProvider7,
                'dataProvider11' => $dataProvider11,
                'dataProvider12' => $dataProvider12,
                'dataProvider13' => $dataProvider13,
                'dataProvider8' => $dataProvider8,
            ]);
        }   
    }
    public function actionRecibidosMotorista()
    {

        //$start = $_REQUEST['start'];
        //$end = $_REQUEST['end'];

        //Solicitado 1
        //$searchModel = new HistorialpedidoSearch();
        $searchModel = new HistorialSearch();

        $searchModelRechazadas = new HistorialRechazadoRestauranteSearch();

        $searchModelPerdidas = new OrdenUsuarioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idestadonuevo'=>1])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Aprobado  por restaurante 7
        $dataProvider7 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider7->query->andWhere(['idestadonuevo'=>7])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending   

          //Recibido por motorista 11
        $dataProvider11 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider11->query->andWhere(['idestadonuevo'=>11])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending  

        //Entregao 12
        $dataProvider12 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider12->query->andWhere(['idestadonuevo'=>12])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Perdido 12
        $dataProvider13 = $searchModelPerdidas->search(Yii::$app->request->queryParams);
        $dataProvider13->query->andWhere(['estadoPedido'=>13])->orderBy(['fechaInicio'=>SORT_DESC]); // sort descending

         //Rechazado por restaurante 8
        $dataProvider8 = $searchModelRechazadas->search(Yii::$app->request->queryParams);
        $dataProvider8->query->where('TIMESTAMPDIFF(MINUTE,fechaCreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        $model = new Historialpedido();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['historial&start='.$model->Inicio.'&end='.$model->Fin]);
        } else {
            return $this->render('recibidos-motorista', [
                'model' => $model,
                'searchModel' => $searchModel,
                'searchModelPerdidas' => $searchModelPerdidas,
                'dataProvider' => $dataProvider,
                'dataProvider7' => $dataProvider7,
                'dataProvider11' => $dataProvider11,
                'dataProvider12' => $dataProvider12,
                'dataProvider13' => $dataProvider13,
                'dataProvider8' => $dataProvider8,
            ]);
        }   
    }
    public function actionEntregados()
    {

        //$start = $_REQUEST['start'];
        //$end = $_REQUEST['end'];

        //Solicitado 1
        //$searchModel = new HistorialpedidoSearch();
        $searchModel = new HistorialSearch();

        $searchModelRechazadas = new HistorialRechazadoRestauranteSearch();

        $searchModelPerdidas = new OrdenUsuarioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idestadonuevo'=>1])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Aprobado  por restaurante 7
        $dataProvider7 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider7->query->andWhere(['idestadonuevo'=>7])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending   

          //Recibido por motorista 11
        $dataProvider11 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider11->query->andWhere(['idestadonuevo'=>11])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending  

        //Entregao 12
        $dataProvider12 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider12->query->andWhere(['idestadonuevo'=>12])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Perdido 12
        $dataProvider13 = $searchModelPerdidas->search(Yii::$app->request->queryParams);
        $dataProvider13->query->andWhere(['estadoPedido'=>13])->orderBy(['fechaInicio'=>SORT_DESC]); // sort descending

         //Rechazado por restaurante 8
        $dataProvider8 = $searchModelRechazadas->search(Yii::$app->request->queryParams);
        $dataProvider8->query->where('TIMESTAMPDIFF(MINUTE,fechaCreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        $model = new Historialpedido();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['historial&start='.$model->Inicio.'&end='.$model->Fin]);
        } else {
            return $this->render('entregados', [
                'model' => $model,
                'searchModel' => $searchModel,
                'searchModelPerdidas' => $searchModelPerdidas,
                'dataProvider' => $dataProvider,
                'dataProvider7' => $dataProvider7,
                'dataProvider11' => $dataProvider11,
                'dataProvider12' => $dataProvider12,
                'dataProvider13' => $dataProvider13,
                'dataProvider8' => $dataProvider8,
            ]);
        }   
    }
    public function actionOrdenesPerdidas()
    {

        //$start = $_REQUEST['start'];
        //$end = $_REQUEST['end'];

        //Solicitado 1
        //$searchModel = new HistorialpedidoSearch();
        $searchModel = new HistorialSearch();

        $searchModelRechazadas = new HistorialRechazadoRestauranteSearch();

        $searchModelPerdidas = new OrdenUsuarioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idestadonuevo'=>1])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Aprobado  por restaurante 7
        $dataProvider7 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider7->query->andWhere(['idestadonuevo'=>7])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending   

          //Recibido por motorista 11
        $dataProvider11 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider11->query->andWhere(['idestadonuevo'=>11])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending  

        //Entregao 12
        $dataProvider12 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider12->query->andWhere(['idestadonuevo'=>12])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Perdido 12
        $dataProvider13 = $searchModelPerdidas->search(Yii::$app->request->queryParams);
        $dataProvider13->query->andWhere(['estadoPedido'=>13])->orderBy(['fechaInicio'=>SORT_DESC]); // sort descending

         //Rechazado por restaurante 8
        $dataProvider8 = $searchModelRechazadas->search(Yii::$app->request->queryParams);
        $dataProvider8->query->where('TIMESTAMPDIFF(MINUTE,fechaCreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        $model = new Historialpedido();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['historial&start='.$model->Inicio.'&end='.$model->Fin]);
        } else {
            return $this->render('ordenes-perdidas', [
                'model' => $model,
                'searchModel' => $searchModel,
                'searchModelPerdidas' => $searchModelPerdidas,
                'dataProvider' => $dataProvider,
                'dataProvider7' => $dataProvider7,
                'dataProvider11' => $dataProvider11,
                'dataProvider12' => $dataProvider12,
                'dataProvider13' => $dataProvider13,
                'dataProvider8' => $dataProvider8,
            ]);
        }   
    }

     public function action2minutos()
    {

        //$start = $_REQUEST['start'];
        //$end = $_REQUEST['end'];

        //Solicitado 1
        //$searchModel = new HistorialpedidoSearch();
        $searchModel = new HistorialSearch();

        $searchModelRechazadas = new HistorialRechazadoRestauranteSearch();

        $searchModel2minutos = new Historial2minOrdenesSearch();

        $searchModelPerdidas = new OrdenUsuarioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['idestadonuevo'=>1])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Aprobado  por restaurante 7
        $dataProvider7 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider7->query->andWhere(['idestadonuevo'=>7])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending   

          //Recibido por motorista 11
        $dataProvider11 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider11->query->andWhere(['idestadonuevo'=>11])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending  

        //Entregao 12
        $dataProvider12 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider12->query->andWhere(['idestadonuevo'=>12])->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        //Perdido 12
        $dataProvider13 = $searchModelPerdidas->search(Yii::$app->request->queryParams);
        $dataProvider13->query->andWhere(['estadoPedido'=>13])->orderBy(['fechaInicio'=>SORT_DESC]); // sort descending

         //Rechazado por restaurante 8
        $dataProvider8 = $searchModelRechazadas->search(Yii::$app->request->queryParams);
        $dataProvider8->query->where('TIMESTAMPDIFF(MINUTE,fechaCreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

          //Rechazado por restaurante 8
        $dataProvider2minutos = $searchModel2minutos->search(Yii::$app->request->queryParams);
        $dataProvider2minutos->query->where('TIMESTAMPDIFF(MINUTE,fechacreacion,NOW()) < 1440')->orderBy(['fechacreacion'=>SORT_DESC]); // sort descending

        $model = new Historialpedido();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['historial&start='.$model->Inicio.'&end='.$model->Fin]);
        } else {
            return $this->render('2minutos', [
                'model' => $model,
                'searchModel' => $searchModel,
                'searchModelPerdidas' => $searchModelPerdidas,
                'dataProvider' => $dataProvider,
                'dataProvider7' => $dataProvider7,
                'dataProvider11' => $dataProvider11,
                'dataProvider12' => $dataProvider12,
                'dataProvider13' => $dataProvider13,
                'dataProvider8' => $dataProvider8,
                'dataProvider2minutos' => $dataProvider2minutos,
            ]);
        }   
    }


     public function actionDetalle($id)
    {
        $model = OrdenUsuario::findOne($id);

        return $this->render('detalle', [
            'model' => $model,
        ]);
    }
}
