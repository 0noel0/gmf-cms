<?php


namespace backend\controllers;

use Yii;
use backend\models\OrdenUsuario;
use backend\models\OrdenUsuarioSearch;
use backend\models\PlatoHasOrdenUsuarioSearch;
use backend\models\PlatosOrdenesUsuario;
use backend\models\UsuariosOrdenesUsuarios;
use backend\models\OrdenUsuarioExtras;
use backend\models\OrdenUsuarioIniciadas;
use backend\models\OrdenUsuarioHasSucursal;
use backend\models\OrdenUsuarioSucursal;
use backend\models\OrdenUsuarioSucursalSearch;
use backend\models\OrdenesRechazadasRestaurante;
use backend\models\HistorialRechazadoRestaurante;
use backend\models\Sucursal;
use backend\models\Motorista;
use backend\models\Factura;
use backend\models\Historialpedido;
use backend\models\NotificacionUsuario;
use backend\models\NotificacionUsuarioSearch;
use backend\models\TblRestaurantes;
use backend\models\NotificacionDespachoSearch;
use backend\components\BaseController;
use common\models\User;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * OrdenUsuarioController implements the CRUD actions for OrdenUsuario model.
 */
class OrdenUsuarioController extends BaseController
{
    /**
     * @inheritdoc
     */
    const EARTH_RADIUS_KM = 6371;    

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view', 'ordenes-iniciadas', 'ordenes-rechazadas', 'ticket','detalle-orden', 'restablecer-orden','aceptar-orden','rechazar-orden','aceptar-restaurante', 'rechazar-restaurante','lista-tickets','lista-notificaciones','total-iniciadas','distania', 'retraso','habilitar', 'aceptar-restaurante-sin', 'rechazar-restaurante-sin', 'detalle'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','index','despacho','asignar'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN],
            ],
            [
                'actions' => ['update'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['despacho', 'asignar', 'lista-tickets', 'lista-notificaciones-despacho','ordenes-rechazadas', 'ordenes-iniciadas'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR],
            ],
            [
                'actions' => ['delete'],
                'allow' => true,
                'roles' => ['#'],
            ],
        ];
        return $behaviors;
    }

    /**
     * Lists all OrdenUsuario models.
     * @return mixed
     */


    public function actionPush($titulo, $mensaje, $token, $ticket, $tipo)
    { 
      if ($tipo == 2) {
        //Envio de push a Restaurantes/Sucursales
        /********* set Key***/
        $apiKey = 'AAAA1OW5Zak:APA91bFjQIKOsDPIJT5bWd6KqCjMxC3IC4a6N-SyYRXopS3K_she1NRZJjbGp35G6oAVHAYBzqYDXAL9Gi4OnIq04nUArFRytG1xBJ3fHkQu_9ccryGKZoArHQo7VwgZKhnmmFObdX23';
        $client = new Client();
        $client->setApiKey($apiKey);
        $client->injectHttpClient(new \GuzzleHttp\Client());

        $note = new Notification($titulo, $mensaje);
        $note->setIcon('Ic_launcher')
            ->setColor('#777777')
            ->setBadge(1)
            ->setSound('default');

        $message = new Message();
        $message->addRecipient(new Device($token));
        $message->setNotification($note)
            ->setData(['idOrden' => $ticket, 'titulo' => $titulo, 'mensaje' => $mensaje]);

        $response = $client->send($message);

      }elseif ($tipo = 3) {
         //Envio de push a Motoristas
        /********* set Key***/
        $apiKey = 'AAAA1OW5Zak:APA91bFjQIKOsDPIJT5bWd6KqCjMxC3IC4a6N-SyYRXopS3K_she1NRZJjbGp35G6oAVHAYBzqYDXAL9Gi4OnIq04nUArFRytG1xBJ3fHkQu_9ccryGKZoArHQo7VwgZKhnmmFObdX23';
        $client = new Client();
        $client->setApiKey($apiKey);
        $client->injectHttpClient(new \GuzzleHttp\Client());

        $note = new Notification($titulo, $mensaje);
        $note->setIcon('Ic_launcher')
            ->setColor('#777777')
            ->setBadge(1)
            ->setSound('default');

        $message = new Message();
        $message->addRecipient(new Device($token));
        $message->setNotification($note)
            ->setData(['idOrden' => $ticket, 'titulo' => $titulo, 'mensaje' => $mensaje]);

        $response = $client->send($message);

      }else{
        //Envio de push a clientes
        $note = Yii::$app->fcm->createNotification($titulo, $mensaje);
        $note->setIcon('Ic_launcher')
            ->setColor('#777777')
            ->setBadge(1)
            ->setSound('default');

        $message = Yii::$app->fcm->createMessage();
        $message->addRecipient(new Device($token));
        $message->setNotification($note)
            ->setData(['ticket' => $ticket, 'titulo' => $titulo, 'mensaje' => $mensaje, 'sound' => $default]);

        $response = Yii::$app->fcm->send($message);
      }
    }

    public function actionIndex()
    {
        $searchModel = new OrdenUsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrdenUsuario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrdenUsuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrdenUsuario();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idordenUsuario]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OrdenUsuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idordenUsuario]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OrdenUsuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrdenUsuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdenUsuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdenUsuario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

      public function actionTicket()
    {
        $searchModel = new OrdenUsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //Se listan los tickets de la ultima semana en estado 1 o sea los tickets de hace 10080 minutos atras
        $dataProvider->query->where('TIMESTAMPDIFF(MINUTE,fechaInicio,NOW()) < 10080')->andWhere('estadoPedido = 1')->orderBy('fechaInicio DESC')->all();

        $iniciadas = OrdenUsuarioIniciadas::find()->all();
        $totalIniciadas = count($iniciadas);

        return $this->render('ticket', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalIniciadas' => $totalIniciadas,
        ]);
    }

    public function actionDetalleOrden()
    {
       $idordenUsuario = $_GET['idorden'];
       //Buscamos los restaurantes por pedido
       $restaurantes = OrdenUsuarioHasSucursal::find()->where(['idordenUsuario' => $idordenUsuario])->all();

       $totalRestaurantes = count($restaurantes);


       if ($totalRestaurantes > 1) 
       {
        $idordenUsuario = $_GET['idorden'];
        $mapa = $_GET['mapa'];


        $orden = OrdenUsuario::find()->where(['idordenUsuario' => $idordenUsuario])->one();

        if ($orden->estadoPedido == 1) {
         

            if ($orden->save()) {
                  $restaurantes = PlatosOrdenesUsuario::find()->where(['idordenUsuario' => $idordenUsuario])->groupBy(['nombreRestaurante'])->all();

                  $platos = PlatosOrdenesUsuario::find()->where(['idordenUsuario' => $idordenUsuario])->all();

                  $factura = Factura::find()->where(['idordenUsuario' => $idordenUsuario])->one();

                  $categoriaExtras = OrdenUsuarioExtras::find()->where(['idordenUsuario' => $idordenUsuario])->groupBy(['idplatos_has_ordenUsuario','nombreCategoriaPlato'])->orderBy('orden ASC')->orderBy('idcategoria_plato ASC')->all();  

                  //$categoriaExtras = OrdenUsuarioExtras::find()->where(['idordenUsuario' => $idordenUsuario])->groupBy(['idplatos_has_ordenUsuario','nombreCategoriaPlato'])->orderBy('orden ASC')->orderBy('idcategoria_plato ASC')->all();

                  //Relacion de pedido con Sucursal
                   $sucursales = OrdenUsuarioHasSucursal::find()->where(['idordenUsuario' => $idordenUsuario])->all();


                  //Sub Total
                  $subtotal = $orden->totalOrden - $orden->tblCobertura->tarifa;

                  //TIEMPO
                  //date_default_timezone_set('America/El_Salvador');
                  //$orden->fechaInicio = '2017-09-20 13:00:00';
                  $tiempo = $orden->fechaInicio;

                  date_default_timezone_set('America/El_Salvador');

                  $tiempo = strtotime ( '+6 hour' , strtotime ( $tiempo ) ) ;
                  $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );
                  //$tiempo = strtotime ( '-21 minutes' , strtotime ( $tiempo ) ) ;
                  //$tiempo = date ( 'Y-m-d H:m:s' , $tiempo );
                  $tiempo = ( $tiempo < 60*60*24*365)
                            ? Yii::$app->formatter->asRelativeTime($tiempo )
                            : Yii::$app->formatter->asDate($tiempo );
                  //$tiempo = date('Y-m-d H:m:s');
                  //$tiempo = $orden->fechaInicio;
                  //Forma de pago
                  if ($orden->idTipoPago == 1) {

                    $tipoPago = '<div class="row">
                                  <div class="col-md-3 text-center">
                                    <h5 class="text-black">Forma de pago: <span class="font24-gmf" style="color: gray;"> '.$orden->tipoPago->nombre.'</span></h5>
                                  </div>
                                  <div class="col-md-3 text-center">
                                      <h5 class="text-black">Cantidad a Cancelar: <span class="font24-gmf" style="color: gray;"> $'.number_format($orden->totalOrden, 2, '.', '').'</span></h5>
                                  </div>
                                  <div class="col-md-3 text-center">
                                      <h5 class="text-black">Efectivo a Recibir: <span class="font24-gmf" style="color: gray;"> $'.number_format($orden->dinero, 2, '.', '').'</span></h5>
                                  </div>
                                  <div class="col-md-3 text-center">
                                      <h5 class="text-black">Cambio: <span class="font24-gmf" style="color: gray;"> $'.number_format($orden->cambio, 2, '.', '').'</span></h5>
                                  </div>
                                </div>';
                  }elseif ($orden->idTipoPago == 2) {
                    $tipoPago = '<div class="row">
                                  <div class="col-md-1">
                                      
                                  </div>
                                  <div class="col-md-5 text-center">
                                    <h5 class="text-black">Forma de pago: <span class="font24-gmf" style="color: gray;"> '.$orden->tipoPago->nombre.'</span></h5>
                                  </div>
                                  <div class="col-md-5 text-center">
                                      <h5 class="text-black">Cantidad a Cancelar: <span class="font24-gmf" style="color: gray;"> $'.number_format($orden->totalOrden, 2, '.', '').'</span></h5>
                                  </div>
                                  <div class="col-md-1">
                                      
                                  </div>
                                </div>';

                  }
                  $tipoPago .= '<div class="row">
                                  <div class="col-md-2"></div>
                                  <div class="col-md-10">
                                  Factura a: ';

                  if ($factura->nombre != '0') {
                    $tipoPago .= $factura->nombre.'</div>
                                </div>';
                  }else{
                   $tipoPago .= 'Factura no Solicitada </div>
                                </div>';
                  }
                  //ORDEN TEMPLATE

                  $i = 0;
                  $j = 0;
                  
                  $result = "";
                  $sucursal_orden ="";
                  $sucursal_dispositivo="";
                  foreach ($restaurantes as $restaurante) {
                  $result .= '<div class="col-md-offset-1 col-md-11">
                                  <h1>'.$restaurantes[$i]->nombreRestaurante.'</h1>
                              </div>';
                              //<div class="col-md-5">';
                   foreach ($sucursales as $sucursal) {
                            if ($sucursal->id_restaurante ==  $restaurantes[$i]->id_restaurante) {

                              $sucursal_orden = $sucursal->idsucursal;
                              $sucursal_dispositivo = $sucursal->idsucursal0->Dispositivo;
                            }
                          }

                  if ($sucursal_dispositivo == 0) {
                      /*$result .= '<div class="form-group">
                                    <label class="control-label" for="tiempo">Tiempo</label>
                                      <select id="tiempo" class="form-control" name="tiempo" onchange="getTiempo('.$orden->idordenUsuario.','.$sucursal_orden.');">
                                        <option value="0">Seleccione</option>
                                        <option value="15">15 Minutos</option>
                                        <option value="20">20 Minutos</option>
                                        <option value="30">30 Minutos</option>
                                        <option value="40">40 Minutos</option>
                                      </select>
                                  </div>';
                    */
                  }else{
                        /*
                        $result .= '<div class="form-group">
                                   <h1 style="color: white;">Si tiene dispositivvo</h1>
                                 </div>';
                        */
 
                  }

                  //$result .='</div>';
                    foreach ($platos as $plato) {
                        if ($plato->nombreRestaurante == $restaurantes[$i]->nombreRestaurante) {
                          $result .= '<div class="col-md-3">
                                        <h6 class="text-black text-center"><strong class="text-grey-gmf">Cant.</strong></h6>
                                        <h3 class="text-black text-center">'.$plato->cantidadPlato.'</h3>
                                      </div>
                                      <div class="col-md-7">
                                        <h6 class="text-black text-left"><strong class="text-grey-gmf">Producto</strong></h6>
                                        <h3>'.$plato->nombrePlato.'</h3>';
                        
						
						//$categoriaExtras = OrdenUsuarioExtras::find()->where(['idplatos_has_ordenUsuario' => $plato->idplatos_has_ordenUsuario])->groupBy(['idplatos_has_ordenUsuario','nombreCategoriaPlato'])->orderBy('orden ASC')->orderBy('idcategoria_plato ASC')->all();
                          foreach($categoriaExtras as $categoriaExtra){  
                            
                            if($plato->idplatos_has_ordenUsuario == $categoriaExtra->idplatos_has_ordenUsuario ){
                              
							  
                              $result .= ' <h3 style="color: black;background-color: rgb(243, 243, 243);padding: 10px 10px 10px 10px;"><strong>'.$categoriaExtra->nombreCategoriaPlato.'</strong></h3>';
                              //$extras = OrdenUsuarioExtras::find()->where(['idplatos_has_ordenUsuario' => $categoriaExtra->idplatos_has_ordenUsuario])->orderBy('ordenExtra ASC')->orderBy('idextra ASC')->all();
							  
                               $extras = OrdenUsuarioExtras::find()->where(['idplatos_has_ordenUsuario' => $plato->idplatos_has_ordenUsuario])->orderBy('ordenExtra ASC')->orderBy('idextra ASC')->all();
                              
							  
							  foreach ($extras as $extra) {
                                if ($categoriaExtra->idcategoria_plato == $extra->idcategoria_plato && $extra->idplatos_has_ordenUsuario == $plato->idplatos_has_ordenUsuario) {
                                  $result .= ' <h4 style="padding-left: 20px;">'.$extra->nombreExta.'</h4>';
                                }
                              }
                            }
                          }  
                            
                          $result .= '</div>
                                      <div class="col-md-2">
                                        <h6 class="text-black text-left"><strong class="text-grey-gmf">Precio</strong></h6> 
                                        <h3>$'.number_format(((number_format($plato->precioPlato, 2, '.', ''))*($plato->cantidadPlato)), 2, '.', '').'</h3>';
                          
                          foreach($categoriaExtras as $categoriaExtra){
                            
                            if($plato->idplatos_has_ordenUsuario == $categoriaExtra->idplatos_has_ordenUsuario ){
                              
                              $result .= '<h3 style="padding:  35px 10px 10px 10px;"><strong></strong></h3>';
                              
							  
							  
                              foreach ($extras as $extra) {
                                if ($categoriaExtra->idcategoria_plato == $extra->idcategoria_plato && $extra->idplatos_has_ordenUsuario == $plato->idplatos_has_ordenUsuario) {
                                  $result .= ' <h4>$ '.number_format($extra->precioExtra, 2, '.', '').'</h4>';
                                }
                              }
                            }
                          }




                          /*foreach ($extras as $extra) {
                            if ($extra->idplatos_has_ordenUsuario == $plato->idplatos_has_ordenUsuario) {
                                  $result .= ' <h4>$ '.number_format($extra->precioExtra, 2, '.', '').'</h4>';
                            }
                            
                          }*/               

                          $result .= '</div>
                                      <div class="col-md-12" style="background-color: rgba(238, 238, 238, 0.58);;">
                                        <h5 class="col-md-offset-1 col-md-11 text-grey-gmf">Comentarios del plato:<span class="font20-gmf text-black"> '.utf8_decode($plato->comentarios).'</span></h5>
                                      </div>';
                          }
                        $j++;
                      } 
                      $result .='<div class="col-md-6">
                                  <div class="box-footer">
                                      <button id="'.$restaurantes[$i]->id_restaurante.'" type="submit" class="btn btn-success center-block" data-toggle="modal" data-target="#modal-default'.$restaurantes[$i]->id_restaurante.'">Enviar a Restaurante</button>
                                  </div>
                                 </div>
                                 <div class="col-md-6">
                                  <div class="box-footer">
                                      <button id="rechazar-'.$restaurantes[$i]->id_restaurante.'" type="submit" class="btn btn-warning center-block" data-toggle="modal" data-target="#modal-rechazar'.$restaurantes[$i]->id_restaurante.'">Rechazar Orden</button>
                                  </div>
                                 </div>';
                      //Gereneramos modal para restaurante
                       foreach ($sucursales as $sucursal) {
                            if ($sucursal->id_restaurante ==  $restaurantes[$i]->id_restaurante) {
                              echo '<div class="modal fade" id="modal-default'.$restaurantes[$i]->id_restaurante.'">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header-gmf">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title"></h4>
                                          </div>
                                          <div class="modal-body">
                                            <h3 class="text-center">&iquest;Deseas enviar esta orden al Restaurante?</h3>
                                          </div>
                                          <div class="modal-footer-gmf">
                                            <button type="button" class="btn btn-red-gmf" onclick="aceptarRestaurante('.$sucursal->idordenUsuario.','.$sucursal->idsucursal.','.$restaurantes[$i]->id_restaurante.')">Aceptar</button>
                                            <button type="button" class="btn btn-red-gmf" data-dismiss="modal" onclick="cerrarModal('.$restaurantes[$i]->id_restaurante.')">Cancelar</button> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>' ; 

                                 echo '<div class="modal fade" id="modal-rechazar'.$restaurantes[$i]->id_restaurante.'">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header-gmf">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title"></h4>
                                          </div>
                                          <div class="modal-body">
                                            <h3 class="text-center">&iquest;Deseas rechazar esta orden a Restaurante?</h3>
                                          </div>
                                          <div class="modal-footer-gmf">
                                            <button type="button" class="btn btn-red-gmf" data-dismiss="modal" onclick="rechazarRestaurante('.$sucursal->idordenUsuario.','.$sucursal->idsucursal.','.$restaurantes[$i]->id_restaurante.')">Aceptar</button>
                                            <button type="button" class="btn btn-red-gmf" data-dismiss="modal" onclick="cerrarModal('.$restaurantes[$i]->id_restaurante.')">Cancelar</button> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>' ; 
                            }
                          }   
                      $i++;
                  }

                  //$idordenUsuario = "'".$idordenUsuario."'";

                  echo '<div class="box-header with-border">
                        <h2 class="box-title"><span style="color: gray;">Ticket: </span><strong class="uppercase-text font24-gmf">'.$orden->ticket.'</strong></h2>
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <h2 class="box-title"><span style="color: gray;">Hora del Pedido: '.$orden->fechaInicio.'</span><strong class="uppercase-text font24-gmf"></strong></h2>

                        <div class="box-tools pull-right">
                         
                          <button type="button" class="btn btn-box-tool" onclick="cerrarOrden('.$idordenUsuario.')"><i class="fa fa-times"></i></button>
                        </div>
                        </div>
                          <div class="mailbox-messages container-fluid">
                            <div class="row">
                                <div id="map">
                                <iframe
                                      width="'.$mapa.'" 
                                      height="200"
                                      frameborder="0" style="border:0"
                                      src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCYkhtnCIFD6yWm3v1V78OKIwmUaPQqNvk&q='.$orden->direccion->latitud.','.$orden->direccion->longitud.'" allowfullscreen>
                                    </iframe>
                                </div>
                              </div>
                              <div class="row" id="ordenAceptada" style="display:none;">
                                <div class="box-body" id="alerta">
                                   <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                      <h4><i class="icon fa fa-info"></i>Alerta!</h4>
                                      Orden Aceptada
                                   </div>
                                </div>
                              </div>
                                <div class="row"  id="ordenRechazada" style="background-color: rgba(238, 10, 10, 0.58)!important; display:none;">
                                 <h4>Orden Rehazada</h4>
                              </div>
                                <div>
                                  '.$result.'
                                </div>
                                <div class="col-md-10">
                                    <h4 class="text-black text-right">Sub-Total:</h4>
                                    <h4 class="text-black text-right">Costo de Envío:</h4>
                                    <h4 class="text-green-gmf text-right"><strong>Total:</strong></h4>
                                </div>
                                <div class="col-md-2">
                                    <h4 class="text-black text-left">$'.number_format($subtotal, 2, '.', '').'</h4>
                                    <h4 class="text-black text-left">$'.number_format($orden->tblCobertura->tarifa, 2, '.', '').'</h4>
                                    <h4 class="text-green-gmf text-left"><strong> $'.number_format($orden->totalOrden, 2, '.', '').'</strong></h4>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            '.$tipoPago.'
                            
                            <div class="row" style="background-color: #eee;">
                              <div class="col-md-4 text-center">
                                <h5 style="color: red;">Cliente: <span class="font20-gmf" style="color: black;">
                                '.htmlspecialchars(utf8_decode($orden->idusuarioApp->name)).' '.htmlspecialchars(utf8_decode($orden->idusuarioApp->last_name)).'</span></h5>
                              </div>
                              <div class="col-md-4 text-center">
                                <h5 style="color: red;">Telefonos: <span class="font20-gmf" style="color: black;">
                                '.$orden->idusuarioApp->phone.' - '.$orden->idusuarioApp->mobile.' </span></h5>
                              </div>
                              <div class="col-md-4 text-center">
                                <h5 style="color: red;">Tiempo de Entrega: <span class="font20-gmf" style="color: black;">'.$orden->tiempoOrden.' minutos</span></h5>
                              </div>
                            
                                <div class="col-md-offset-3 col-md-9 text-left">
                                    <strong style="font-size: 20px;">  '.htmlspecialchars(utf8_decode($orden->direccion->direccion)).'</strong>
                                </div>
                                 <div class="col-md-offset-3 col-md-9 text-left">
                                    <strong style="font-size: 20px;">Comentarios:  '.htmlspecialchars(utf8_decode($orden->descripcion)).'</strong>
                                </div>
                            </div>
                          </div>';
                     echo ' <div class="row" id="botonAceptar" style="background-color: rgba(238, 238, 238,0.58)!important;display: none;">
                            <div class="box-footer" style="background-color: rgba(238, 238, 238, 0.58)!important;">
                                <button type="submit" class="btn btn-red-gmf center-block" onclick="tomarOrden('.$idordenUsuario.')">Tomar Orden</button>
                            </div>
                          </div>
                          <div class="row" id="botonCancelar" style="display: none!;">
                          <div class="box-footer text-center" style="background-color: rgba(238, 238, 238, 0.58)!important;">
                            <button type="button" class="btn btn-red-gmf center-block" onclick="rechazarOrden('.$idordenUsuario.')"><i class="glyphicon glyphicon-remove" style="top:2px!important;"></i> Cancelar</button>
                              
                          </div>
                        </div>

                        <div class="row" id="botonOtraOrden" style="display: none;">
                          <div class="col-md-12 text-center">
                            <a href="index.php?r=orden-usuario/ticket" class="btn btn-red-gmf">Tomar otra orden</a>
                          </div>
                        </div>';                  
            }
                         
           }else{

             echo '
                <div class="box-header with-border">
                    <div class="box-tools pull-right">                      
                      <button type="button" class="btn btn-box-tool" onclick="cerrarOrden('.$idordenUsuario.')"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="mailbox-messages container-fluid">
                  <div class="row">
                    <div id="map">
                      <h2 class="text-center">Esta orden ya ha sido seleccionada por otro Operador</h2>
                    </div>
                  </div>
                </div>';
                
           }

        //Creamos el nuevo registro del historial
        $historial = new Historialpedido();

        //Ingresamos lo datos al historial
        $historial->idordenUsuario = $orden->idordenUsuario;
        $historial->idusuario = \Yii::$app->user->id;
        $historial->idtipousuario = 2;
        $historial->idestadoanterior = $orden->estadoPedido;
        $historial->idestadonuevo = 13;

        //Cambiamos estado de pedido a Abierto para que ningun otro usuario pueda tomarlo.
        $orden->estadoPedido = 13;
        $orden->save();

        //Registramos el id del teleoperador
        $orden->idteleoperador = \Yii::$app->user->id;

        //Actualiamos la orden con los nuevos datos
        $orden->update();

        $historial->save();
       }else{
        //Orden cuando solo hay un restaurante
        $idordenUsuario = $_GET['idorden'];
        $mapa = $_GET['mapa'];


        $orden = OrdenUsuario::find()->where(['idordenUsuario' => $idordenUsuario])->one();

        if ($orden->estadoPedido == 1) {
         

            if ($orden->save()) {
                  $restaurantes = PlatosOrdenesUsuario::find()->where(['idordenUsuario' => $idordenUsuario])->groupBy(['nombreRestaurante'])->all();

                  $platos = PlatosOrdenesUsuario::find()->where(['idordenUsuario' => $idordenUsuario])->all();

                  $factura = Factura::find()->where(['idordenUsuario' => $idordenUsuario])->one();

                  //$extras = OrdenUsuarioExtras::find()->where(['idplatos_has_ordenUsuario' => $plato->idplatos_has_ordenUsuario])->orderBy('ordenExtra ASC')->orderBy('idextra ASC')->all();

                  $categoriaExtras = OrdenUsuarioExtras::find()->where(['idordenUsuario' => $idordenUsuario])->groupBy(['idplatos_has_ordenUsuario','nombreCategoriaPlato'])->orderBy('orden ASC')->orderBy('idcategoria_plato ASC')->all();

                  //Relacion de pedido con Sucursal
                   $sucursales = OrdenUsuarioHasSucursal::find()->where(['idordenUsuario' => $idordenUsuario])->all();


                  //Sub Total
                  $subtotal = $orden->totalOrden - $orden->tblCobertura->tarifa;

                  //TIEMPO
                  //date_default_timezone_set('America/El_Salvador');
                  //$orden->fechaInicio = '2017-09-20 13:00:00';
                  $tiempo = $orden->fechaInicio;

                  date_default_timezone_set('America/El_Salvador');

                  $tiempo = strtotime ( '+6 hour' , strtotime ( $tiempo ) ) ;
                  $tiempo = date ( 'Y-m-d H:m:s' , $tiempo );
                  //$tiempo = strtotime ( '-21 minutes' , strtotime ( $tiempo ) ) ;
                  //$tiempo = date ( 'Y-m-d H:m:s' , $tiempo );
                  $tiempo = ( $tiempo < 60*60*24*365)
                            ? Yii::$app->formatter->asRelativeTime($tiempo )
                            : Yii::$app->formatter->asDate($tiempo );
                  //$tiempo = date('Y-m-d H:m:s');
                  //$tiempo = $orden->fechaInicio;
                  //Forma de pago
                  if ($orden->idTipoPago == 1) {

                    $tipoPago = '<div class="row">
                                  <div class="col-md-3 text-center">
                                    <h5 class="text-black">Forma de pago: <span class="font24-gmf" style="color: gray;"> '.$orden->tipoPago->nombre.'</span></h5>
                                  </div>
                                  <div class="col-md-3 text-center">
                                      <h5 class="text-black">Cantidad a Cancelar: <span class="font24-gmf" style="color: gray;"> $'.number_format($orden->totalOrden, 2, '.', '').'</span></h5>
                                  </div>
                                  <div class="col-md-3 text-center">
                                      <h5 class="text-black">Efectivo a Recibir: <span class="font24-gmf" style="color: gray;"> $'.number_format($orden->dinero, 2, '.', '').'</span></h5>
                                  </div>
                                  <div class="col-md-3 text-center">
                                      <h5 class="text-black">Cambio: <span class="font24-gmf" style="color: gray;"> $'.number_format($orden->cambio, 2, '.', '').'</span></h5>
                                  </div>
                                </div>';
                  }elseif ($orden->idTipoPago == 2) {
                    $tipoPago = '<div class="row">
                                  <div class="col-md-1">
                                      
                                  </div>
                                  <div class="col-md-5 text-center">
                                    <h5 class="text-black">Forma de pago: <span class="font24-gmf" style="color: gray;"> '.$orden->tipoPago->nombre.'</span></h5>
                                  </div>
                                  <div class="col-md-5 text-center">
                                      <h5 class="text-black">Cantidad a Cancelar: <span class="font24-gmf" style="color: gray;"> $'.number_format($orden->totalOrden, 2, '.', '').'</span></h5>
                                  </div>
                                  <div class="col-md-1">
                                      
                                  </div>
                                </div>';

                  }
                  $tipoPago .= '<div class="row">
                                  <div class="col-md-2"></div>
                                  <div class="col-md-10">
                                  Factura a: ';

                  if ($factura->nombre != '0') {
                    $tipoPago .= $factura->nombre.'</div>
                                </div>';
                  }else{
                   $tipoPago .= 'Factura no Solicitada </div>
                                </div>';
                  }
                  //ORDEN TEMPLATE

                  $i = 0;
                  $j = 0;
                  
                  /*$result = "";
                  foreach ($restaurantes as $restaurante) {
                  $result .= '<div class="col-md-offset-1 col-md-11">
                                  <h1>'.$restaurantes[$i]->nombreRestaurante.'</h1>
                              </div>';*/
                  $result = "";
                  $sucursal_orden ="";
                  $sucursal_dispositivo = "";
                  foreach ($restaurantes as $restaurante) {
                  $result .= '<div class="col-md-offset-1 col-md-11">
                                  <h1>'.$restaurantes[$i]->nombreRestaurante.'</h1>
                              </div>';
                             // <div class="col-md-5">';
                    foreach ($sucursales as $sucursal) {
                            if ($sucursal->id_restaurante ==  $restaurantes[$i]->id_restaurante) {

                              $sucursal_orden = $sucursal->idsucursal; 
                              $sucursal_dispositivo = $sucursal->idsucursal0->Dispositivo;
                            }
                          }

                  if ($sucursal_dispositivo == 0) {
                    /*
                      $result .= '<div class="form-group">
                                    <label class="control-label" for="tiempo">Tiempo</label>
                                      <select id="tiempo" class="form-control" name="tiempo" onchange="getTiempo('.$orden->idordenUsuario.','.$sucursal_orden.');">
                                        <option value="0">Seleccione</option>
                                        <option value="15">15 Minutos</option>
                                        <option value="20">20 Minutos</option>
                                        <option value="30">30 Minutos</option>
                                        <option value="40">40 Minutos</option>
                                      </select>
                                  </div>';
                      */
                  }else{
                    /*
                    $result .= '<div class="form-group">
                                <h1 style="color: white;">Si tiene dispositivvo</h1>
                               </div>';
                    */
                  }

                 // $result .='</div>';
                    foreach ($platos as $plato) {
                        if ($plato->nombreRestaurante == $restaurantes[$i]->nombreRestaurante) {
                          $result .= '<div class="col-md-3">
                                        <h6 class="text-black text-center"><strong class="text-grey-gmf">Cant.</strong></h6>
                                        <h3 class="text-black text-center">'.$plato->cantidadPlato.'</h3>
                                      </div>
                                      <div class="col-md-7">
                                        <h6 class="text-black text-left"><strong class="text-grey-gmf">Producto</strong></h6>
                                        <h3>'.$plato->nombrePlato.'</h3>';
										
                        
                          
						  foreach($categoriaExtras as $categoriaExtra){
                            
                            if($plato->idplatos_has_ordenUsuario == $categoriaExtra->idplatos_has_ordenUsuario ){ 
                              
                              $result .= ' <h3 style="color: black;background-color: rgb(243, 243, 243);padding: 10px 10px 10px 10px;"><strong>'.$categoriaExtra->nombreCategoriaPlato.'</strong></h3>';
                              
							  $extras = OrdenUsuarioExtras::find()->where(['idplatos_has_ordenUsuario' => $plato->idplatos_has_ordenUsuario])->orderBy('ordenExtra ASC')->orderBy('idextra ASC')->all();
                              
							  
							  foreach ($extras as $extra) {
                                if ($categoriaExtra->idcategoria_plato == $extra->idcategoria_plato && $extra->id_plato_movil == $plato->plato->id_plato_movil) {
                                  $result .= ' <h4 style="padding-left: 20px;">'.$extra->nombreExta.'</h4>';
                                }
                              }
                            }
                          }  
                            
                          $result .= '</div>
                                      <div class="col-md-2">
                                        <h6 class="text-black text-left"><strong class="text-grey-gmf">Precio</strong></h6> 
                                        <h3>$'.number_format(((number_format($plato->precioPlato, 2, '.', ''))*($plato->cantidadPlato)), 2, '.', '').'</h3>';
                          
                          foreach($categoriaExtras as $categoriaExtra){
                            
                            if($plato->idplatos_has_ordenUsuario == $categoriaExtra->idplatos_has_ordenUsuario ){
                              
                              $result .= '<h3 style="padding:  35px 10px 10px 10px;"><strong></strong></h3>';
                              
                              foreach ($extras as $extra) {
                                if ($categoriaExtra->idcategoria_plato == $extra->idcategoria_plato && $extra->idplatos_has_ordenUsuario == $plato->idplatos_has_ordenUsuario) {
                                  $result .= ' <h4>$ '.number_format($extra->precioExtra, 2, '.', '').'</h4>';
                                }
                              }
                            }
                          }




                          /*foreach ($extras as $extra) {
                            if ($extra->idplatos_has_ordenUsuario == $plato->idplatos_has_ordenUsuario) {
                                  $result .= ' <h4>$ '.number_format($extra->precioExtra, 2, '.', '').'</h4>';
                            }
                            
                          }*/               

                          $result .= '</div>
                                      <div class="col-md-12" style="background-color: rgba(238, 238, 238, 0.58);;">
                                        <h5 class="col-md-offset-1 col-md-11 text-grey-gmf">Comentarios del plato:<span class="font20-gmf text-black"> '.utf8_decode($plato->comentarios).'</span></h5>
                                      </div>';
                          }
                        $j++;
                      } 
                      $result .='<div class="col-md-6">
                                  <div class="box-footer">
                                      <button id="'.$restaurantes[$i]->id_restaurante.'" type="submit" class="btn btn-success center-block" data-toggle="modal" data-target="#modal-default'.$restaurantes[$i]->id_restaurante.'">Enviar a Restaurante</button>
                                  </div>
                                 </div>
                                 <div class="col-md-6">
                                  <div class="box-footer">
                                      <button id="rechazar-'.$restaurantes[$i]->id_restaurante.'" type="submit" class="btn btn-warning center-block" data-toggle="modal" data-target="#modal-rechazar'.$restaurantes[$i]->id_restaurante.'">Rechazar Orden</button>
                                  </div>
                                 </div>';
                      //Gereneramos modal para restaurante
                       foreach ($sucursales as $sucursal) {
                            if ($sucursal->id_restaurante ==  $restaurantes[$i]->id_restaurante) {
                              echo '<div class="modal fade" id="modal-default'.$restaurantes[$i]->id_restaurante.'">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header-gmf">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title"></h4>
                                          </div>
                                          <div class="modal-body">
                                            <h3 class="text-center">&iquest;Deseas enviar esta orden al Restaurante?</h3>
                                          </div>
                                          <div class="modal-footer-gmf">
                                            <button type="button" class="btn btn-red-gmf" onclick="aceptarRestaurante1('.$sucursal->idordenUsuario.','.$sucursal->idsucursal.','.$restaurantes[$i]->id_restaurante.')">Aceptar</button>
                                            <button type="button" class="btn btn-red-gmf" data-dismiss="modal" onclick="cerrarModal('.$restaurantes[$i]->id_restaurante.')">Cancelar</button> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>' ; 

                                 echo '<div class="modal fade" id="modal-rechazar'.$restaurantes[$i]->id_restaurante.'">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header-gmf">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title"></h4>
                                          </div>
                                          <div class="modal-body">
                                            <h3 class="text-center">&iquest;Deseas rechazar esta orden a Restaurante?</h3>
                                          </div>
                                          <div class="modal-footer-gmf">
                                            <button type="button" class="btn btn-red-gmf" data-dismiss="modal" onclick="rechazarRestaurante1('.$sucursal->idordenUsuario.','.$sucursal->idsucursal.','.$restaurantes[$i]->id_restaurante.')">Aceptar</button>
                                            <button type="button" class="btn btn-red-gmf" data-dismiss="modal" onclick="cerrarModal('.$restaurantes[$i]->id_restaurante.')">Cancelar</button> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>' ; 
                            }
                          }   
                      $i++;
                  }

                  //$idordenUsuario = "'".$idordenUsuario."'";

                  echo '<div class="box-header with-border">
                        <h2 class="box-title"><span style="color: gray;">Ticket: </span><strong class="uppercase-text font24-gmf">'.$orden->ticket.'</strong></h2>
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <h2 class="box-title"><span style="color: gray;">Hora del Pedido: '.$tiempo.' '.$orden->fechaInicio.' '.date("Y-m-d H:i:s").'</span><strong class="uppercase-text font24-gmf"></strong></h2>

                        <div class="box-tools pull-right">
                         
                          <button type="button" class="btn btn-box-tool" onclick="cerrarOrden('.$idordenUsuario.')"><i class="fa fa-times"></i></button>
                        </div>
                        </div>
                          <div class="mailbox-messages container-fluid">
                            <div class="row">
                                <div id="map">
                                <iframe
                                      width="'.$mapa.'" 
                                      height="200"
                                      frameborder="0" style="border:0"
                                      src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCYkhtnCIFD6yWm3v1V78OKIwmUaPQqNvk&q='.$orden->direccion->latitud.','.$orden->direccion->longitud.'" allowfullscreen>
                                    </iframe>
                                </div>
                              </div>
                              <div class="row" id="ordenAceptada" style="display:none;">
                                <div class="box-body" id="alerta">
                                   <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                      <h4><i class="icon fa fa-info"></i>Alerta!</h4>
                                      Orden Aceptada
                                   </div>
                                </div>
                              </div>
                                <div class="row"  id="ordenRechazada" style="background-color: rgba(238, 10, 10, 0.58)!important; display:none;">
                                 <h4>Orden Rehazada</h4>
                              </div>
                                <div>
                                  '.$result.'
                                </div>
                                <div class="col-md-10">
                                    <h4 class="text-black text-right">Sub-Total:</h4>
                                    <h4 class="text-black text-right">Costo de Envío:</h4>
                                    <h4 class="text-green-gmf text-right"><strong>Total:</strong></h4>
                                </div>
                                <div class="col-md-2">
                                    <h4 class="text-black text-left">$'.number_format($subtotal, 2, '.', '').'</h4>
                                    <h4 class="text-black text-left">$'.number_format($orden->tblCobertura->tarifa, 2, '.', '').'</h4>
                                    <h4 class="text-green-gmf text-left"><strong> $'.number_format($orden->totalOrden, 2, '.', '').'</strong></h4>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            '.$tipoPago.'
                            
                            <div class="row" style="background-color: #eee;">
                              <div class="col-md-4 text-center">
                                <h5 style="color: red;">Cliente: <span class="font20-gmf" style="color: black;">
                                '.htmlspecialchars(utf8_decode($orden->idusuarioApp->name)).' '.htmlspecialchars(utf8_decode($orden->idusuarioApp->last_name)).'</span></h5>
                              </div>
                              <div class="col-md-4 text-center">
                                <h5 style="color: red;">Telefonos: <span class="font20-gmf" style="color: black;">
                                '.$orden->idusuarioApp->phone.' - '.$orden->idusuarioApp->mobile.' </span></h5>
                              </div>
                              <div class="col-md-4 text-center">
                                <h5 style="color: red;">Tiempo de Entrega: <span class="font20-gmf" style="color: black;">'.$orden->tiempoOrden.' minutos</span></h5>
                              </div>
                            
                                <div class="col-md-offset-3 col-md-9 text-left">
                                    <strong style="font-size: 20px;">  '.htmlspecialchars(utf8_decode($orden->direccion->direccion)).'</strong>
                                </div>
                                 <div class="col-md-offset-3 col-md-9 text-left">
                                    <strong style="font-size: 20px;">Comentarios:  '.htmlspecialchars(utf8_decode($orden->descripcion)).'</strong>
                                </div>
                            </div>
                          </div>';
                     echo ' <div class="row" id="botonAceptar" style="background-color: rgba(238, 238, 238,0.58)!important;display: none;">
                            <div class="box-footer" style="background-color: rgba(238, 238, 238, 0.58)!important;">
                                <button type="submit" class="btn btn-red-gmf center-block" onclick="tomarOrden('.$idordenUsuario.')">Tomar Orden</button>
                            </div>
                          </div>
                          <div class="row" id="botonCancelar" style="display: none;">
                          <div class="box-footer text-center" style="background-color: rgba(238, 238, 238, 0.58)!important;">
                            <button type="button" class="btn btn-red-gmf center-block" onclick="rechazarOrden('.$idordenUsuario.')"><i class="glyphicon glyphicon-remove" style="top:2px!important;"></i> Cancelar</button>
                              
                          </div>
                        </div>

                        <div class="row" id="botonOtraOrden" style="display: none;">
                          <div class="col-md-12 text-center">
                            <a href="index.php?r=orden-usuario/ticket" class="btn btn-red-gmf">Tomar otra orden</a>
                          </div>
                        </div>';                  
            }
                         
           }else{

             echo '
                <div class="box-header with-border">
                    <div class="box-tools pull-right">                      
                      <button type="button" class="btn btn-box-tool" onclick="cerrarOrden('.$idordenUsuario.')"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="mailbox-messages container-fluid">
                  <div class="row">
                    <div id="map">
                      <h2 class="text-center">Esta orden ya ha sido seleccionada por otro Operador</h2>
                    </div>
                  </div>
                </div>';
                
           }

        //Creamos el nuevo registro del historial
        $historial = new Historialpedido();

        //Ingresamos lo datos al historial
        $historial->idordenUsuario = $orden->idordenUsuario;
        $historial->idusuario = \Yii::$app->user->id;
        $historial->idtipousuario = 2;
        $historial->idestadoanterior = $orden->estadoPedido;
        $historial->idestadonuevo = 13;

        //Cambiamos estado de pedido a Abierto para que ningun otro usuario pueda tomarlo.
        $orden->estadoPedido = 13;
        $orden->save();

        //Registramos el id del teleoperador
        $orden->idteleoperador = \Yii::$app->user->id;

        //Actualiamos la orden con los nuevos datos
        $orden->update();

        $historial->save();
       }
    }

    public function actionRestablecerOrden()
    {

        $idordenUsuario = $_GET['id'];

        $orden = OrdenUsuario::find()->where(['idordenUsuario' => $idordenUsuario])->one();
        $orden->estadoPedido = 1;
        $orden->update();
    }

    public function actionAceptarOrden()
    {

        $idordenUsuario = $_GET['id'];

        //Creamos el nuevo registro del historial
        $historial = new Historialpedido();

        $orden = OrdenUsuario::find()->where(['idordenUsuario' => $idordenUsuario])->one();

        //Ingresamos lo datos al historial
        $historial->idordenUsuario = $idordenUsuario;
        $historial->idusuario = \Yii::$app->user->id;
        $historial->idtipousuario = 2;
        $historial->idestadoanterior = $orden->estadoPedido;
        $historial->idestadonuevo = 3;

        //Actualizamos el estado del pedido a Recibido por teleoperador
        $orden->estadoPedido = 3;

        //Registramos el id del teleoperador
        $orden->idteleoperador = \Yii::$app->user->id; 

        //Actualiamos la orden con los nuevos datos
        $orden->update();

        $historial->save();

        //Enviar push cuando se rechaza la orden
        $modelOrden = OrdenUsuario::findOne($idordenUsuario);
        $token = $modelOrden->idusuarioApp->tokenPush;

        $this->actionPush('Orden Aceptada', 'Su orden ha sido aceptada por el teleoperador', $token, $modelOrden->ticket, 1);
    }

    public function actionRechazarOrden()
    {

        $idordenUsuario = $_GET['id'];

        $orden = OrdenUsuario::find()->where(['idordenUsuario' => $idordenUsuario])->one();

        //Creamos el nuevo registro del historial
        $historial = new Historialpedido();


         //Ingresamos lo datos al historial
        $historial->idordenUsuario = $idordenUsuario;
        $historial->idusuario = \Yii::$app->user->id;
        $historial->idtipousuario = 2;
        $historial->idestadoanterior = $orden->estadoPedido;
        $historial->idestadonuevo = 15;

        //Actualizamos el estado del pedido
        $orden->estadoPedido = 15;
        $orden->update();

        //Guardamos el registro del historial
        $historial->save();

        //Enviar push cuando se rechaza la orden
        $modelOrden = OrdenUsuario::findOne($idordenUsuario);
        $token = $modelOrden->idusuarioApp->tokenPush;

        $this->actionPush('Orden Rechazada', '¡Hola! Te informamos que tu orden fue rechazada, en un momento uno de nuestros asesores de servicio al cliente se pondrá en contacto.', $token, $modelOrden->ticket, 1);
    }

    public function actionAceptarRestaurante()
    {

        $model = new OrdenUsuarioSucursal();
        
        $idSucursal = $_GET['id'];
        $idOrden = $_GET['orden'];
        $idRestaurante = $_GET['restaurante'];

        $model->idOrdenUsuario = $idOrden;
        $model->idSucursal = $idSucursal;
        //Se acepta por 
        $model->idestado = 6;

        try {
          $model->save();
          echo "Success";
        } catch (Exception $e) {
          echo $e;
          echo "error";
        }

         //Actualizamos la fecha de envio a restaurante
         $Orden = $this->findModel($idOrden);
         $Orden->update_at = date("Y-m-d H:i:s");

         $Orden->save();


        //Enviar push cuando se Acepte la orden
        $modelOrden = Sucursal::findOne($idSucursal);
        $token = $modelOrden->tokenPush;

        $this->actionPush('Nueva Orden Recibida', 'Una nueva orden ha sido enviada al restaurante por el Teleoperador', $token, $idOrden, 2);
    }

    public function actionRechazarRestaurante()
    {

        $model = new OrdenUsuarioSucursal();
        
        $idSucursal = $_GET['id'];
        $idOrden = $_GET['orden'];
        $idRestaurante = $_GET['restaurante'];

        //instanciamos el restaurante
        $restaurante = TblRestaurantes::findOne($idRestaurante);

        $model->idOrdenUsuario = $idOrden;
        $model->idSucursal = $idSucursal;

        //Se rechaza por OPerador el pedido 
        $model->idestado = 16;

        echo print_r($model);
        try {
          $model->save();
          echo "Success";
        } catch (Exception $e) {
          echo $e;
          echo "error";
        }

        //Enviar push cuando se rechaza la orden
        $modelOrden = OrdenUsuario::findOne($idOrden);
        $token = $modelOrden->idusuarioApp->tokenPush;

        $this->actionPush('Su orden a '.$restaurante->nombre.' fue rechazada!', 'En un momento uno de nuestros teleoperadores se pondrá en contacto con usted.', $token, $idOrden, 1);
    }

    public function actionOrdenesIniciadas()
    {

      $iniciadas = OrdenUsuarioIniciadas::find()->all();
      $totalIniciadas = count($iniciadas);

      $response = '
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-calendar-check-o"></i>
                    <span class="label label-success">'.$totalIniciadas.'</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">'.$totalIniciadas.' Ordenes iniciadas</li>
                    <li>
                    <!-- inner menu: contains the actual data -->
                      <ul class="menu">';                     
                        
      foreach ($iniciadas as $iniciada) {
          
          $tiempo = $iniciada->orden->fechaInicio;

          $tiempo = strtotime ( '+6 hour' , strtotime ( $tiempo ) ) ;
          $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );
          //$tiempo = strtotime ( '-21 minutes' , strtotime ( $tiempo ) ) ;
          //$tiempo = date ( 'Y-m-d H:m:s' , $tiempo );
          $tiempo = ( $tiempo < 60*60*24*365)? Yii::$app->formatter->asRelativeTime($tiempo ): Yii::$app->formatter->asDate($tiempo );

        $response.= '<li>
                <a href="index.php?r=orden-usuario/ticket">
                  <i class="fa fa-calendar-check-o text-yellow"></i> Ticket: '.$iniciada->orden->ticket.' - '.$tiempo.'.
                </a>
              </li>';
      }

      $response .= '</ul>
                    </li>                 
                  </ul>';

      echo $response;
    }

    public function actionOrdenesRechazadas()
    {

      $rechazadas = HistorialRechazadoRestaurante::find()->where('TIMESTAMPDIFF(MINUTE,fechacreacion,NOW()) < 1440')->all();
      $totalRechazadas = count($rechazadas);

      $response = '
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning">'.$totalRechazadas.'</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">'.$totalRechazadas.' Ordenes Rechazadas</li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">';                    
                                          
                        
      foreach ($rechazadas as $rechazada) {
        $tiempo = $rechazada->fechacreacion;

        date_default_timezone_set('America/El_Salvador');

        $tiempo = strtotime ( '+6 hour' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );
        //$tiempo = strtotime ( '-21 minutes' , strtotime ( $tiempo ) ) ;
        //$tiempo = date ( 'Y-m-d H:m:s' , $tiempo );
        $tiempo = ( $tiempo < 60*60*24*365)? Yii::$app->formatter->asRelativeTime($tiempo ): Yii::$app->formatter->asDate($tiempo );

        $response .='
                <li>
                    <a href="index.php?r=orden-usuario/ticket">
                      <i class="fa fa-warning text-yellow"></i>Ticket: '.$rechazada->orden->ticket.' <br/>Restaurante:  '.$rechazada->nombreRestaurante.' <br/> '.$tiempo.'
                    </a>
                  </li>';
      }

      $response .= ' </ul>
                      </li>
                  </ul>';  

      echo $response;
    }

    public function actionListaTickets()
    {

      $searchModel = new OrdenUsuarioSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      //Se listan los tickets de la ultima semana en estado 1 o sea los tickets de hace 10080 minutos atras
      $dataProvider->query->where('TIMESTAMPDIFF(MINUTE,fechaInicio,NOW()) < 10080')->andWhere('estadoPedido = 1')->orderBy('idordenUsuario ASC')->all();

      $response = '';
            foreach ($dataProvider->models as $model) {
              //Creamos la notifiacion vista
              $notificacion = new NotificacionUsuario();

              //Cambiamos de estado a visto de las ordenes para el filtro de notificaciones.
              $modelOrdenActual = OrdenUsuario::findOne($model->idordenUsuario);
              $modelOrdenActual->visto = 1;
              $modelOrdenActual->update();
             
              //Construimos la lista de tickest
              $response .= '<a href="#" onclick="detalleOrden('.$model->idordenUsuario.')"> 
                    <div class="info-box " style="background-color: #fff;">
                      <div class="info-box-content-01 box-success" style="">
                        <span class="info-box-text text-black">Ticket: '.$model->ticket.'</span>
                        <span class="info-box-text text-black">'.htmlspecialchars(utf8_decode($model->idusuarioApp->name)).' '.htmlspecialchars(utf8_decode($model->idusuarioApp->last_name)).'</span>
                        <span class="info-box-text text-black">Tel: '.$model->idusuarioApp->phone.'</span>
                        <span class="info-box-text text-black">Cel: '.$model->idusuarioApp->mobile.'</span>
                        <span class="info-box-text text-black text-right">'.$model->fechaInicio.'</span>
                        <a class="btn btn-flat btn-block btn-sm" style="color: white; background-color: #'.$model->catalogoestados->color.';" onclick="detalleOrden('.$model->idordenUsuario.')"><strong>Tomar Orden</strong></a>
                        <span></span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                  </a>' ;
            }

      echo $response;
    }



    public function actionListaNotificaciones($idusuario)
    {
      \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

      //Obtenemos todas las notificaciones
      $searchModelNotificaciones = new NotificacionUsuarioSearch();
      
      //Filtramos las notificaciones por usuario y agrupamos por id de la orden
      $dataProviderNotificaciones = $searchModelNotificaciones->search(Yii::$app->request->queryParams);
      $dataProviderNotificaciones ->query->where('TIMESTAMPDIFF(MINUTE,time,NOW()) < 60')->andWhere('visto = 0')->andWhere('idusuario ='.\Yii::$app->user->id)->all();

      $i = 0;

      if(count($dataProviderNotificaciones->models) > 0 )
        { 
          
          foreach ($dataProviderNotificaciones->models as $modelNotificacion) {
               //recorremos todas las ordenes
              $response[$i] = $modelNotificacion;
              $modelNotificacion->visto = 1;
              $modelNotificacion->update();
              $i++;
            }
            

          return array('status' => true, 'data'=> $response);
        }else
        {
          return array('status'=>false,'data'=> 'No se encontraron datos');           
        }
    }

    public function actionRetraso($idusuario)
    {
      \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

      
      $searchModelOrdenes = new OrdenUsuarioSucursalSearch();
      
  
       //Filtramos las notificaciones por usuario y agrupamos por id de la orden
      $dataProviderOrdenes = $searchModelOrdenes->search(Yii::$app->request->queryParams);
      $dataProviderOrdenes->query->where('(TIMESTAMPDIFF(MINUTE,fechaCreacion,NOW()) > 2)')->andWhere('idestado = 6')->orderBy('idOrdenUsuario DESC')->all();

      $i = 0;

      if(count($dataProviderOrdenes->models) > 0 )
        { 
                
          foreach ($dataProviderOrdenes->models as $model) {
              //Obtenemos todas las notificaciones
              $searchModelNotificaciones = new NotificacionUsuarioSearch();
              
              //Filtramos las notificaciones por usuario y agrupamos por id de la orden
              $dataProviderNotificaciones = $searchModelNotificaciones->search(Yii::$app->request->queryParams);
                
              //guardamos las alertas sino existen
              $dataProviderNotificaciones ->query->where('idusuario ='.\Yii::$app->user->id)->andWhere('idorden = '.$model->idOrdenUsuario)->andWhere('estadoOrden = 18')->all();

              if (count($dataProviderNotificaciones->models) == 0 ) {

                $o = OrdenUsuario::findOne($model->idOrdenUsuario);

                

                if ($o->idteleoperador == (\Yii::$app->user->id)) {
  
                    $notificacion = new NotificacionUsuario();

                    $notificacion->idusuario = \Yii::$app->user->id;
                    $notificacion->idorden = $o->idordenUsuario;
                    $notificacion->ticket = $o->ticket;
                    $notificacion->estadoOrden = 18;
                    $notificacion->visto = 0;

                    $notificacion->save();
                    
                    $response[$i] = $model;
                }
              }
  
              $i++;
            }

            if (!isset($response)) {
              $response = "No se encontraron datos";
            }
            

          return array('status' => true, 'data'=> $response);
        }else
        {
          return array('status'=>false,'data'=> 'No se encontraron datos');           
        }
    }

    public function actionTotalIniciadas()
    {
        $searchModel = new OrdenUsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where('TIMESTAMPDIFF(MINUTE,fechaInicio,NOW()) < 10080')->andWhere('estadoPedido = 1')->orderBy('idordenUsuario ASC')->all();

        $iniciadas = OrdenUsuarioIniciadas::find()->all();
        $totalIniciadas = count($iniciadas);

        echo $totalIniciadas;
    }

    public function actionDespacho()
    {
        $searchModel = new OrdenUsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //Aprobado 3
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['estadoPedido'=>7])->orderBy(['fechaInicio'=>SORT_DESC]); // sort descending     

        return $this->render('despacho', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAsignar($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            //Creamos el nuevo registro del historial
            $historial = new Historialpedido();

             //Ingresamos lo datos al historial
            $historial->idordenUsuario = $id;
            $historial->idusuario = \Yii::$app->user->id;
            $historial->idtipousuario = 3;
            $historial->idestadoanterior = $model->estadoPedido;
            $historial->idestadonuevo = 4;

            //Actualizamos el estado del pedido
            $model->estadoPedido = 4;

            //Guardamos el registro del historial
            $historial->save();

            if ($model->save()) {
              //Enviar push notificando la asignacino al motorista
              $motorista = Motorista::findOne($model->idmotorista);
              $this->actionPush("Nuevo pedido asignado", "Se le ha asignado un nuevo pedido", $motorista->tokePush, $model->idordenUsuario, 3);
              return $this->redirect(['despacho']);
            }

        } else {
            return $this->render('asignar', [
                'model' => $model,
            ]);
        }
    }

    public static function actionDistancia($lat1, $lon1, $lat2, $lon2)
    {
      return acos(sin(deg2rad($lat1)) * SIN(deg2rad($lat2)) + COS(deg2rad($lat1)) * COS(deg2rad($lat2)) * COS(deg2rad($lon2 - $lon1))) * self::EARTH_RADIUS_KM;
    }

     public function actionHabilitar($id)
    {
        //Modificamos comportamiento de delete para que solamente deshabilite
        //los productos que se eliminen.

        //$model = $this->findModel($id);
        $model = OrdenUsuario::findOne($id);
        $model->estadoPedido = 1;

        if ($model->save()) {
            return $this->redirect(['historialpedido/ordenes-perdidas']);
        }

    }

    public function actionListaNotificacionesDespacho($idusuario)
    {
      \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

      //Obtenemos todas las notificaciones
      $searchModelNotificaciones = new NotificacionDespachoSearch();
      
      //Filtramos las notificaciones por usuario y agrupamos por id de la orden
      $dataProviderNotificaciones = $searchModelNotificaciones->search(Yii::$app->request->queryParams);
      $dataProviderNotificaciones ->query->where('TIMESTAMPDIFF(MINUTE,time,NOW()) < 60')->andWhere('visto = 0')->andWhere('idusuario ='.\Yii::$app->user->id)->all();

      $i = 0;

      if(count($dataProviderNotificaciones->models) > 0 )
        { 
          
          foreach ($dataProviderNotificaciones->models as $modelNotificacion) {
               //recorremos todas las ordenes
              $response[$i] = $modelNotificacion;
              $modelNotificacion->visto = 1;
              $modelNotificacion->update();
              $i++;
            }
            

          return array('status' => true, 'data'=> $response);
        }else
        {
          return array('status'=>false,'data'=> 'No se encontraron datos');           
        }
    }

    public function actionAceptarRestauranteSin($idorden, $idsucursal, $tiempo)
    {
      \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

      $hora = date("H:i:s");

      //Obtenemos la orden
      $Orden = OrdenUsuario::find()->where(['idordenUsuario' => $idorden])->one();

      //Obtenemos la orden dentro de la sucursal
      $ordenSucursal = New OrdenUsuarioSucursal();
     

        //Instanciamos un nuevo Historial
        $historialpedido = New Historialpedido();

        //Ingresamos los datos Actualizados
        $ordenSucursal->idOrdenUsuario = $idorden;
        $ordenSucursal->idSucursal = $idsucursal;
        $ordenSucursal->idestado = 7;
        $ordenSucursal->tiempoSucursal = $tiempo;
        $ordenSucursal->ordenAceptada = $hora;

        //Actualizamos la orden
        $Orden->estadoPedido = 7;

        //Registramos el Historial
        $historialpedido->idordenUsuario = $idorden;
        $historialpedido->idusuario = $idsucursal; //Raul sabe porque guarda aqui la sucursal
        $historialpedido->idtipousuario = 5;
        $historialpedido->idestadoanterior = 3; //Raul dice que siempre ponga 3
        $historialpedido->idestadonuevo = 7;


        //Acutualizamos la orden el la sucursal
        $ordenSucursal->save();

        //Actualizamos la Orden;
        $Orden->update();

        //Registramos el historial
        $historialpedido->save();
 
 
    }
        public function actionRechazarRestauranteSin($idorden, $idsucursal, $tiempo)
    {
      \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

       //Obtenemos la orden
      $Orden = OrdenUsuario::find()->where(['idordenUsuario' => $idorden])->one();

      //Obtenemos la orden dentro de la sucursal
      $ordenSucursal = New OrdenUsuarioSucursal();

      //Instanciamos un nuevo Historial
      $historialpedido = New Historialpedido();

      //Ingresamos los datos Actualizados
      $ordenSucursal->idOrdenUsuario = $idorden;
      $ordenSucursal->idSucursal = $idsucursal;
      $ordenSucursal->idestado = 8;

      //Actualizamos la orden
      $Orden->estadoPedido = 8;

      //Registramos el Historial
      $historialpedido->idordenUsuario = $idorden;
      $historialpedido->idusuario = $idsucursal; //Raul sabe porque guarda aqui la sucursal
      $historialpedido->idtipousuario = 5;
      $historialpedido->idestadoanterior = 3; //Raul dice que siempre ponga 3
      $historialpedido->idestadonuevo = 8;


      //Acutualizamos la orden el la sucursal
      $ordenSucursal->save();
      
      //Actualizamos la Orden;
      $Orden->update();
      
      //Registramos el historial
      $historialpedido->save();
  
    }

    public function actionDetalle($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            //Creamos el nuevo registro del historial
            $historial = new Historialpedido();

             //Ingresamos lo datos al historial
            $historial->idordenUsuario = $id;
            $historial->idusuario = \Yii::$app->user->id;
            $historial->idtipousuario = 3;
            $historial->idestadoanterior = $model->estadoPedido;
            $historial->idestadonuevo = 4;

            //Actualizamos el estado del pedido
            $model->estadoPedido = 4;

            //Guardamos el registro del historial
            $historial->save();

            if ($model->save()) {
              //Enviar push notificando la asignacino al motorista
              $motorista = Motorista::findOne($model->idmotorista);
              $this->actionPush("Nuevo pedido asignado", "Se le ha asignado un nuevo pedido", $motorista->tokePush, $model->idordenUsuario, 3);
              return $this->redirect(['despacho']);
            }

        } else {
            return $this->render('detalle', [
                'model' => $model,
            ]);
        }
    }
}
