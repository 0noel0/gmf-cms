<?php

namespace backend\controllers;

use Yii;
use backend\models\Horario;
use backend\models\HorarioSearch;
use backend\models\TblRestaurantes;
use backend\components\BaseController;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HorarioController implements the CRUD actions for Horario model.
 */
class HorarioController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view','index'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','update', 'delete'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN],
            ]
        ];
        return $behaviors;
    }

    /**
     * Lists all Horario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HorarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Horario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Horario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Horario();

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $id])->one();

        $searchModelFilterHorario = new HorarioSearch();
        $dataProviderHorario = $searchModelFilterHorario->search(Yii::$app->request->queryParams);
        $dataProviderHorario->query->andWhere(['idrestaurante'=>$id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create', 'id' => $restaurante->id_restaurante]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'restaurante' => $restaurante,
                'dataProviderHorario' => $dataProviderHorario,
            ]);
        }
    }

    /**
     * Updates an existing Horario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $idrestaurante)
    {
        $model = $this->findModel($id);

        $restaurante = TblRestaurantes::find()->where(['id_restaurante' => $idrestaurante])->one();

        $searchModelFilterHorario = new HorarioSearch();
        $dataProviderHorario = $searchModelFilterHorario->search(Yii::$app->request->queryParams);
        $dataProviderHorario->query->andWhere(['idrestaurante'=>$idrestaurante]);
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->idhorario, 'idrestaurante' => $restaurante->id_restaurante]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'restaurante' => $restaurante,
                'dataProviderHorario' => $dataProviderHorario,
            ]);
        }
    }

    /**
     * Deletes an existing Horario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $idrestaurante)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['horario/create','id' => $idrestaurante]);
    }

    /**
     * Finds the Horario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Horario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Horario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
