<?php

namespace backend\controllers;

use Yii;
use backend\models\Motorista;
use backend\models\MotoristaSearch;
use backend\models\OrdenUsuarioSearch;
use backend\components\BaseController;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MotoristaController implements the CRUD actions for Motorista model.
 */
class MotoristaController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view','index'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','update', 'detele', 'cerrar-sesion'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR],
            ]
        ];
        return $behaviors;
    }

    /**
     * Lists all Motorista models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MotoristaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Motorista model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $hoy = date('Y-m-d');
        $motorista = $this->findModel($id);
        $searchOrdenes = new OrdenUsuarioSearch();
        $ordenes = $searchOrdenes->search(Yii::$app->request->queryParams);
        $ordenes->query->where(['between', 'fechaInicio', $hoy.' 00:00:00', $hoy.' 23:59:59'])->andWhere('estadoPedido = 12')->andWhere('idmotorista = '.$id)->orderBy('idordenUsuario ASC')->all();


        return $this->render('view', [
            'model' => $this->findModel($id),
            'total' => $ordenes,
         ]);
    }

    /**
     * Creates a new Motorista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Motorista();

        if ($model->load(Yii::$app->request->post())) {
            $model->password = sha1($model->password);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->idmotorista]);
             }else{
             }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Motorista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           $model->password = sha1($model->password);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->idmotorista]);
             } 
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Motorista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Motorista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Motorista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Motorista::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCerrarSesion()
    {
        $searchModel = new MotoristaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        foreach ($dataProvider->models as $model) {
            $model->tokePush = '';
            $model->tokenSesion = '';
            $model->update();
        }

        return $this->redirect(['index']);
    }
}
