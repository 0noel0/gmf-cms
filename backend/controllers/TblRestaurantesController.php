<?php

namespace backend\controllers;

use Yii;
use backend\models\TblRestaurantes;
use backend\models\TblRestaurantesSearch;
use backend\models\MenusPlatosMovil;
use backend\models\MenusPlatosMovilSearch;
use backend\models\RelRestaurantesCatsSearch;
use backend\models\RelRestaurantesCats;
use backend\models\TblMenusCategorias;
use backend\models\CategoriaPlato;
use backend\models\CatalogoCategoriaextra;
use backend\models\HorarioSearch;
use backend\models\Horario;
use backend\models\Sucursal;
use backend\components\BaseController;
use common\models\User;
use common\components\Util;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;



/**
 * TblRestaurantesController implements the CRUD actions for TblRestaurantes model.
 */
class TblRestaurantesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['view','index'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN, User::ROLE_DESPACHADOR, User::ROLE_TELEOPERADOR],
            ],
            [
                'actions' => ['create','update', 'detele'],
                'allow' => true,
                'roles' => [User::ROLE_ADMIN],
            ]
        ];
        return $behaviors;
    }

    /**
     * Lists all TblRestaurantes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblRestaurantesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblRestaurantes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModelFilter = new MenusPlatosMovilSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['id_restaurante'=>$id]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelPlatos' => $dataProvider,
        ]);
    }

    /**
     * Creates a new TblRestaurantes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblRestaurantes();
        
        if ($model->load(Yii::$app->request->post())) {
            
            //Cargamos los archivos en variables temporales
            $model->_logotipo = UploadedFile::getInstance($model, '_logotipo');
            $model->_fotografia = UploadedFile::getInstance($model, '_fotografia');

            //validamos que si se haya subido las imagenes por parte del usuario
            if ($model->_logotipo) {
                $model->logotipo = Yii::$app->security->generateRandomString() . '.' . $model->_logotipo->extension;
            }
            if ($model->_fotografia) {
                $model->fotografia = Yii::$app->security->generateRandomString() . '.' . $model->_fotografia->extension;
            }

            if ($model->save()) {

                //validamos el registro del nombre de la foto
                if (!empty($model->logotipo)) {
                    Util::uploadLogo($model->_logotipo, $model->logotipo);
                }
                if (!empty($model->fotografia)) {
                    Util::uploadFotografia($model->_fotografia, $model->fotografia);
                }

                return $this->redirect(['update', 'id' => $model->id_restaurante]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblRestaurantes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $searchModelFilter = new MenusPlatosMovilSearch();
        $dataProvider = $searchModelFilter->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['id_restaurante'=>$id]);

        $searchModelFilterHorario = new HorarioSearch();
        $dataProviderHorario = $searchModelFilterHorario->search(Yii::$app->request->queryParams);
        $dataProviderHorario->query->andWhere(['idrestaurante'=>$id]);

        //Total de categorias de restaurante
        $ResCat = RelRestaurantesCats::find()->where(['id_restaurante' => $id])->all();
        $count = count($ResCat);

        //Total de Categorias de plato
        $ResCatPlato = TblMenusCategorias::find()->where(['id_restaurante' => $id])->all();
        $countCatPlatos = count($ResCatPlato);

        //Total de Platos
        $ResPlato = MenusPlatosMovil::find()->where(['id_restaurante' => $id])->all();
        $countPlatos = count($ResPlato);

        //Total de Categorias de extra (Catalogos de extra)
        $ResCatExtra = CatalogoCategoriaextra::find()->where(['idrestaurante' => $id])->all();
        $countCatExtra = count($ResCatExtra);        

        //Total de Horarios
        $ResHorario = Horario::find()->where(['idrestaurante' => $id])->all();
        $countHorario = count($ResHorario);    

         //Total de Sucursales
        $ResSucursal = Sucursal::find()->where(['id_restaurante' => $id])->all();
        $countSucursal = count($ResSucursal);                
        

         if ($model->load(Yii::$app->request->post())) {
            
            //Cargamos los archivos en variables temporales
            $model->_logotipo = UploadedFile::getInstance($model, '_logotipo');
            $model->_fotografia = UploadedFile::getInstance($model, '_fotografia');

            //validamos que si se haya subido las imagenes por parte del usuario
            if ($model->_logotipo) {
                $model->logotipo = Yii::$app->security->generateRandomString() . '.' . $model->_logotipo->extension;
            }
            if ($model->_fotografia) {
                $model->fotografia = Yii::$app->security->generateRandomString() . '.' . $model->_fotografia->extension;
            }

            if ($model->save()) {

                //validamos el registro del nombre de la foto
                if (!empty($model->logotipo)) {
                    Util::uploadLogo($model->_logotipo, $model->logotipo);
                }
                if (!empty($model->fotografia)) {
                    Util::uploadFotografia($model->_fotografia, $model->fotografia);
                }

                return $this->redirect(['update', 'id' => $model->id_restaurante]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelPlatos' => $dataProvider,
                'totalCat' => $count,
                'countCatPlatos' => $countCatPlatos,
                'countPlatos' => $countPlatos,
                'countCatExtra' => $countCatExtra,
                'dataProviderHorario' => $dataProviderHorario,
                'countHorario' => $countHorario,
                'countSucursal' => $countSucursal,
            ]);
        }
    }

    /**
     * Deletes an existing TblRestaurantes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblRestaurantes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblRestaurantes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblRestaurantes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
