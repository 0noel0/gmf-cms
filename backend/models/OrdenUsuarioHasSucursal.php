<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ordenUsuario_has_sucursal".
 *
 * @property integer $idordenUsuario_has_sucursal
 * @property integer $idordenUsuario
 * @property integer $idsucursal
 * @property integer $id_restaurante
 *
 * @property Sucursal $idsucursal0
 * @property OrdenUsuario $idordenUsuario0
 */
class OrdenUsuarioHasSucursal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ordenUsuario_has_sucursal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idordenUsuario', 'idsucursal', 'id_restaurante'], 'required'],
            [['idordenUsuario', 'idsucursal', 'id_restaurante'], 'integer'],
            [['idsucursal'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursal::className(), 'targetAttribute' => ['idsucursal' => 'idsucursal']],
            [['idordenUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenUsuario::className(), 'targetAttribute' => ['idordenUsuario' => 'idordenUsuario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idordenUsuario_has_sucursal' => 'Idorden Usuario Has Sucursal',
            'idordenUsuario' => 'Idorden Usuario',
            'idsucursal' => 'Idsucursal',
            'id_restaurante' => 'Id Restaurante',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdsucursal0()
    {
        return $this->hasOne(Sucursal::className(), ['idsucursal' => 'idsucursal']);
    }

    public function getDispositivo()
    {
        return $this->hasOne(Sucursal::className(), ['idsucursal' => 'idsucursal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdordenUsuario0()
    {
        return $this->hasOne(OrdenUsuario::className(), ['idordenUsuario' => 'idordenUsuario']);
    }
}
