<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "horario".
 *
 * @property integer $idhorario
 * @property integer $idrestaurante
 * @property integer $dia
 * @property string $hora_inicio1
 * @property string $hora_fin1
 * @property string $hora_inicio2
 * @property string $hora_fin2
 *
 * @property TblRestaurantes $idrestaurante0
 */
class Horario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'horario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idrestaurante', 'dia'], 'required'],
            [['idrestaurante', 'dia'], 'integer'],
            [['hora_inicio1', 'hora_fin1', 'hora_inicio2', 'hora_fin2'], 'string', 'max' => 10],
            [['idrestaurante'], 'exist', 'skipOnError' => true, 'targetClass' => TblRestaurantes::className(), 'targetAttribute' => ['idrestaurante' => 'id_restaurante']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhorario' => 'Idhorario',
            'idrestaurante' => 'Restaurante',
            'dia' => 'Dia',
            'hora_inicio1' => 'Horario Apertura 1',
            'hora_fin1' => 'Horario Cierre 1',
            'hora_inicio2' => 'Horario Apertura 2',
            'hora_fin2' => 'Horario Cierre 2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdrestaurante0()
    {
        return $this->hasOne(TblRestaurantes::className(), ['id_restaurante' => 'idrestaurante']);
    }
}
