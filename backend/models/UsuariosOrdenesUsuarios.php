<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "usuarios_ordenesUsuarios".
 *
 * @property integer $idordenUsuario
 * @property string $totalOrden
 * @property string $ticket
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $descripcion
 * @property string $tipoPago
 * @property string $direccion
 * @property string $referencia
 * @property string $nombre_direccion
 * @property string $latitud
 * @property string $longitud
 * @property string $nombreTarida
 * @property integer $estadoPedido
 * @property string $fechaInicio
 * @property integer $idcobertura
 * @property string $tarifa
 */
class UsuariosOrdenesUsuarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuarios_ordenesUsuarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idordenUsuario', 'estadoPedido', 'idcobertura'], 'integer'],
            [['totalOrden', 'tarifa'], 'number'],
            [['name', 'last_name', 'email', 'descripcion', 'tipoPago', 'direccion', 'referencia', 'nombre_direccion', 'latitud', 'longitud', 'nombreTarida', 'idcobertura', 'tarifa'], 'required'],
            [['descripcion', 'direccion', 'referencia', 'nombre_direccion'], 'string'],
            [['fechaInicio'], 'safe'],
            [['ticket', 'tipoPago'], 'string', 'max' => 45],
            [['name', 'last_name'], 'string', 'max' => 50],
            [['email', 'nombreTarida'], 'string', 'max' => 100],
            [['latitud', 'longitud'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idordenUsuario' => 'Idorden Usuario',
            'totalOrden' => 'Total Orden',
            'ticket' => 'Ticket',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'descripcion' => 'Descripcion',
            'tipoPago' => 'Tipo Pago',
            'direccion' => 'Direccion',
            'referencia' => 'Referencia',
            'nombre_direccion' => 'Nombre Direccion',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'nombreTarida' => 'Nombre Tarida',
            'estadoPedido' => 'Estado Pedido',
            'fechaInicio' => 'Fecha Inicio',
            'idcobertura' => 'Idcobertura',
            'tarifa' => 'Tarifa',
        ];
    }
}
