<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mobile_users".
 *
 * @property integer $idmobileusr
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property integer $idcountry
 * @property string $phone
 * @property string $mobile
 * @property string $creation
 * @property string $pin_cp
 * @property string $tokenSesion
 * @property string $tokenPush
 * @property string $primerRegistro
 * @property integer $idDispositivo
 *
 * @property Direccion[] $direccions
 * @property TblPais $idcountry0
 * @property NotificacionesHasUsuario[] $notificacionesHasUsuarios
 * @property OrdenUsuario[] $ordenUsuarios
 */
class MobileUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mobile_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'last_name', 'email', 'password', 'idcountry', 'tokenPush', 'idDispositivo'], 'required'],
            [['idcountry', 'idDispositivo'], 'integer'],
            [['creation'], 'safe'],
            [['tokenPush'], 'string'],
            [['name', 'last_name', 'pin_cp'], 'string', 'max' => 50],
            [['email', 'password'], 'string', 'max' => 100],
            [['phone', 'mobile', 'tokenSesion', 'primerRegistro'], 'string', 'max' => 45],
            [['idcountry'], 'exist', 'skipOnError' => true, 'targetClass' => TblPais::className(), 'targetAttribute' => ['idcountry' => 'id_pais']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idmobileusr' => 'Idmobileusr',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'idcountry' => 'Idcountry',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'creation' => 'Creation',
            'pin_cp' => 'Pin Cp',
            'tokenSesion' => 'Token Sesion',
            'tokenPush' => 'Token Push',
            'primerRegistro' => 'Primer Registro',
            'idDispositivo' => 'Id Dispositivo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDireccions()
    {
        return $this->hasMany(Direccion::className(), ['idmobileuser' => 'idmobileusr']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcountry0()
    {
        return $this->hasOne(TblPais::className(), ['id_pais' => 'idcountry']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificacionesHasUsuarios()
    {
        return $this->hasMany(NotificacionesHasUsuario::className(), ['idmobile_users' => 'idmobileusr']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenUsuarios()
    {
        return $this->hasMany(OrdenUsuario::className(), ['idusuario_app' => 'idmobileusr']);
    }

    public function getUsers()
    {
        return $this->hasOne(MobileUsers::className(), ['idusuario_app' => 'idmobileusr']);
    }

    /*public static function listMobileUsers(){
       return ArrayHelper::map(self::find()->all(), 'idmobileusr', 'name');
   }*/

}
