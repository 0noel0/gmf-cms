<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "catalogoestados".
 *
 * @property integer $idcatalogoestados
 * @property string $nombre
 * @property integer $estado
 */
class Catalogoestados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalogoestados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'estado'], 'required'],
            [['estado'], 'integer'],
            [['nombre','color'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcatalogoestados' => 'Idcatalogoestados',
            'nombre' => 'Nombre',
            'color' => 'Color',
            'estado' => 'Estado',
        ];
    }
}
