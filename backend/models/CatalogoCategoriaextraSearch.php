<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CatalogoCategoriaextra;

/**
 * CatalogoCategoriaextraSearch represents the model behind the search form about `backend\models\CatalogoCategoriaextra`.
 */
class CatalogoCategoriaextraSearch extends CatalogoCategoriaextra
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcatalogocategoriaextra', 'idrestaurante', 'estado', 'orden'], 'integer'],
            [['requerido', 'multiple', 'nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogoCategoriaextra::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idcatalogocategoriaextra' => $this->idcatalogocategoriaextra,
            'idrestaurante' => $this->idrestaurante,
            'orden' => $this->orden,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'requerido', $this->requerido])
            ->andFilterWhere(['like', 'multiple', $this->multiple])
            ->andFilterWhere(['like', 'orden', $this->orden])
            ->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
