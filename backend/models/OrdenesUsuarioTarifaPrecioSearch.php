<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\OrdenesUsuarioTarifaPrecio;

/**
 * OrdenesUsuarioTarifaPrecioSearch represents the model behind the search form about `backend\models\OrdenesUsuarioTarifaPrecio`.
 */
class OrdenesUsuarioTarifaPrecioSearch extends OrdenesUsuarioTarifaPrecio
{
    /**
     * @inheritdoc
     */
    public $fecha_inicio;
    public $fecha_fin;

    public function rules()
    {
        return [
            [['idordenUsuario'], 'integer'],
            [['fechaInicio', 'nombreUsuario', 'nombre', 'ticket', 'nombreEstado'], 'safe'],
            [['totalOrden', 'tarifa'], 'number'],
            [['fecha_inicio', 'fecha_fin', ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenesUsuarioTarifaPrecio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idordenUsuario' => $this->idordenUsuario,
            'fechaInicio' => $this->fechaInicio,
            'totalOrden' => $this->totalOrden,
            'tarifa' => $this->tarifa,
        ]);

        $query->andFilterWhere(['like', 'nombreUsuario', $this->nombreUsuario])
            ->andFilterWhere(['like', 'nombre', $this->nombre]);
        $query->andFilterWhere(['between', 'fechaInicio', $this->fecha_inicio.' 00:00:00', $this->fecha_fin.' 23:59:59']);
        //$query->andFilterWhere(['>=', 'fechaInicio', $this->fecha_inicio]);
        //$query->andFilterWhere(['<=', 'fechaInicio', $this->fecha_fin]);

        return $dataProvider;
    }

    public function searchSemana(){

        $query = OrdenesUsuarioTarifaPrecio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $query->andFilterWhere(['between', 'fechaInicio', $this->fecha_inicio, $this->fecha_fin]);
        //$query->andFilterWhere(['>', 'fechaInicio', $this->fecha_inicio]);
        //$query->andFilterWhere(['<', 'fechaInicio', $this->fecha_fin]);

         return $dataProvider;
    }
}
