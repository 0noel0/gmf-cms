<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "historicoPerdidas".
 *
 * @property integer $idhistorialpedido
 * @property string $teleoperador
 * @property string $ticket
 * @property string $nombreEstado
 * @property string $usuario
 * @property string $direccion
 * @property string $cobertura
 * @property string $mobile
 * @property string $email
 * @property integer $idusuario
 * @property integer $idestadonuevo
 * @property string $fechacreacion
 * @property integer $idordenUsuario
 * @property integer $estadoRestaurante
 */
class HistoricoPerdidas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historicoPerdidas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idhistorialpedido', 'idusuario', 'idestadonuevo', 'idordenUsuario', 'estadoRestaurante'], 'integer'],
            [['teleoperador', 'nombreEstado', 'direccion', 'cobertura', 'email', 'idusuario', 'idestadonuevo'], 'required'],
            [['direccion'], 'string'],
            [['fechacreacion'], 'safe'],
            [['teleoperador'], 'string', 'max' => 255],
            [['ticket', 'mobile'], 'string', 'max' => 45],
            [['nombreEstado'], 'string', 'max' => 1000],
            [['usuario'], 'string', 'max' => 101],
            [['cobertura', 'email'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhistorialpedido' => 'Idhistorialpedido',
            'teleoperador' => 'Teleoperador',
            'ticket' => 'Ticket',
            'nombreEstado' => 'Nombre Estado',
            'usuario' => 'Usuario',
            'direccion' => 'Direccion',
            'cobertura' => 'Cobertura',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'idusuario' => 'Idusuario',
            'idestadonuevo' => 'Idestadonuevo',
            'fechacreacion' => 'Fechacreacion',
            'idordenUsuario' => 'Idorden Usuario',
            'estadoRestaurante' => 'Estado Restaurante',
        ];
    }

    public static function primaryKey()
    {
        return ['idhistorialpedido'];
    }


}
