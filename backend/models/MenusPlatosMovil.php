<?php

namespace backend\models;

use Yii;
use cornernote\softdelete\SoftDeleteBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use backend\models\Estatus;

/**
 * This is the model class for table "menus_platos_movil".
 *
 * @property integer $id_plato_movil
 * @property integer $id_restaurante
 * @property integer $id_categoria
 * @property integer $estatus
 * @property integer $orden
 * @property string $fotografia
 * @property string $nombre
 * @property string $descripcion
 * @property string $precio1
 * @property integer $idTipoPedido
 *
 * @property TblRestaurantes $idRestaurante
 * @property TblMenusCategorias $idCategoria
 * @property TipoPedido $idTipoPedido0
 */
class MenusPlatosMovil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    //public variables to handle image uploads;
    public $_fotografia;
    
    public static function tableName()
    {
        return 'menus_platos_movil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_restaurante', 'id_categoria', 'estatus', 'orden', 'nombre', 'precio1', 'idTipoPedido'], 'required'],
            [['id_restaurante', 'id_categoria', 'estatus', 'orden', 'idTipoPedido'], 'integer'],
            [['fotografia', 'nombre', 'descripcion'], 'string'],
            [['precio1'], 'number'],
            [['id_restaurante'], 'exist', 'skipOnError' => true, 'targetClass' => TblRestaurantes::className(), 'targetAttribute' => ['id_restaurante' => 'id_restaurante']],
            [['id_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => TblMenusCategorias::className(), 'targetAttribute' => ['id_categoria' => 'id_categoria']],
            [['idTipoPedido'], 'exist', 'skipOnError' => true, 'targetClass' => TipoPedido::className(), 'targetAttribute' => ['idTipoPedido' => 'idTipoPedido']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_plato_movil' => 'Id Plato',
            'id_restaurante' => 'Restaurante',
            'id_categoria' => 'Categoría',
            'estatus' => 'Estado',
            'orden' => 'Orden',
            'fotografia' => 'Fotografía (Nombre de archivo y extensión)',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'precio1' => 'Precio',
            'idTipoPedido' => 'Tipo de Pedido',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRestaurante()
    {
        return $this->hasOne(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategoria()
    {
        return $this->hasOne(TblMenusCategorias::className(), ['id_categoria' => 'id_categoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipopedido()
    {
        return $this->hasOne(TipoPedido::className(), ['idTipoPedido' => 'idTipoPedido']);
    }

    public function getTblrestaurantes()
    {
        return $this->hasMany(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

    public function getTblmenuscategorias()
    {
        return $this->hasMany(TblMenusCategorias::className(), ['id_categoria' => 'id_categoria']);
    }

    public static function listMenusplatosmovil(){
        return ArrayHelper::map(self::find()->orderBy(['nombre'=>SORT_ASC])->all(), 'id_plato_movil', 'nombre');
    }

    public static function filterPlatos($idrestaurante){
        return ArrayHelper::map(self::find()->where('id_restaurante='.$idrestaurante)->all(), 'id_plato_movil', 'nombre');
    }

    public function getEestatus()
    {
        return $this->hasOne(Estatus::className(), ['id_estatus' => 'estatus']);
    }
}
