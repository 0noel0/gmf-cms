<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_pais".
 *
 * @property integer $id_pais
 * @property string $nombre
 *
 * @property MobileUsers[] $mobileUsers
 */
class TblPais extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_pais';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pais' => 'Id Pais',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobileUsers()
    {
        return $this->hasMany(MobileUsers::className(), ['idcountry' => 'id_pais']);
    }
}
