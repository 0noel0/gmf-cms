<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RelRestaurantesCats;

/**
 * RelRestaurantesCatsSearch represents the model behind the search form about `backend\models\RelRestaurantesCats`.
 */
class RelRestaurantesCatsSearch extends RelRestaurantesCats
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_relacion', 'id_restaurante', 'id_categoria'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RelRestaurantesCats::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_relacion' => $this->id_relacion,
            'id_restaurante' => $this->id_restaurante,
            'id_categoria' => $this->id_categoria,
        ]);

        return $dataProvider;
    }
}
