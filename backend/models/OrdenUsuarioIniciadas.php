<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ordenUsuario_Iniciadas".
 *
 * @property integer $idordenUsuario
 * @property string $descripcion
 * @property string $totalOrden
 * @property string $name
 * @property string $last_name
 */
class OrdenUsuarioIniciadas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ordenUsuario_Iniciadas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idordenUsuario'], 'integer'],
            [['descripcion', 'name', 'last_name'], 'required'],
            [['descripcion'], 'string'],
            [['totalOrden'], 'number'],
            [['name', 'last_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idordenUsuario' => 'Idorden Usuario',
            'descripcion' => 'Descripcion',
            'totalOrden' => 'Total Orden',
            'name' => 'Name',
            'last_name' => 'Last Name',
        ];
    }

     public function getOrden()
    {
        return $this->hasOne(OrdenUsuario::className(), ['idordenUsuario' => 'idordenUsuario']);
    }
}
