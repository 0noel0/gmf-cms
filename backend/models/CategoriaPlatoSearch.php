<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CategoriaPlato;

/**
 * CategoriaPlatoSearch represents the model behind the search form about `backend\models\CategoriaPlato`.
 */
class CategoriaPlatoSearch extends CategoriaPlato
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcategoria_plato', 'estado', 'id_plato_movil', 'id_restaurante', 'orden'], 'integer'],
            [['nombre', 'multiple', 'requerido'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CategoriaPlato::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idcategoria_plato' => $this->idcategoria_plato,
            'estado' => $this->estado,
            'id_plato_movil' => $this->id_plato_movil,
            'id_restaurante' => $this->id_restaurante,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'multiple', $this->multiple])
            ->andFilterWhere(['like', 'orden', $this->orden])
            ->andFilterWhere(['like', 'requerido', $this->requerido]);

        return $dataProvider;
    }
}
