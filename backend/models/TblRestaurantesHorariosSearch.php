<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblRestaurantesHorarios;

/**
 * TblRestaurantesHorariosSearch represents the model behind the search form about `backend\models\TblRestaurantesHorarios`.
 */
class TblRestaurantesHorariosSearch extends TblRestaurantesHorarios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_horario', 'dia_cerrado', 'id_restaurante'], 'integer'],
            [['dias', 'hora_desde', 'hora_hasta'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblRestaurantesHorarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_horario' => $this->id_horario,
            'dia_cerrado' => $this->dia_cerrado,
            'id_restaurante' => $this->id_restaurante,
        ]);

        $query->andFilterWhere(['like', 'dias', $this->dias])
            ->andFilterWhere(['like', 'hora_desde', $this->hora_desde])
            ->andFilterWhere(['like', 'hora_hasta', $this->hora_hasta]);

        return $dataProvider;
    }
}
