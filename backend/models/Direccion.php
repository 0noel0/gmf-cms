<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "direccion".
 *
 * @property integer $idDireccion
 * @property string $direccion
 * @property string $referencia
 * @property string $nombre_direccion
 * @property integer $idmobileuser
 * @property string $latitud
 * @property string $longitud
 *
 * @property MobileUsers $idmobileuser0
 */
class Direccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'direccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['direccion', 'referencia', 'nombre_direccion', 'idmobileuser', 'latitud', 'longitud'], 'required'],
            [['direccion', 'referencia', 'nombre_direccion'], 'string'],
            [['idmobileuser'], 'integer'],
            [['latitud', 'longitud'], 'string', 'max' => 500],
            [['idmobileuser'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUsers::className(), 'targetAttribute' => ['idmobileuser' => 'idmobileusr']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDireccion' => 'Id Direccion',
            'direccion' => 'Direccion',
            'referencia' => 'Referencia',
            'nombre_direccion' => 'Nombre Direccion',
            'idmobileuser' => 'Idmobileuser',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdmobileuser0()
    {
        return $this->hasOne(MobileUsers::className(), ['idmobileusr' => 'idmobileuser']);
    }
}
