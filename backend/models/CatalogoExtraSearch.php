<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CatalogoExtra;

/**
 * CatologoExtraSearch represents the model behind the search form about `backend\models\CatalogoExtra`.
 */
class CatalogoExtraSearch extends CatalogoExtra
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcatalogo_extra', 'estado', 'idcatalogocategoriaextra'], 'integer'],
            [['nombre'], 'safe'],
            [['precio'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogoExtra::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idcatalogo_extra' => $this->idcatalogo_extra,
            'precio' => $this->precio,
            'estado' => $this->estado,
            'idcatalogocategoriaextra' => $this->idcatalogocategoriaextra,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
