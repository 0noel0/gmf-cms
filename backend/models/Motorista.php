<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "motorista".
 *
 * @property integer $idmotorista
 * @property string $nombre
 * @property string $apellido
 * @property string $email
 * @property string $password
 * @property string $identificacion
 * @property string $telefono
 * @property string $direccion
 * @property string $documento
 * @property string $creacion
 * @property string $tokenSesion
 * @property string $tokePush
 * @property string $latitud
 * @property string $longitud
 * @property integer $disponible
 * @property integer $idtipousuario
 *
 * @property TipoUsuario $idtipousuario0
 * @property OrdenUsuario[] $ordenUsuarios
 */
class Motorista extends \yii\db\ActiveRecord
{
     var $distancia = 0;
    var $pedidos = 0; 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'motorista';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'email', 'password', 'identificacion', 'telefono', 'documento'], 'required'],
            [['creacion'], 'safe'],
            [['disponible', 'idtipousuario'], 'integer'],
            [['nombre', 'apellido', 'email', 'password', 'tokenSesion', 'tokePush', 'latitud', 'longitud'], 'string', 'max' => 1000],
            [['identificacion', 'documento'], 'string', 'max' => 100],
            [['telefono'], 'string', 'max' => 50],
            [['direccion'], 'string', 'max' => 500],
            [['idtipousuario'], 'exist', 'skipOnError' => true, 'targetClass' => TipoUsuario::className(), 'targetAttribute' => ['idtipousuario' => 'idtipousuario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idmotorista' => 'Idmotorista',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'email' => 'Email',
            'password' => 'Password',
            'identificacion' => 'Identificacion',
            'telefono' => 'Telefono',
            'direccion' => 'Direccion',
            'documento' => 'Documento',
            'creacion' => 'Creacion',
            'tokenSesion' => 'Token Sesion',
            'tokePush' => 'Toke Push',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'disponible' => 'Disponible',
            'idtipousuario' => 'Idtipousuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdtipousuario0()
    {
        return $this->hasOne(TipoUsuario::className(), ['idtipousuario' => 'idtipousuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenUsuarios()
    {
        return $this->hasMany(OrdenUsuario::className(), ['idmotorista' => 'idmotorista']);
    }
}
