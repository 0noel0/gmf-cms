<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\NotificacionGeneral;

/**
 * NotificacionGeneralSearch represents the model behind the search form about `backend\models\NotificacionGeneral`.
 */
class NotificacionGeneralSearch extends NotificacionGeneral
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idnotificacion_general', 'estado'], 'integer'],
            [['titulo', 'descripcion', 'fechaCreacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotificacionGeneral::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idnotificacion_general' => $this->idnotificacion_general,
            'fechaCreacion' => $this->fechaCreacion,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
