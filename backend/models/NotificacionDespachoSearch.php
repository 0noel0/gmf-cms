<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\NotificacionDespacho;

/**
 * NotificacionDespachoSearch represents the model behind the search form about `backend\models\NotificacionDespacho`.
 */
class NotificacionDespachoSearch extends NotificacionDespacho
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idnotificacion_despacho', 'idusuario', 'idorden', 'estadoOrden', 'visto'], 'integer'],
            [['ticket', 'time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotificacionDespacho::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idnotificacion_despacho' => $this->idnotificacion_despacho,
            'idusuario' => $this->idusuario,
            'idorden' => $this->idorden,
            'time' => $this->time,
            'estadoOrden' => $this->estadoOrden,
            'visto' => $this->visto,
        ]);

        $query->andFilterWhere(['like', 'ticket', $this->ticket]);

        return $dataProvider;
    }
}
