<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "notificacion_despacho".
 *
 * @property integer $idnotificacion_despacho
 * @property integer $idusuario
 * @property integer $idorden
 * @property string $ticket
 * @property string $time
 * @property integer $estadoOrden
 * @property integer $visto
 */
class NotificacionDespacho extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notificacion_despacho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idusuario', 'idorden', 'ticket', 'estadoOrden', 'visto'], 'required'],
            [['idusuario', 'idorden', 'estadoOrden', 'visto'], 'integer'],
            [['time'], 'safe'],
            [['ticket'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idnotificacion_despacho' => 'Idnotificacion Despacho',
            'idusuario' => 'Idusuario',
            'idorden' => 'Idorden',
            'ticket' => 'Ticket',
            'time' => 'Time',
            'estadoOrden' => 'Estado Orden',
            'visto' => 'Visto',
        ];
    }
}
