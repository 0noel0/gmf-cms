<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\GeoHelper;
/**
 * This is the model class for table "ordenUsuario".
 *
 * @property integer $idordenUsuario
 * @property string $descripcion
 * @property integer $idusuario_app
 * @property integer $idTipoPago
 * @property integer $html
 * @property string $token
 * @property string $id
 * @property string $estatus
 * @property string $respuestaRetorno
 * @property string $ticket
 * @property integer $estadoPedido
 * @property string $totalOrden
 * @property string $fechaFinal
 * @property string $fechaInicio
 *
 * @property Factura[] $facturas
 * @property MobileUsers $idusuarioApp
 * @property OrdenUsuarioHasExtra[] $ordenUsuarioHasExtras
 * @property OrdenesAdmin[] $ordenesAdmins
 * @property PlatosHasOrdenUsuario[] $platosHasOrdenUsuarios
 */
class OrdenUsuario extends \yii\db\ActiveRecord
{
    const EARTH_RADIUS_KM = 6371; 
    var $distancia = 0;
    var $pedidos = 0; 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ordenUsuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion', 'idusuario_app', 'idTipoPago', 'estadoPedido'], 'required'],
            [['descripcion', 'token', 'respuestaRetorno'], 'string'],
            [['idusuario_app', 'idTipoPago', 'html', 'estadoPedido', 'visto', 'idmotorista','idteleoperador'], 'integer'],
            [['totalOrden'], 'number'],
            [['fechaFinal', 'fechaInicio'], 'safe'],
            [['id', 'estatus', 'ticket'], 'string', 'max' => 45],
            [['idusuario_app'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUsers::className(), 'targetAttribute' => ['idusuario_app' => 'idmobileusr']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idordenUsuario' => 'Id Orden',
            'descripcion' => 'Descripcion',
            'idusuario_app' => 'Cliente',
            'idTipoPago' => 'Id Tipo Pago',
            'html' => 'Html',
            'token' => 'Token',
            'id' => 'ID',
            'estatus' => 'Estatus',
            'respuestaRetorno' => 'Respuesta Retorno',
            'ticket' => 'Ticket',
            'estadoPedido' => 'Estado Pedido',
            'totalOrden' => 'Total Orden',
            'fechaFinal' => 'Fecha Final',
            'fechaInicio' => 'Fecha Inicio',
            'idmotorista' => 'Motorista',
            'idteleoperador' => 'Teleoperador',
            'iddireccion' => 'Dirección',
            'idcobertura' => 'idcobertura',
            'visto' => 'Visto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturas()
    {
        return $this->hasMany(Factura::className(), ['idordenUsuario' => 'idordenUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdusuarioApp()
    {
        return $this->hasOne(MobileUsers::className(), ['idmobileusr' => 'idusuario_app']);
    }

    public function getCatalogoestados()
    {
        return $this->hasOne(Catalogoestados::className(), ['idcatalogoestados' => 'estadoPedido']);
    }

    public function getMotoristas()
    {
        return $this->hasOne(Motorista::className(), ['idmotorista' => 'idmotorista']);
    }


     public function getDireccion()
    {
        return $this->hasOne(Direccion::className(), ['idDireccion' => 'iddireccion']);
    }

      public function getTipoPago()
    {
        return $this->hasOne(TipoPago::className(), ['idtipo_pago' => 'idTipoPago']);
    }

      public function getTblCobertura()
    {
        return $this->hasOne(TblCobertura::className(), ['id_cobertura' => 'idcobertura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenUsuarioHasExtras()
    {
        return $this->hasMany(OrdenUsuarioHasExtra::className(), ['idordenUsuario' => 'idordenUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenesAdmins()
    {
        return $this->hasMany(OrdenesAdmin::className(), ['idordenUsuario' => 'idordenUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatosHasOrdenUsuarios()
    {
        return $this->hasMany(PlatosHasOrdenUsuario::className(), ['idordenUsuario' => 'idordenUsuario']);
    }
    /*public static function listMobileUsers(){
       return ArrayHelper::map(self::find()->all(), 'idmobileusr', 'name');
   }*/

   public static function listMotoristas($idordenUsuario){

    $motos = Motorista::find()->where('disponible = 1')->andWhere("tokePush != ''")->andWhere("tokenSesion != ''")->all();
    $orden = OrdenUsuario::findOne($idordenUsuario);

    foreach($motos as &$moto){
        //Obtenemos el total de pedidos entregados por motorista en el dia.
        $cantidad = OrdenUsuario::totalOrdenes($moto->idmotorista);
        $moto->pedidos = $cantidad;

        //Obtenemos la distancia del motorista disponible a la direccion del cliente.
        $distancia = OrdenUsuario::obtenerDistancia($moto->latitud, $moto->longitud, $orden->direccion->latitud, $orden->direccion->longitud);
        $moto->distancia = round((float) $distancia, 2); 

        $moto->nombre = $moto->nombre.' '.$moto->apellido.' ( Órdenes entregadas Hoy: '.$moto->pedidos.'; Distancia del Cliente: '.$moto->distancia.' Kms)';
    }

    $response = ArrayHelper::multisort($motos, ['disponible','pedidos', 'distancia'], [SORT_DESC, SORT_ASC, SORT_ASC]);
    return ArrayHelper::map($motos, 'idmotorista', 'nombre'); 
    }

    public function totalOrdenes($idmotorista){

        $hoy = date('Y-m-d');
        $searchOrdenes = new OrdenUsuarioSearch();
        $ordenes = $searchOrdenes->search(Yii::$app->request->queryParams);
        
        return count($ordenes->query->where(['between', 'fechaInicio', $hoy.' 00:00:00', $hoy.' 23:59:59'])->andWhere('estadoPedido = 12')->andWhere('idmotorista = '.$idmotorista)->orderBy('idordenUsuario ASC')->all());

    }

    public  function obtenerDistancia($lat1, $lon1, $lat2, $lon2)
    {
        return acos(sin(deg2rad($lat1)) * SIN(deg2rad($lat2)) + COS(deg2rad($lat1)) * COS(deg2rad($lat2)) * COS(deg2rad($lon2 - $lon1))) * self::EARTH_RADIUS_KM;
    }

}
