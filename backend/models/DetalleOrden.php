<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "detalle_orden".
 *
 * @property integer $idordenUsuario
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $nombreRestaurante
 * @property string $nombrePlato
 * @property string $precioPlato
 * @property string $nombreExta
 * @property double $precioExtra
 * @property integer $cantidadPlato
 * @property string $totalOrden
 * @property string $comentarios
 * @property string $ticket
 */
class DetalleOrden extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detalle_orden';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idordenUsuario', 'cantidadPlato'], 'integer'],
            [['name', 'last_name', 'email', 'nombreRestaurante', 'nombrePlato', 'precioPlato', 'cantidadPlato', 'comentarios'], 'required'],
            [['nombrePlato', 'comentarios'], 'string'],
            [['precioPlato', 'precioExtra', 'totalOrden'], 'number'],
            [['name', 'last_name'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
            [['nombreRestaurante'], 'string', 'max' => 255],
            [['nombreExta', 'ticket'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idordenUsuario' => 'Idorden Usuario',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'nombreRestaurante' => 'Nombre Restaurante',
            'nombrePlato' => 'Nombre Plato',
            'precioPlato' => 'Precio Plato',
            'nombreExta' => 'Nombre Exta',
            'precioExtra' => 'Precio Extra',
            'cantidadPlato' => 'Cantidad Plato',
            'totalOrden' => 'Total Orden',
            'comentarios' => 'Comentarios',
            'ticket' => 'Ticket',
        ];
    }
}
