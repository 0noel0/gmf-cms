<?php

namespace backend\models;

use Yii;
use cornernote\softdelete\SoftDeleteBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_menus_categorias".
 *
 * @property integer $id_categoria
 * @property integer $estatus
 * @property integer $id_restaurante
 * @property integer $orden
 * @property string $nombre
 *
 * @property MenusPlatosMovil[] $menusPlatosMovils
 * @property Estatus $estatus0
 * @property TblRestaurantes $idRestaurante
 */
class TblMenusCategorias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_menus_categorias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estatus', 'id_restaurante', 'orden'], 'integer'],
            [['id_restaurante', 'nombre'], 'required'],
            [['nombre'], 'string', 'max' => 255],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'id_estatus']],
            [['id_restaurante'], 'exist', 'skipOnError' => true, 'targetClass' => TblRestaurantes::className(), 'targetAttribute' => ['id_restaurante' => 'id_restaurante']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_categoria' => 'Id Categoria',
            'estatus' => 'Estatus',
            'id_restaurante' => 'Restaurante',
            'orden' => 'Orden',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenusPlatosMovils()
    {
        return $this->hasMany(MenusPlatosMovil::className(), ['id_categoria' => 'id_categoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEestatus()
    {
        return $this->hasOne(Estatus::className(), ['id_estatus' => 'estatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRestaurante()
    {
        return $this->hasOne(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

     public function getRestaurante($id)
    {
        return $this->hasOne(TblRestaurantes::className(), ['id_restaurante' => $id]);
    }

    public static function listTblmenuscategorias(){
        return ArrayHelper::map(self::find()->all(), 'id_categoria', 'nombre');
    }

    public static function filterCategorias($idcategoria){
        return ArrayHelper::map(self::find()->where('id_restaurante='.$idcategoria)->all(), 'id_categoria', 'nombre');
    }

    public function getTblrestaurantes()
    {
        return $this->hasMany(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

}
