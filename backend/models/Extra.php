<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "extra".
 *
 * @property integer $idextra
 * @property string $nombre
 * @property integer $estado
 * @property double $precio
 * @property integer $idcategoria_plato
 * @property integer $id_plato_movil
 * @property integer $id_restaurante
 *
 * @property CategoriaPlato $idcategoriaPlato
 */
class Extra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'extra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        [['estado', 'idcategoria_plato', 'id_plato_movil', 'id_restaurante', 'nombre', 'precio'], 'required'],
            [['estado', 'idcategoria_plato', 'id_plato_movil', 'id_restaurante'], 'integer'],
            [['precio'], 'number'],
            [['nombre'], 'string', 'max' => 45],
            [['idcategoria_plato'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriaPlato::className(), 'targetAttribute' => ['idcategoria_plato' => 'idcategoria_plato']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idextra' => 'Id extra',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
            'precio' => 'Precio',
            'idcategoria_plato' => 'Categoría de Extra',
            'id_plato_movil' => 'Plato App Móvil',
            'id_restaurante' => 'Restaurante',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriaplato()
    {
        return $this->hasOne(CategoriaPlato::className(), ['idcategoria_plato' => 'idcategoria_plato']);
    }

    public function getMenusplatomovil()
    {
        return $this->hasOne(MenusPlatosMovil::className(), ['id_plato_movil' => 'id_plato_movil']);
    }

    public function getTblrestaurantes()
    {
        return $this->hasMany(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

    public function getIdplatoMovil()
    {
        return $this->hasOne(MenusPlatosMovil::className(), ['id_plato_movil' => 'id_plato_movil']);
    }

    public function getEestatus()
    {
        return $this->hasOne(Estatus::className(), ['id_estatus' => 'estado']);
    }

    public static function listEstatus(){
        return ArrayHelper::map(Estatus::find()->all(), 'id_estatus', 'estado');
    }
}
