<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Motorista;

/**
 * MotoristaSearch represents the model behind the search form about `backend\models\Motorista`.
 */
class MotoristaSearch extends Motorista
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idmotorista', 'disponible', 'idtipousuario'], 'integer'],
            [['nombre', 'apellido', 'email', 'password', 'identificacion', 'telefono', 'direccion', 'documento', 'creacion', 'tokenSesion', 'tokePush', 'latitud', 'longitud'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Motorista::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idmotorista' => $this->idmotorista,
            'creacion' => $this->creacion,
            'disponible' => $this->disponible,
            'idtipousuario' => $this->idtipousuario,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellido', $this->apellido])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'documento', $this->documento])
            ->andFilterWhere(['like', 'tokenSesion', $this->tokenSesion])
            ->andFilterWhere(['like', 'tokePush', $this->tokePush])
            ->andFilterWhere(['like', 'latitud', $this->latitud])
            ->andFilterWhere(['like', 'longitud', $this->longitud]);

        return $dataProvider;
    }
}
