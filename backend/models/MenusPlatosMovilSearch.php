<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MenusPlatosMovil;

/**
 * MenusPlatosMovilSearch represents the model behind the search form about `backend\models\MenusPlatosMovil`.
 */
class MenusPlatosMovilSearch extends MenusPlatosMovil
{
    /**
     * @inheritdoc
     */

    public $_fotografia;
    public function rules()
    {
        return [
            [['id_plato_movil', 'id_restaurante', 'id_categoria', 'estatus', 'orden', 'idTipoPedido'], 'integer'],
            [['fotografia', 'nombre', 'descripcion'], 'safe'],
            [['precio1'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MenusPlatosMovil::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_plato_movil' => $this->id_plato_movil,
            'id_restaurante' => $this->id_restaurante,
            'id_categoria' => $this->id_categoria,
            'estatus' => $this->estatus,
            'orden' => $this->orden,
            'precio1' => $this->precio1,
            'idTipoPedido' => $this->idTipoPedido,
        ]);

        $query->andFilterWhere(['like', 'fotografia', $this->fotografia])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
