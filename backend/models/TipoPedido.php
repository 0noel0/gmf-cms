<?php

namespace backend\models;

use Yii;
use cornernote\softdelete\SoftDeleteBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/**
 * This is the model class for table "tipoPedido".
 *
 * @property integer $idTipoPedido
 * @property string $nombre
 *
 * @property MenusPlatosMovil[] $menusPlatosMovils
 */
class TipoPedido extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipoPedido';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTipoPedido', 'nombre'], 'required'],
            [['idTipoPedido'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTipoPedido' => 'Id Tipo Pedido',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenusPlatosMovils()
    {
        return $this->hasMany(MenusPlatosMovil::className(), ['idTipoPedido' => 'idTipoPedido']);
    }

     public static function listTipoPedido(){
        return ArrayHelper::map(self::find()->all(), 'idTipoPedido', 'nombre');
    }
}
