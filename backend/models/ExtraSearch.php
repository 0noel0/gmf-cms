<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Extra;

/**
 * ExtraSearch represents the model behind the search form about `backend\models\Extra`.
 */
class ExtraSearch extends Extra
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idextra', 'estado', 'idcategoria_plato', 'id_plato_movil', 'id_restaurante'], 'integer'],
            [['nombre'], 'safe'],
            [['precio'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Extra::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idextra' => $this->idextra,
            'estado' => $this->estado,
            'precio' => $this->precio,
            'idcategoria_plato' => $this->idcategoria_plato,
            'id_plato_movil' => $this->id_plato_movil,
            'id_restaurante' => $this->id_restaurante,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
