<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_restaurantes_horarios".
 *
 * @property integer $id_horario
 * @property string $dias
 * @property string $hora_desde
 * @property string $hora_hasta
 * @property integer $dia_cerrado
 * @property integer $id_restaurante
 */
class TblRestaurantesHorarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_restaurantes_horarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dias', 'hora_desde', 'hora_hasta', 'dia_cerrado', 'id_restaurante'], 'required'],
            [['dia_cerrado', 'id_restaurante'], 'integer'],
            [['dias'], 'string', 'max' => 200],
            [['hora_desde', 'hora_hasta'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_horario' => 'Id Horario',
            'dias' => 'Dias',
            'hora_desde' => 'Hora Desde',
            'hora_hasta' => 'Hora Hasta',
            'dia_cerrado' => 'Dia Cerrado',
            'id_restaurante' => 'Restaurante',
        ];
    }

    public function getTblrestaurantes()
    {
        return $this->hasMany(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }
}
