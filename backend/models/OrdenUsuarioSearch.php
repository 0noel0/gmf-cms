<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\OrdenUsuario;

/**
 * OrdenUsuarioSearch represents the model behind the search form about `backend\models\OrdenUsuario`.
 */
class OrdenUsuarioSearch extends OrdenUsuario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idordenUsuario', 'idusuario_app', 'idTipoPago', 'html', 'estadoPedido'], 'integer'],
            [['descripcion', 'token', 'id', 'estatus', 'respuestaRetorno', 'ticket', 'fechaFinal', 'fechaInicio'], 'safe'],
            [['totalOrden'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenUsuario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idordenUsuario' => $this->idordenUsuario,
            'idusuario_app' => $this->idusuario_app,
            'idTipoPago' => $this->idTipoPago,
            'html' => $this->html,
            'estadoPedido' => $this->estadoPedido,
            'totalOrden' => $this->totalOrden,
            'fechaFinal' => $this->fechaFinal,
            'fechaInicio' => $this->fechaInicio,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'estatus', $this->estatus])
            ->andFilterWhere(['like', 'respuestaRetorno', $this->respuestaRetorno])
            ->andFilterWhere(['like', 'ticket', $this->ticket]);

        return $dataProvider;
    }
}
