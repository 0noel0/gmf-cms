<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "OrdenesUsuario_tarifa_precio".
 *
 * @property integer $idordenusuario
 * @property string $fechaInicio
 * @property string $nombreUsuario
 * @property string $nombre
 * @property string $totalOrden
 * @property string $tarifa
 */
class OrdenesUsuarioTarifaPrecio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $fecha_inicio;
    public $fecha_fin;

    public static function tableName()
    {
        return 'OrdenesUsuario_tarifa_precio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idordenUsuario'], 'integer'],
            [['fechaInicio'], 'safe'],
            [['nombre', 'tarifa'], 'required'],
            [['totalOrden', 'tarifa'], 'number'],
            [['nombreUsuario'], 'string', 'max' => 101],
            [['nombre', 'nombreEstado', 'ticket'], 'string', 'max' => 255],
            [['fecha_inicio', 'fecha_fin', ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idordenUsuario' => 'ID Orden',
            'ticket' => 'Ticket',
            'fechaInicio' => 'Fecha',
            'nombreUsuario' => 'Cliente',
            'nombre' => 'Comercio',
            'totalOrden' => 'Cargo a Cliente',
            'tarifa' => 'Delivery',
            'nombreEstado' => 'Estado',
        ];
    }

    public static function primaryKey()
    {
        return ['idordenusuario'];
    }
}
