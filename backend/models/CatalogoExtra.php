<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "catalogo_extra".
 *
 * @property integer $idcatalogo_extra
 * @property string $nombre
 * @property string $precio
 * @property integer $estado
 * @property integer $idcatalogocategoriaextra
 *
 * @property CatalogoCategoriaextra $idcatalogocategoriaextra0
 */
class CatalogoExtra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalogo_extra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'precio', 'estado', 'idcatalogocategoriaextra'], 'required'],
            [['idcatalogo_extra', 'estado', 'idcatalogocategoriaextra'], 'integer'],
            [['precio'], 'number'],
            [['nombre'], 'string', 'max' => 1000],
            [['idcatalogocategoriaextra'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogoCategoriaextra::className(), 'targetAttribute' => ['idcatalogocategoriaextra' => 'idcatalogocategoriaextra']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcatalogo_extra' => 'ID Extra',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'estado' => 'Estado',
            'idcatalogocategoriaextra' => 'Catálogo de Extra',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcatalogocategoriaextra0()
    {
        return $this->hasOne(CatalogoCategoriaextra::className(), ['idcatalogocategoriaextra' => 'idcatalogocategoriaextra']);
    }
}
