<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "notificaciones_has_usuario".
 *
 * @property integer $idnotificaciones_has_usuario
 * @property integer $idmobile_users
 * @property integer $idnotificacion_general
 * @property integer $estado
 *
 * @property MobileUsers $idmobileUsers
 * @property NotificacionGeneral $idnotificacionGeneral
 */
class NotificacionesHasUsuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notificaciones_has_usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idmobile_users', 'idnotificacion_general', 'estado'], 'required'],
            [['idmobile_users', 'idnotificacion_general', 'estado'], 'integer'],
            [['idmobile_users'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUsers::className(), 'targetAttribute' => ['idmobile_users' => 'idmobileusr']],
            [['idnotificacion_general'], 'exist', 'skipOnError' => true, 'targetClass' => NotificacionGeneral::className(), 'targetAttribute' => ['idnotificacion_general' => 'idnotificacion_general']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idnotificaciones_has_usuario' => 'Idnotificaciones Has Usuario',
            'idmobile_users' => 'Idmobile Users',
            'idnotificacion_general' => 'Idnotificacion General',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdmobileUsers()
    {
        return $this->hasOne(MobileUsers::className(), ['idmobileusr' => 'idmobile_users']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdnotificacionGeneral()
    {
        return $this->hasOne(NotificacionGeneral::className(), ['idnotificacion_general' => 'idnotificacion_general']);
    }
}
