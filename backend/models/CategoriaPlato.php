<?php

namespace backend\models;

use Yii;
use backend\models\Estatus;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "categoria_plato".
 *
 * @property integer $idcategoria_plato
 * @property string $nombre
 * @property integer $estado
 * @property string $multiple
 * @property string $requerido
 * @property integer $id_plato_movil
 * @property integer $id_restaurante
 *
 * @property DetalleCategoriaPlato[] $detalleCategoriaPlatos
 * @property Extra[] $extras
 */
class CategoriaPlato extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categoria_plato';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado', 'id_plato_movil', 'id_restaurante', 'orden'], 'integer'],
            [['nombre', 'multiple', 'requerido'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcategoria_plato' => 'Idcategoria Plato',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
            'orden' => 'Orden',
            'multiple' => 'Multiple',
            'requerido' => 'Requerido',
            'id_plato_movil' => 'Plato App Móvil',
            'id_restaurante' => 'Restaurante',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleCategoriaPlatos()
    {
        return $this->hasMany(DetalleCategoriaPlato::className(), ['idcategoria_plato' => 'idcategoria_plato']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtras()
    {
        return $this->hasMany(Extra::className(), ['idcategoria_plato' => 'idcategoria_plato']);
    }

    public function getMenusplatosmovil()
    {
        return $this->hasMany(MenusPlatosMovil::className(), ['id_plato_movil' => 'id_plato_movil']);
    }

    public function getTblrestaurantes()
    {
        return $this->hasMany(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

    public function getEstatus()
    {
        return $this->hasMany(Estatus::className(), ['id_estatus' => 'estado']);
    }

    public static function listCategoriaplato(){
        return ArrayHelper::map(self::find()->all(), 'idcategoria_plato', 'nombre');
    }

}
