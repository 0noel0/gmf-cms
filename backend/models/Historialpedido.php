<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "historialpedido".
 *
 * @property integer $idhistorialpedido
 * @property integer $idordenUsuario
 * @property integer $idusuario
 * @property integer $idtipousuario
 * @property integer $idestadoanterior
 * @property integer $idestadonuevo
 * @property string $fechacreacion
 *
 * @property Catalogoestados $idestadonuevo0
 * @property OrdenUsuario $idordenUsuario0
 * @property TipoUsuario $idtipousuario0
 * @property Catalogoestados $idestadoanterior0
 */
class Historialpedido extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Inicio;
    public $Fin;
    public static function tableName()
    {
        return 'historialpedido';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idordenUsuario', 'idusuario', 'idtipousuario', 'idestadoanterior', 'idestadonuevo'], 'required'],
            [['idordenUsuario', 'idusuario', 'idtipousuario', 'idestadoanterior', 'idestadonuevo'], 'integer'],
            [['fechacreacion'], 'safe'],
            [['idestadonuevo'], 'exist', 'skipOnError' => true, 'targetClass' => Catalogoestados::className(), 'targetAttribute' => ['idestadonuevo' => 'idcatalogoestados']],
            [['idordenUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenUsuario::className(), 'targetAttribute' => ['idordenUsuario' => 'idordenUsuario']],
            [['idtipousuario'], 'exist', 'skipOnError' => true, 'targetClass' => TipoUsuario::className(), 'targetAttribute' => ['idtipousuario' => 'idtipousuario']],
            [['idestadoanterior'], 'exist', 'skipOnError' => true, 'targetClass' => Catalogoestados::className(), 'targetAttribute' => ['idestadoanterior' => 'idcatalogoestados']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhistorialpedido' => 'Idhistorialpedido',
            'idordenUsuario' => 'Idorden Usuario',
            'idusuario' => 'Idusuario',
            'idtipousuario' => 'Idtipousuario',
            'idestadoanterior' => 'Idestadoanterior',
            'idestadonuevo' => 'Idestadonuevo',
            'fechacreacion' => 'Fechacreacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdestadonuevo0()
    {
        return $this->hasOne(Catalogoestados::className(), ['idcatalogoestados' => 'idestadonuevo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdordenUsuario0()
    {
        return $this->hasOne(OrdenUsuario::className(), ['idordenUsuario' => 'idordenUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdtipousuario0()
    {
        return $this->hasOne(TipoUsuario::className(), ['idtipousuario' => 'idtipousuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdestadoanterior0()
    {
        return $this->hasOne(Catalogoestados::className(), ['idcatalogoestados' => 'idestadoanterior']);
    }
}
