<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblRestaurantesCats;

/**
 * TblRestaurantesCatsSearch represents the model behind the search form about `backend\models\TblRestaurantesCats`.
 */
class TblRestaurantesCatsSearch extends TblRestaurantesCats
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_categoria', 'estatus'], 'integer'],
            [['categoria'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblRestaurantesCats::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_categoria' => $this->id_categoria,
            'estatus' => $this->estatus,
        ]);

        $query->andFilterWhere(['like', 'categoria', $this->categoria]);

        return $dataProvider;
    }
}
