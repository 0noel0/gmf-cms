<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "catalogo_categoriaextra".
 *
 * @property integer $idcatalogocategoriaextra
 * @property integer $idrestaurante
 * @property string $requerido
 * @property string $multiple
 * @property string $nombre
 * @property integer $estado
 *
 * @property TblRestaurantes $idrestaurante0
 * @property CatalogoExtra[] $catalogoExtras
 */
class CatalogoCategoriaextra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalogo_categoriaextra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idrestaurante', 'requerido', 'multiple', 'nombre', 'estado'], 'required'],
            [['idrestaurante', 'estado', 'orden'], 'integer'],
            [['requerido', 'multiple'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 1000],
            [['idrestaurante'], 'exist', 'skipOnError' => true, 'targetClass' => TblRestaurantes::className(), 'targetAttribute' => ['idrestaurante' => 'id_restaurante']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcatalogocategoriaextra' => 'Catalogo de Extras',
            'idrestaurante' => 'Idrestaurante',
            'requerido' => 'Requerido',
            'multiple' => 'Multiple',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdrestaurante0()
    {
        return $this->hasOne(TblRestaurantes::className(), ['id_restaurante' => 'idrestaurante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogoExtras()
    {
        return $this->hasMany(CatalogoExtra::className(), ['idcatalogocategoriaextra' => 'idcatalogocategoriaextra']);
    }
}
