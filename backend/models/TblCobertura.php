<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_cobertura".
 *
 * @property integer $id_cobertura
 * @property integer $id_municipio
 * @property string $nombre
 * @property string $tarifa
 * @property integer $minutoCobertura
 * @property integer $estado
 *
 * @property OrdenUsuario[] $ordenUsuarios
 * @property TblClientesDireccion[] $tblClientesDireccions
 */
class TblCobertura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cobertura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_municipio', 'nombre', 'tarifa', 'minutoCobertura', 'estado'], 'required'],
            [['id_municipio', 'minutoCobertura', 'estado'], 'integer'],
            [['tarifa'], 'number'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_cobertura' => 'Id Cobertura',
            'id_municipio' => 'Id Municipio',
            'nombre' => 'Nombre',
            'tarifa' => 'Tarifa',
            'minutoCobertura' => 'Minuto Cobertura',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenUsuarios()
    {
        return $this->hasMany(OrdenUsuario::className(), ['idcobertura' => 'id_cobertura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblClientesDireccions()
    {
        return $this->hasMany(TblClientesDireccion::className(), ['id_cobertura' => 'id_cobertura']);
    }
}
