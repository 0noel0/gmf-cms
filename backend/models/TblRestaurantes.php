<?php

namespace backend\models;

use Yii;
use cornernote\softdelete\SoftDeleteBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
/**
 * This is the model class for table "tbl_restaurantes".
 *
 * @property integer $id_restaurante
 * @property integer $estatus
 * @property integer $orden
 * @property string $logotipo
 * @property string $nombre
 * @property string $descripcion
 * @property string $slug
 * @property integer $id
 * @property integer $moneda
 *
 * @property MenusPlatosMovil[] $menusPlatosMovils
 * @property RelRestaurantesCats[] $relRestaurantesCats
 * @property TblMenusCategorias[] $tblMenusCategorias
 * @property Estatus $estatus0
 */
class TblRestaurantes extends \yii\db\ActiveRecord
{
    //public variables to handle image uploads;
    public $_logotipo;
    public $_fotografia;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_restaurantes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estatus', 'orden', 'id', 'moneda', 'esperaMin', 'esperaMax', 'id_pais'], 'integer'],
            [['nombre', 'esperaMin', 'esperaMax'], 'required'],
            [['descripcion', 'fotografia'], 'string'],
            [['logotipo', 'nombre', 'slug'], 'string', 'max' => 255],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'id_estatus']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_restaurante' => 'Id Restaurante',
            'estatus' => 'Estado',
            'orden' => 'Orden',
            'logotipo' => 'Logotipo',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'slug' => 'Slug',
            'id' => 'ID',
            'moneda' => 'Moneda',
            'fotografia' => 'Banner',
            'id_pais' => 'País'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenusPlatosMovils()
    {
        return $this->hasMany(MenusPlatosMovil::className(), ['id_restaurante' => 'id_restaurante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelRestaurantesCats()
    {
        return $this->hasMany(RelRestaurantesCats::className(), ['id_restaurante' => 'id_restaurante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblMenusCategorias()
    {
        return $this->hasMany(TblMenusCategorias::className(), ['id_restaurante' => 'id_restaurante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEestatus()
    {
        return $this->hasOne(Estatus::className(), ['id_estatus' => 'estatus']);
    }

    public static function listTblRestaurantes(){
        return ArrayHelper::map(self::find()->all(), 'id_restaurante', 'nombre');
    }

    public function getNombreRestaurante() {
        return $this->hasOne(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

    public function getPais() {
        return $this->hasOne(TblPais::className(), ['id_pais' => 'id_pais']);
    }

     public static function listTblPais(){
        return ArrayHelper::map(TblPais::find()->all(), 'id_pais', 'nombre');
    }

}
