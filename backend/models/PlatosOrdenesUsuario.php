<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "platos_ordenesUsuario".
 *
 * @property integer $idordenUsuario
 * @property string $nombreRestaurante
 * @property string $nombrePlato
 * @property string $precioPlato
 * @property integer $cantidadPlato
 * @property string $comentarios
 * @property integer $idplatos_has_ordenUsuario
 * @property string $zona`s
 * @property string $latitud
 * @property string $longitud
 */
class PlatosOrdenesUsuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'platos_ordenesUsuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idordenUsuario', 'nombreRestaurante', 'nombrePlato', 'precioPlato', 'cantidadPlato', 'comentarios', 'zona`s', 'latitud', 'longitud'], 'required'],
            [['idordenUsuario', 'cantidadPlato', 'idplatos_has_ordenUsuario', 'id_restaurante'], 'integer'],
            [['nombrePlato', 'comentarios'], 'string'],
            [['precioPlato'], 'number'],
            [['nombreRestaurante'], 'string', 'max' => 255],
            [['zona`s', 'latitud', 'longitud'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idordenUsuario' => 'Idorden Usuario',
            'nombreRestaurante' => 'Nombre Restaurante',
            'nombrePlato' => 'Nombre Plato',
            'precioPlato' => 'Precio Plato',
            'cantidadPlato' => 'Cantidad Plato',
            'comentarios' => 'Comentarios',
            'idplatos_has_ordenUsuario' => 'Idplatos Has Orden Usuario',
            'zona`s' => 'Zona`s',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
        ];
    }

    public function getPlato()
    {
        return $this->hasOne(PlatosHasOrdenUsuario::className(), ['idplatos_has_ordenUsuario' => 'idplatos_has_ordenUsuario']);
    }
}
