<?php

namespace backend\models;

use Yii;
use cornernote\softdelete\SoftDeleteBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_restaurantes_cats".
 *
 * @property integer $id_categoria
 * @property integer $estatus
 * @property string $categoria
 *
 * @property RelRestaurantesCats[] $relRestaurantesCats
 */
class TblRestaurantesCats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_restaurantes_cats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estatus'], 'integer'],
            [['categoria'], 'required'],
            [['categoria'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_categoria' => 'Id Categoria',
            'estatus' => 'Estatus',
            'categoria' => 'Nombre de categoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelRestaurantesCats()
    {
        return $this->hasMany(RelRestaurantesCats::className(), ['id_categoria' => 'id_categoria']);
    }


    public static function ListTblrestaurantescats(){
        return ArrayHelper::map(self::find()->all(), 'id_categoria', 'categoria');
    }

    public function getEestatus() //Set doble ee para quitar ambiguedad con label
    {
        return $this->hasOne(Estatus::className(), ['id_estatus' => 'estatus']);
    }

}
