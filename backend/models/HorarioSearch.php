<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Horario;

/**
 * HorarioSearch represents the model behind the search form about `backend\models\Horario`.
 */
class HorarioSearch extends Horario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idhorario', 'idrestaurante', 'dia'], 'integer'],
            [['hora_inicio1', 'hora_fin1', 'hora_inicio2', 'hora_fin2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Horario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idhorario' => $this->idhorario,
            'idrestaurante' => $this->idrestaurante,
            'dia' => $this->dia,
        ]);

        $query->andFilterWhere(['like', 'hora_inicio1', $this->hora_inicio1])
            ->andFilterWhere(['like', 'hora_fin1', $this->hora_fin1])
            ->andFilterWhere(['like', 'hora_inicio2', $this->hora_inicio2])
            ->andFilterWhere(['like', 'hora_fin2', $this->hora_fin2]);

        return $dataProvider;
    }
}
