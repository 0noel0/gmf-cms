<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ordenUsuarioSucursal".
 *
 * @property integer $idordenUsuarioSucursal
 * @property integer $idOrdenUsuario
 * @property integer $idSucursal
 * @property integer $idestado
 * @property string $fechaCreacion
 *
 * @property Catalogoestados $idestado0
 * @property Sucursal $idSucursal0
 * @property OrdenUsuario $idOrdenUsuario0
 */
class OrdenUsuarioSucursal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ordenUsuarioSucursal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idOrdenUsuario', 'idSucursal', 'idestado'], 'required'],
            [['idOrdenUsuario', 'idSucursal', 'idestado'], 'integer'],
            [['fechaCreacion'], 'safe'],
            [['idestado'], 'exist', 'skipOnError' => true, 'targetClass' => Catalogoestados::className(), 'targetAttribute' => ['idestado' => 'idcatalogoestados']],
            [['idSucursal'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursal::className(), 'targetAttribute' => ['idSucursal' => 'idsucursal']],
            [['idOrdenUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenUsuario::className(), 'targetAttribute' => ['idOrdenUsuario' => 'idordenUsuario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idordenUsuarioSucursal' => 'Idorden Usuario Sucursal',
            'idOrdenUsuario' => 'Id Orden Usuario',
            'idSucursal' => 'Id Sucursal',
            'idestado' => 'Idestado',
            'fechaCreacion' => 'Fecha Creacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdestado0()
    {
        return $this->hasOne(Catalogoestados::className(), ['idcatalogoestados' => 'idestado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSucursal0()
    {
        return $this->hasOne(Sucursal::className(), ['idsucursal' => 'idSucursal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOrdenUsuario0()
    {
        return $this->hasOne(OrdenUsuario::className(), ['idordenUsuario' => 'idOrdenUsuario']);
    }
}
