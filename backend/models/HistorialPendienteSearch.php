<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\HistorialPendiente;

/**
 * HistorialPendienteSearch represents the model behind the search form about `backend\models\HistorialPendiente`.
 */
class HistorialPendienteSearch extends HistorialPendiente
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idhistorialpedido', 'idusuario', 'idestadonuevo', 'idordenUsuario'], 'integer'],
            [['ticket', 'usuario', 'direccion', 'cobertura', 'mobile', 'email', 'fechacreacion', 'nombreEstado', 'sucursal', 'nombreRestaurante'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HistorialPendiente::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idhistorialpedido' => $this->idhistorialpedido,
            'idusuario' => $this->idusuario,
            'idestadonuevo' => $this->idestadonuevo,
            'fechacreacion' => $this->fechacreacion,
            'idordenUsuario' => $this->idordenUsuario,
        ]);

        $query->andFilterWhere(['like', 'ticket', $this->ticket])
            ->andFilterWhere(['like', 'usuario', $this->usuario])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'cobertura', $this->cobertura])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'nombreEstado', $this->nombreEstado])
            ->andFilterWhere(['like', 'sucursal', $this->sucursal])
            ->andFilterWhere(['like', 'nombreRestaurante', $this->nombreRestaurante]);

        return $dataProvider;
    }
}
