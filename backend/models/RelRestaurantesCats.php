<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "rel_restaurantes_cats".
 *
 * @property integer $id_relacion
 * @property integer $id_restaurante
 * @property integer $id_categoria
 *
 * @property TblRestaurantesCats $idCategoria
 * @property TblRestaurantes $idRestaurante
 */
class RelRestaurantesCats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_restaurantes_cats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_restaurante', 'id_categoria'], 'required'],
            [['id_restaurante', 'id_categoria'], 'integer'],
            [['id_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => TblRestaurantesCats::className(), 'targetAttribute' => ['id_categoria' => 'id_categoria']],
            [['id_restaurante'], 'exist', 'skipOnError' => true, 'targetClass' => TblRestaurantes::className(), 'targetAttribute' => ['id_restaurante' => 'id_restaurante']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_relacion' => 'Id Relacion',
            'id_restaurante' => 'Restaurante',
            'id_categoria' => 'Categoría',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategoria()
    {
        return $this->hasOne(TblRestaurantesCats::className(), ['id_categoria' => 'id_categoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRestaurante()
    {
        return $this->hasOne(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

    /**
     * @inheritdoc
     * @return RelRestaurantesCatsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RelRestaurantesCatsQuery(get_called_class());
    }

    public function getRestaurante()
    {
        return $this->hasOne(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

     public function getTblrestaurantescats()
    {
        return $this->hasMany(TblRestaurantesCats::className(), ['id_categoria' => 'id_categoria']);
    }

    public function getTblrestaurantes()
    {
        return $this->hasMany(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }

    public function getCategorias()
    {
        return $this->hasOne(TblRestaurantesCats::className(), ['id_categoria' => 'id_categoria']);
    }
}
