<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "notificacion_general".
 *
 * @property integer $idnotificacion_general
 * @property string $titulo
 * @property string $descripcion
 * @property string $fechaCreacion
 * @property integer $estado
 *
 * @property NotificacionesHasUsuario[] $notificacionesHasUsuarios
 */
class NotificacionGeneral extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notificacion_general';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'descripcion', 'estado'], 'required'],
            [['fechaCreacion'], 'safe'],
            [['estado'], 'integer'],
            [['titulo'], 'string', 'max' => 50],
            [['descripcion'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idnotificacion_general' => 'Idnotificacion General',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'fechaCreacion' => 'Fecha Creacion',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificacionesHasUsuarios()
    {
        return $this->hasMany(NotificacionesHasUsuario::className(), ['idnotificacion_general' => 'idnotificacion_general']);
    }
}
