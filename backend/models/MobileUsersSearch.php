<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MobileUsers;

/**
 * MobileUsersSearch represents the model behind the search form about `backend\models\MobileUsers`.
 */
class MobileUsersSearch extends MobileUsers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idmobileusr', 'idcountry'], 'integer'],
            [['name', 'last_name', 'email', 'password', 'phone', 'mobile', 'creation', 'pin_cp', 'tokenSesion', 'tokenPush', 'primerRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MobileUsers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idmobileusr' => $this->idmobileusr,
            'idcountry' => $this->idcountry,
            'creation' => $this->creation,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'pin_cp', $this->pin_cp])
            ->andFilterWhere(['like', 'tokenSesion', $this->tokenSesion])
            ->andFilterWhere(['like', 'tokenPush', $this->tokenPush])
            ->andFilterWhere(['like', 'primerRegistro', $this->primerRegistro]);

        return $dataProvider;
    }
}
