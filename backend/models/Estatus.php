<?php

namespace backend\models;

use Yii;

use cornernote\softdelete\SoftDeleteBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
/**
 * This is the model class for table "estatus".
 *
 * @property integer $id_estatus
 * @property string $estatus
 *
 * @property TblMenusCategorias[] $tblMenusCategorias
 * @property TblRestaurantes[] $tblRestaurantes
 */
class Estatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estatus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estatus'], 'required'],
            [['id_estatus'], 'integer'],
            [['estatus'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_estatus' => 'Id Estatus',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblMenusCategorias()
    {
        return $this->hasMany(TblMenusCategorias::className(), ['estatus' => 'id_estatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRestaurantes()
    {
        return $this->hasMany(TblRestaurantes::className(), ['estatus' => 'id_estatus']);
    }

     public static function listEstatus(){
        return ArrayHelper::map(self::find()->all(), 'id_estatus', 'estatus');
    }
}
