<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblRestaurantes;

/**
 * TblRestaurantesSearch represents the model behind the search form about `backend\models\TblRestaurantes`.
 */
class TblRestaurantesSearch extends TblRestaurantes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_restaurante', 'estatus', 'orden', 'id', 'moneda'], 'integer'],
            [['logotipo', 'nombre', 'descripcion', 'slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblRestaurantes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_restaurante' => $this->id_restaurante,
            'estatus' => $this->estatus,
            'orden' => $this->orden,
            'id' => $this->id,
            'moneda' => $this->moneda,
        ]);

        $query->andFilterWhere(['like', 'logotipo', $this->logotipo])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
