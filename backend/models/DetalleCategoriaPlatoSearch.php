<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DetalleCategoriaPlato;

/**
 * DetalleCategoriaPlatoSearch represents the model behind the search form about `backend\models\DetalleCategoriaPlato`.
 */
class DetalleCategoriaPlatoSearch extends DetalleCategoriaPlato
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iddetalle_categoria_plato', 'id_plato_movil', 'idcategoria_plato'], 'integer'],
            [['nombre_categoria'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetalleCategoriaPlato::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iddetalle_categoria_plato' => $this->iddetalle_categoria_plato,
            'id_plato_movil' => $this->id_plato_movil,
            'idcategoria_plato' => $this->idcategoria_plato,
        ]);

        $query->andFilterWhere(['like', 'nombre_categoria', $this->nombre_categoria]);

        return $dataProvider;
    }
}
