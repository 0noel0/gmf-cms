<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tipo_pago".
 *
 * @property integer $idtipo_pago
 * @property string $nombre
 */
class TipoPago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_pago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idtipo_pago', 'nombre'], 'required'],
            [['idtipo_pago'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idtipo_pago' => 'Idtipo Pago',
            'nombre' => 'Nombre',
        ];
    }
}
