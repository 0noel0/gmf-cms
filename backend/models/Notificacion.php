<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "notificacion".
 *
 * @property integer $idnotificacion
 * @property integer $idusuario
 * @property integer $idorden
 * @property string $time
 * @property integer $estadoOrden
 * @property integer $visto
 */
class Notificacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notificacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idusuario', 'idorden', 'estadoOrden', 'visto'], 'required'],
            [['idusuario', 'idorden', 'estadoOrden', 'visto'], 'integer'],
            [['time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idnotificacion' => 'Idnotificacion',
            'idusuario' => 'Idusuario',
            'idorden' => 'Idorden',
            'time' => 'Time',
            'estadoOrden' => 'Estado Orden',
            'visto' => 'Visto',
        ];
    }
}
