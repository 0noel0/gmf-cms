<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "historial".
 *
 * @property integer $idhistorialpedido
 * @property string $motorista
 * @property string $ticket
 * @property string $nombreEstado
 * @property string $usuario
 * @property string $direccion
 * @property string $cobertura
 * @property string $mobile
 * @property string $email
 * @property integer $idusuario
 * @property integer $idestadonuevo
 * @property string $fechacreacion
 */
class Historial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idhistorialpedido', 'idusuario', 'idestadonuevo'], 'integer'],
            [['motorista', 'nombreEstado', 'direccion', 'cobertura', 'email', 'idusuario', 'idestadonuevo'], 'required'],
            [['motorista', 'direccion'], 'string'],
            [['fechacreacion'], 'safe'],
            [['ticket', 'mobile'], 'string', 'max' => 45],
            [['nombreEstado'], 'string', 'max' => 1000],
            [['usuario'], 'string', 'max' => 101],
            [['cobertura', 'email'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhistorialpedido' => 'Idhistorialpedido',
            'motorista' => 'Motorista',
            'teleoperador' => 'Teleoperador',
            'ticket' => 'Ticket',
            'nombreEstado' => 'Estado',
            'usuario' => 'Usuario',
            'direccion' => 'Direccion',
            'cobertura' => 'Cobertura',
            'mobile' => 'Móvil',
            'email' => 'Email',
            'idusuario' => 'Usuario',
            'idestadonuevo' => 'Idestadonuevo',
            'fechacreacion' => 'Fecha y Hora',
        ];
    }

    public static function primaryKey()
    {
        return ['idhistorialpedido'];
    }
}
