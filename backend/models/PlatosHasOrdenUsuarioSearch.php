<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PlatosHasOrdenUsuario;

/**
 * PlatosHasOrdenUsuarioSearch represents the model behind the search form about `backend\models\PlatosHasOrdenUsuario`.
 */
class PlatosHasOrdenUsuarioSearch extends PlatosHasOrdenUsuario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idplatos_has_ordenUsuario', 'id_plato_movil', 'idordenUsuario', 'cantidad', 'id_restaurante'], 'integer'],
            [['total'], 'number'],
            [['comentarios'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlatosHasOrdenUsuario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idplatos_has_ordenUsuario' => $this->idplatos_has_ordenUsuario,
            'id_plato_movil' => $this->id_plato_movil,
            'idordenUsuario' => $this->idordenUsuario,
            'cantidad' => $this->cantidad,
            'total' => $this->total,
            'id_restaurante' => $this->id_restaurante,
        ]);

        $query->andFilterWhere(['like', 'comentarios', $this->comentarios]);

        return $dataProvider;
    }
}
