<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sucursal".
 *
 * @property integer $idsucursal
 * @property string $latitud
 * @property string $longitud
 * @property string $zona
 * @property integer $id_restaurante
 *
 * @property OrdenUsuarioSucursal[] $ordenUsuarioSucursals
 * @property OrdenUsuarioHasSucursal[] $ordenUsuarioHasSucursals
 * @property TblRestaurantes $idRestaurante
 */
class Sucursal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sucursal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['latitud', 'longitud', 'zona', 'id_restaurante', 'password_hash','direccion','telefono'], 'required'],
            [['id_restaurante', 'estado', 'Dispositivo'], 'integer'],
            [['latitud', 'longitud', 'zona', 'username'], 'string', 'max' => 500],
            [['id_restaurante'], 'exist', 'skipOnError' => true, 'targetClass' => TblRestaurantes::className(), 'targetAttribute' => ['id_restaurante' => 'id_restaurante']],
        ];
    }
  
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idsucursal' => 'Idsucursal',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'zona' => 'Zona',
            'username' => 'Username',
            'password_hash' => 'Password',
            'id_restaurante' => 'Id Restaurante',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'estado' => 'Estado',
            'Dispositivo' => 'Dispositivo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenUsuarioSucursals()
    {
        return $this->hasMany(OrdenUsuarioSucursal::className(), ['idSucursal' => 'idsucursal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenUsuarioHasSucursals()
    {
        return $this->hasMany(OrdenUsuarioHasSucursal::className(), ['idsucursal' => 'idsucursal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurante()
    {
        return $this->hasOne(TblRestaurantes::className(), ['id_restaurante' => 'id_restaurante']);
    }
	
	/**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
		
		
    }
	
	/**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

     public static function listEstatus(){
        return ArrayHelper::map(Estatus::find()->all(), 'id_estatus', 'estatus');
    }
	

}
