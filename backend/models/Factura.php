<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "factura".
 *
 * @property integer $idfactura
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono
 * @property string $movil
 * @property string $valorEfectivo
 * @property integer $idordenUsuario
 *
 * @property OrdenUsuario $idordenUsuario0
 */
class Factura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'factura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['direccion'], 'string'],
            [['idordenUsuario'], 'required'],
            [['idordenUsuario'], 'integer'],
            [['nombre', 'telefono', 'movil', 'valorEfectivo'], 'string', 'max' => 45],
            [['idordenUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenUsuario::className(), 'targetAttribute' => ['idordenUsuario' => 'idordenUsuario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idfactura' => 'Idfactura',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'movil' => 'Movil',
            'valorEfectivo' => 'Valor Efectivo',
            'idordenUsuario' => 'Idorden Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdordenUsuario0()
    {
        return $this->hasOne(OrdenUsuario::className(), ['idordenUsuario' => 'idordenUsuario']);
    }
}
