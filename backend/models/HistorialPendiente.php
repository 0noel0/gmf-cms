<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "historialPendiente".
 *
 * @property integer $idhistorialpedido
 * @property string $ticket
 * @property string $usuario
 * @property string $direccion
 * @property string $cobertura
 * @property string $mobile
 * @property string $email
 * @property integer $idusuario
 * @property integer $idestadonuevo
 * @property string $fechacreacion
 * @property integer $idordenUsuario
 * @property string $nombreEstado
 * @property string $sucursal
 * @property string $nombreRestaurante
 */
class HistorialPendiente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historialPendiente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idhistorialpedido', 'idusuario', 'idestadonuevo', 'idordenUsuario'], 'integer'],
            [['direccion', 'cobertura', 'email', 'idusuario', 'idestadonuevo', 'nombreEstado', 'sucursal', 'nombreRestaurante'], 'required'],
            [['direccion'], 'string'],
            [['fechacreacion'], 'safe'],
            [['ticket', 'mobile'], 'string', 'max' => 45],
            [['usuario'], 'string', 'max' => 101],
            [['cobertura', 'email'], 'string', 'max' => 100],
            [['nombreEstado'], 'string', 'max' => 1000],
            [['sucursal'], 'string', 'max' => 500],
            [['nombreRestaurante'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhistorialpedido' => 'Idhistorialpedido',
            'ticket' => 'Ticket',
            'usuario' => 'Usuario',
            'direccion' => 'Direccion',
            'cobertura' => 'Cobertura',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'idusuario' => 'Idusuario',
            'idestadonuevo' => 'Idestadonuevo',
            'fechacreacion' => 'Fechacreacion',
            'idordenUsuario' => 'Idorden Usuario',
            'nombreEstado' => 'Nombre Estado',
            'sucursal' => 'Sucursal',
            'nombreRestaurante' => 'Nombre Restaurante',
        ];
    }

    public static function primaryKey()
    {
        return ['idhistorialpedido'];
    }
}
