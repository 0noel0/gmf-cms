<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "detalle_categoria_plato".
 *
 * @property integer $iddetalle_categoria_plato
 * @property integer $id_plato_movil
 * @property integer $idcategoria_plato
 * @property string $nombre_categoria
 *
 * @property MenusPlatosMovil $idPlatoMovil
 * @property CategoriaPlato $idcategoriaPlato
 */
class DetalleCategoriaPlato extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detalle_categoria_plato';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_plato_movil', 'idcategoria_plato', 'nombre_categoria'], 'required'],
            [['id_plato_movil', 'idcategoria_plato'], 'integer'],
            [['nombre_categoria'], 'string', 'max' => 100],
            [['id_plato_movil'], 'exist', 'skipOnError' => true, 'targetClass' => MenusPlatosMovil::className(), 'targetAttribute' => ['id_plato_movil' => 'id_plato_movil']],
            [['idcategoria_plato'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriaPlato::className(), 'targetAttribute' => ['idcategoria_plato' => 'idcategoria_plato']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iddetalle_categoria_plato' => 'Id Plato/Categoria de Extra',
            'id_plato_movil' => 'Plato App Móvil',
            'idcategoria_plato' => 'Idcategoria Plato',
            'nombre_categoria' => 'Nombre Categoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPlatoMovil()
    {
        return $this->hasOne(MenusPlatosMovil::className(), ['id_plato_movil' => 'id_plato_movil']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcategoriaPlato()
    {
        return $this->hasOne(CategoriaPlato::className(), ['idcategoria_plato' => 'idcategoria_plato']);
    }

    public function getMenusplatosmovil()
    {
        return $this->hasMany(MenusPlatosMovil::className(), ['id_plato_movil' => 'id_plato_movil']);
    }

}
