<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ordenUsuario_extras".
 *
 * @property integer $idordenUsuario
 * @property string $nombreExta
 * @property double $precioExtra
 * @property integer $idplatos_has_ordenUsuario
 * @property integer $id_plato_movil
 */
class OrdenUsuarioExtras extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ordenUsuario_extras';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idordenUsuario', 'id_plato_movil'], 'required'],
            [['idordenUsuario', 'idplatos_has_ordenUsuario', 'id_plato_movil'], 'integer'],
            [['precioExtra'], 'number'],
            [['nombreExta'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idordenUsuario' => 'Idorden Usuario',
            'nombreExta' => 'Nombre Exta',
            'precioExtra' => 'Precio Extra',
            'idplatos_has_ordenUsuario' => 'Idplatos Has Orden Usuario',
            'id_plato_movil' => 'Id Plato Movil',
        ];
    }

       public function getPlato()
    {
        return $this->hasOne(MenusPlatosMovil::className(), ['id_plato_movil' => 'id_plato_movil']);
    }

}
