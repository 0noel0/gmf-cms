<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "platos_has_ordenUsuario".
 *
 * @property integer $idplatos_has_ordenUsuario
 * @property integer $id_plato_movil
 * @property integer $idordenUsuario
 * @property integer $cantidad
 * @property string $total
 * @property string $comentarios
 * @property integer $id_restaurante
 *
 * @property MenusPlatosMovil $idPlatoMovil
 * @property OrdenUsuario $idordenUsuario0
 */
class PlatosHasOrdenUsuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'platos_has_ordenUsuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_plato_movil', 'idordenUsuario', 'cantidad', 'total', 'comentarios', 'id_restaurante'], 'required'],
            [['id_plato_movil', 'idordenUsuario', 'cantidad', 'id_restaurante'], 'integer'],
            [['total'], 'number'],
            [['comentarios'], 'string'],
            [['id_plato_movil'], 'exist', 'skipOnError' => true, 'targetClass' => MenusPlatosMovil::className(), 'targetAttribute' => ['id_plato_movil' => 'id_plato_movil']],
            [['idordenUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenUsuario::className(), 'targetAttribute' => ['idordenUsuario' => 'idordenUsuario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idplatos_has_ordenUsuario' => 'Idplatos Has Orden Usuario',
            'id_plato_movil' => 'Id Plato Movil',
            'idordenUsuario' => 'Idorden Usuario',
            'cantidad' => 'Cantidad',
            'total' => 'Total',
            'comentarios' => 'Comentarios',
            'id_restaurante' => 'Id Restaurante',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPlatoMovil()
    {
        return $this->hasOne(MenusPlatosMovil::className(), ['id_plato_movil' => 'id_plato_movil']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdordenUsuario0()
    {
        return $this->hasOne(OrdenUsuario::className(), ['idordenUsuario' => 'idordenUsuario']);
    }
}
