<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tipoUsuario".
 *
 * @property integer $idtipousuario
 * @property string $nombre
 * @property integer $estado
 *
 * @property Historialpedido[] $historialpedidos
 * @property MobileUsers[] $mobileUsers
 * @property Motorista[] $motoristas
 * @property Teleoperador[] $teleoperadors
 */
class TipoUsuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipoUsuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idtipousuario', 'nombre', 'estado'], 'required'],
            [['idtipousuario', 'estado'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idtipousuario' => 'Idtipousuario',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistorialpedidos()
    {
        return $this->hasMany(Historialpedido::className(), ['idtipousuario' => 'idtipousuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobileUsers()
    {
        return $this->hasMany(MobileUsers::className(), ['idtipousuario' => 'idtipousuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMotoristas()
    {
        return $this->hasMany(Motorista::className(), ['idtipousuario' => 'idtipousuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeleoperadors()
    {
        return $this->hasMany(Teleoperador::className(), ['idtipousuario' => 'idtipousuario']);
    }
}
