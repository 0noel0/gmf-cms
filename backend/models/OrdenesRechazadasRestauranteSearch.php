<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\OrdenesRechazadasRestaurante;

/**
 * OrdenesRechazadasRestauranteSearch represents the model behind the search form about `backend\models\OrdenesRechazadasRestaurante`.
 */
class OrdenesRechazadasRestauranteSearch extends OrdenesRechazadasRestaurante
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idrechazoRestaurante', 'idordenUsuario', 'idsucursal'], 'integer'],
            [['comentarios', 'fechaCreacion', 'usuario', 'nombreRestaurante', 'zona', 'telefono'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenesRechazadasRestaurante::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idrechazoRestaurante' => $this->idrechazoRestaurante,
            'idordenUsuario' => $this->idordenUsuario,
            'idsucursal' => $this->idsucursal,
            'fechaCreacion' => $this->fechaCreacion,
        ]);

        $query->andFilterWhere(['like', 'comentarios', $this->comentarios])
            ->andFilterWhere(['like', 'usuario', $this->usuario])
            ->andFilterWhere(['like', 'nombreRestaurante', $this->nombreRestaurante])
            ->andFilterWhere(['like', 'zona', $this->zona])
            ->andFilterWhere(['like', 'telefono', $this->telefono]);

        return $dataProvider;
    }
}
