<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ordenesRechazadas_restaurante".
 *
 * @property integer $idrechazoRestaurante
 * @property integer $idordenUsuario
 * @property integer $idsucursal
 * @property string $comentarios
 * @property string $fechaCreacion
 * @property string $usuario
 * @property string $nombreRestaurante
 * @property string $zona
 * @property string $telefono
 */
class OrdenesRechazadasRestaurante extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ordenesRechazadas_restaurante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idrechazoRestaurante', 'idordenUsuario', 'idsucursal'], 'integer'],
            [['idordenUsuario', 'idsucursal', 'nombreRestaurante', 'zona'], 'required'],
            [['fechaCreacion'], 'safe'],
            [['telefono'], 'string'],
            [['comentarios'], 'string', 'max' => 10000],
            [['usuario'], 'string', 'max' => 101],
            [['nombreRestaurante'], 'string', 'max' => 255],
            [['zona'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idrechazoRestaurante' => 'Idrechazo Restaurante',
            'idordenUsuario' => 'Idorden Usuario',
            'idsucursal' => 'Idsucursal',
            'comentarios' => 'Comentarios',
            'fechaCreacion' => 'Fecha de Rechazo',
            'usuario' => 'Usuario',
            'nombreRestaurante' => 'Restaurante',
            'zona' => 'Zona',
            'telefono' => 'Telefono',
        ];
    }

    public static function primaryKey()
    {
        return ['idrechazoRestaurante'];
    }

    public function getOrden()
    {
        return $this->hasOne(OrdenUsuario::className(), ['idordenUsuario' => 'idordenUsuario']);
    }

    public function getSucursal()
    {
        return $this->hasOne(Sucursal::className(), ['idsucursal' => 'idsucursal']);
    }

}
