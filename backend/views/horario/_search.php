<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\HorarioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="horario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idhorario') ?>

    <?= $form->field($model, 'idrestaurante') ?>

    <?= $form->field($model, 'dia') ?>

    <?= $form->field($model, 'hora_inicio1') ?>

    <?= $form->field($model, 'hora_fin1') ?>

    <?php // echo $form->field($model, 'hora_inicio2') ?>

    <?php // echo $form->field($model, 'hora_fin2') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
