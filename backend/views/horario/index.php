<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\TblRestaurantes;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HorarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Horarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Horario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idhorario',
             [
                'attribute' => 'idrestaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    try {
                        return $model->idrestaurante0->nombre;
                    } catch (Exception $e) {
                        echo "N/A";
                    }
                },
            ],   
            [
                'attribute' => 'dia',
                'value' => function ($model) {
                    try {
                        $dia = ($model->dia == '1') ? 'Domingo' : 'Otro dia' ;
                        $dia = ($model->dia == '2') ? 'Lunes' : $dia ;
                        $dia = ($model->dia == '3') ? 'Martes' : $dia ;
                        $dia = ($model->dia == '4') ? 'Miércoles' : $dia ;
                        $dia = ($model->dia == '5') ? 'Jueves' : $dia ;
                        $dia = ($model->dia == '6') ? 'Viernes' : $dia ;
                        $dia = ($model->dia == '7') ? 'Sábado' : $dia ;

                        return $dia;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ], 
            'hora_inicio1',
            'hora_fin1',
            'hora_inicio2',
            'hora_fin2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
