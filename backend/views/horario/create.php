<?php


use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Horario */

$this->title = 'Agregar Horario';

?>
<div class="horario-create">

	<?php 
	    echo Breadcrumbs::widget([
	    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
	    'links' => [
		        [
		            'label' => 'Restaurantes',
		            'url' => ['tbl-restaurantes/index'],
		            'template' => "<li><b>{link}</b></li>\n", // template for this link only
		        ],
		        [
		            'label' => $restaurante->nombre, 
		            'url' => ['tbl-restaurantes/update', 'id' => $restaurante->id_restaurante]
		        ],
		        'Horarios',
			],
	    ]);

	?>

    <?= $this->render('_form', [
        'model' => $model,
        'restaurante' => $restaurante,
    ]) ?>

</div>

<h1>Horarios del Restaurante</h1>
<div>
        <?= GridView::widget([
        'dataProvider' => $dataProviderHorario,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'dia',
                'value' => function ($model) {
                    try {
                        $dia = ($model->dia == '1') ? 'Domingo' : 'Otro dia' ;
                        $dia = ($model->dia == '2') ? 'Lunes' : $dia ;
                        $dia = ($model->dia == '3') ? 'Martes' : $dia ;
                        $dia = ($model->dia == '4') ? 'Miércoles' : $dia ;
                        $dia = ($model->dia == '5') ? 'Jueves' : $dia ;
                        $dia = ($model->dia == '6') ? 'Viernes' : $dia ;
                        $dia = ($model->dia == '7') ? 'Sábado' : $dia ;

                        return $dia;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ], 
            'hora_inicio1',
            'hora_fin1',
            'hora_inicio2',
            'hora_fin2',
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{leadUpdate}  {leadDelete}',
                'buttons'  => [
                    'leadUpdate' => function ($url, $model) {
                        $url ='index.php?r=horario/update&id='.$model->idhorario.'&idrestaurante='.$model->idrestaurante;
                        return Html::a('<span class="fa fa-pencil"></span>', $url, ['title' => 'Editar']);
                    },
                    'leadDelete' => function ($url, $model) {
                        $url =['horario/delete', 'id' => $model->idhorario, 'idrestaurante' => $model->idrestaurante];
                        return Html::a('<span class="fa fa-trash"></span>', $url, [
                            'title'        => 'Eliminar',
                            'data-confirm' => Yii::t('yii', '¿Está seguro que desea Eliminar el Horario?'),
                            'data-method'  => 'post',
                        ]);
                    },
                ],
            ],

        ],
    ]); ?>
</div>

