<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Estatus;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblRestaurantesCatsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categorías de restaurante';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-restaurantes-cats-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Categoría', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id_categoria',
            'categoria',
            [
                'attribute' => 'estatus',
                'value' => function ($model) {
                    try {
                        return ($model->eestatus->estatus);
                    } catch (Exception $e) {
                        echo "N/A";
                    }
                },
            ], 

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
