<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Estatus;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantesCats */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-restaurantes-cats-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'estatus')->dropDownList(Estatus::listEstatus()) ?>

    <?= $form->field($model, 'categoria')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
