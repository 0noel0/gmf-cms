<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantesCats */

$this->title = 'Actualizar categoría: ' . $model->categoria;
$this->params['breadcrumbs'][] = ['label' => 'Categorías de restaurante', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_categoria, 'url' => ['view', 'id' => $model->id_categoria]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="tbl-restaurantes-cats-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
