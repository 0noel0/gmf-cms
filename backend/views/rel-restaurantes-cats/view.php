<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\RelRestaurantesCats */

$this->title = "Restaurante/Categoría: ".$model->id_relacion;
$this->params['breadcrumbs'][] = ['label' => 'Restaurante/Categoría', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rel-restaurantes-cats-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_relacion, 'restaurante' => $model->id_restaurante], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro que desea elimnar el item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_relacion',
            [
                'attribute' => 'id_restaurante',
                'value' => $model->tblrestaurantes[0]->nombre,
            ],
            [
                'attribute' => 'id_categoria',
                'value' => $model->categorias->categoria,
            ],
            
        ],
    ]) ?>

</div>
