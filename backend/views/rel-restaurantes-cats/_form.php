<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\TblRestaurantes;
use backend\models\TblRestaurantesCats;
/* @var $this yii\web\View */
/* @var $model backend\models\RelRestaurantesCats */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rel-restaurantes-cats-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'id_restaurante')->hiddenInput(['value' => $restaurante->id_restaurante])->label(false); ?>

    <?= $form->field($model, 'id_categoria')->dropDownList(TblRestaurantesCats::listTblRestaurantesCats()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Agregar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
