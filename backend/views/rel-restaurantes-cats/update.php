<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\RelRestaurantesCats */

$this->title = 'Actualizar restaurante/categoría: ' . $model->id_relacion;
$this->params['breadcrumbs'][] = ['label' => 'restaurante/categoría', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_relacion, 'url' => ['view', 'id' => $model->id_relacion]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="rel-restaurantes-cats-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
