<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\RelRestaurantesCatsSearch;
use backend\models\RelRestaurantesCats;
use backend\models\TblRestaurantes;
use backend\models\TblRestaurantesCats;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RelRestaurantesCatsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Restaurantes/Categorias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-restaurantes-cats-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nueva Restaurante/Categoría', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_relacion',
            [
                'attribute' => 'id_restaurante',
                //'filter' => TblRestaurantes::listTblRestaurantes(),
                'filter'=>ArrayHelper::map(TblRestaurantes::find()->asArray()->all(), 'id_restaurante', 'nombre'),
                'value' => function ($model) {
                    try {
                        return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],     
            [
                'attribute' => 'id_categoria',
                'filter' => TblRestaurantesCats::ListTblrestaurantescats(),
                'value' => function ($model) {
                    try {
                          return $model->tblrestaurantescats[0]->categoria;
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],
            
            ['class' => 'yii\grid\ActionColumn'],
        ],

    ]); ?>
</div>
