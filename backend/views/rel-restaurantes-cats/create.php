<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use backend\models\RelRestaurantesCatsSearch;
use backend\models\RelRestaurantesCats;
use backend\models\TblRestaurantes;
use backend\models\TblRestaurantesCats;
use yii\helpers\ArrayHelper;



/* @var $this yii\web\View */
/* @var $model backend\models\RelRestaurantesCats */
$this->title = $restaurante->nombre;
/*$this->title = $restaurante->nombre.' > Agregar nueva categoría de Restaurante';
$this->params['breadcrumbs'][] = ['label' => $restaurante->nombre, 'url' => ['create&id='.$restaurante->id_restaurante]];
$this->params['breadcrumbs'][] = $this->title;
*/
?>


<?php 
    echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
        [
            'label' => 'Restaurantes',
            'url' => ['tbl-restaurantes/index'],
            'template' => "<li><b>{link}</b></li>\n", // template for this link only
        ],
        [
            'label' => $restaurante->nombre, 
            'url' => ['tbl-restaurantes/update', 'id' => $restaurante->id_restaurante]
        ],
        'Categorías de restaurante',
        ],
    ]);

 ?>

<div class="rel-restaurantes-cats-create">


    <?= $this->render('_form', [
        'model' => $model,
        'restaurante' => $restaurante,
    ]) ?>

</div>


<div>
	<h1>Categorias de Restaurante</h1>
	 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_relacion',
            [
                'attribute' => 'id_restaurante',
                //'filter' => TblRestaurantes::listTblRestaurantes(),
                'filter'=>ArrayHelper::map(TblRestaurantes::find()->asArray()->all(), 'id_restaurante', 'nombre'),
                'value' => function ($model) {
                    try {
                        return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],     
            [
                'attribute' => 'id_categoria',
                'filter' => TblRestaurantesCats::ListTblrestaurantescats(),
                'value' => function ($model) {
                    try {
                          return $model->tblrestaurantescats[0]->categoria;
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],
            
            [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=rel-restaurantes-cats/view&id='.$model->id_relacion;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],

    ]); ?>
</div>