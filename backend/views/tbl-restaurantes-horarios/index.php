<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\TblRestaurantes;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblRestaurantesHorariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Restaurantes Horarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-restaurantes-horarios-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Restaurantes Horarios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_horario',
            [
                'attribute' => 'id_restaurante',
                //'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    return $model->tblrestaurantes[0]->nombre;
                },
            ],
            'dias',
            'hora_desde',
            'hora_hasta',
            'dia_cerrado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
