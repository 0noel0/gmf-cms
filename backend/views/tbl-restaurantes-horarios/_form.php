<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantesHorarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-restaurantes-horarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hora_desde')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hora_hasta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dia_cerrado')->textInput() ?>

    <?= $form->field($model, 'id_restaurante')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
