<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantesHorarios */

$this->title = 'Create Tbl Restaurantes Horarios';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Restaurantes Horarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-restaurantes-horarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
