<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantesHorarios */

$this->title = 'Update Tbl Restaurantes Horarios: ' . $model->id_horario;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Restaurantes Horarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_horario, 'url' => ['view', 'id' => $model->id_horario]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-restaurantes-horarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
