<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantesHorariosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-restaurantes-horarios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_horario') ?>

    <?= $form->field($model, 'dias') ?>

    <?= $form->field($model, 'hora_desde') ?>

    <?= $form->field($model, 'hora_hasta') ?>

    <?= $form->field($model, 'dia_cerrado') ?>

    <?php // echo $form->field($model, 'id_restaurante') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
