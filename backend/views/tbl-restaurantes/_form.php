<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Estatus;
use backend\models\TblRestaurantes;
use kartik\file\FileInput;
use common\components\Util;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-restaurantes-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
        ]);
    ?>

    <?= $form->field($model, 'estatus')->dropDownList(Estatus::listEstatus()) ?>

    <?= $form->field($model, 'orden')->textInput() ?>

    <?= $form->field($model, '_logotipo')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'allowedFileExtensions'=>['jpg', 'gif', 'png'],
                'initialPreview'=>[
                    Html::img(Util::getUrlLogo($model->logotipo))
                ],
                'overwriteInitial'=>true,
                'showUpload' => false,
                'showCaption' => false,
                'maxFileSize'=>2000
            ]
        ]);  
    ?>

    <?= $form->field($model, '_fotografia')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'allowedFileExtensions'=>['jpg', 'gif', 'png'],
                'initialPreview'=>[
                    Html::img(Util::getUrlFotografia($model->fotografia))
                ],
                'overwriteInitial'=>true,
                'showUpload' => false,
                'showCaption' => false,
                'maxFileSize'=>2000
            ]
        ]);  
    ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id_pais')->dropDownList(TblRestaurantes::listTblPais()) ?>

    <?= $form->field($model, 'esperaMin')->textInput() ?>

    <?= $form->field($model, 'esperaMax')->textInput() ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
