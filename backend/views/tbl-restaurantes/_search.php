<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-restaurantes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_restaurante') ?>

    <?= $form->field($model, 'estatus') ?>

    <?= $form->field($model, 'orden') ?>

    <?= $form->field($model, 'logotipo') ?>

    <?= $form->field($model, 'nombre') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'moneda') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
