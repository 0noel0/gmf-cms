<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\models\TblRestaurantes;
use backend\models\TblMenusCategorias;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantes */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Restaurantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-restaurantes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_restaurante], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_restaurante], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro que desea eliminar el item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        //'mode'=>DetailView::MODE_VIEW,
        'attributes' => [
            //'id_restaurante',
            'nombre',
            'descripcion:ntext',
            'orden',
            //'logotipo',
            //'fotografia',
            //'slug',
            /*[
                'attribute'=>'slug',
                'format'=>'raw',
                'value'=>Html::a($model->slug, ['update', 'id' => $model->id_restaurante]),
            ],*/
            [
                'attribute' => 'estatus',
                'value' => $model->eestatus->estatus,
            ],
        ],
    ]) ?>

</div>

<div class="row">
    <div class="col-xs-6 col-md-6 col-sm-12">
        <label>Vista previa de Logotipo</label>
        <br>
        <img src="http://getmyfood.com.sv/img/restaurants/<?php echo $model->logotipo; ?>">
        <br>
        <span><?php echo $model->logotipo; ?></span>
    </div>

    <div class="col-xs-6 col-md-6 col-sm-12">
        <label>Vista previa del Banner</label>
        <br>
        <img src="http://getmyfood.com.sv/img/galleries/<?php echo $model->fotografia; ?>">
        <br>
        <span><?php echo $model->fotografia; ?></span>
    </div>
</div>
<h1>Platos del Restaurante</h1>
<div>
       <?= GridView::widget([
        'dataProvider' => $modelPlatos,
        'columns' => [
            
            'id_plato_movil',
            [
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    try {
                          return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            [
                'attribute' => 'id_categoria',
                'filter' => TblMenusCategorias::ListTblmenuscategorias(),
                'value' => function ($model) {
                    try {
                          return $model->tblmenuscategorias[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            'nombre:ntext',
            //'descripcion:ntext',
            //'estatus',
            // 'fotografia:ntext',
            // 'precio1',
            // 'idTipoPedido',
            'orden',


            //['class' => 'yii\grid\ActionColumn'],
            [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}{update}{delete}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }

                    if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    /*if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
</div>
