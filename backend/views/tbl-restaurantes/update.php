<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\models\TblRestaurantes;
use backend\models\TblMenusCategorias;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRestaurantes */

$this->title = $model->nombre;
?>

<?php 
    echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
        [
            'label' => 'Restaurantes',
            'url' => ['tbl-restaurantes/index'],
            'template' => "<li><b>{link}</b></li>\n", // template for this link only
        ],
        $model->nombre,
        ],
    ]);

 ?>

<div class="tbl-restaurantes-update">

    <h1><?= Html::encode($model->nombre) ?></h1>
   
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<div>
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
       
         <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-gray">
            <div class="inner text-center">
              <h3>
                <?php 
                    echo $countSucursal;
                ?>
              </h3>

              <p>SUCURSALES</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo 'index.php?r=sucursal/create&id='.$model->id_restaurante; ?>" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner text-center">
              <h3>
                <?php 
                    echo $countPlatos;
                ?>
              </h3>

              <p>PLATOS</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo 'index.php?r=menus-platos-movil/create&id='.$model->id_restaurante; ?>" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner text-center">
              <h3>
                <?php 
                    echo $countCatExtra;
                ?>
              </h3>

              <p>CATÁLOGO DE EXTRAS</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?php echo 'index.php?r=catalogo-categoriaextra/create&id='.$model->id_restaurante; ?>" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
       <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">

            <div class="inner text-center">
              <h3>
                <?php 
                  echo $totalCat;
                 ?>
              </h3>

              <p>CATEGORIAS DE RESTAURANTE</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo 'index.php?r=rel-restaurantes-cats/create&id='.$model->id_restaurante; ?>" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner text-center">
               <h3> 
                  <?php 
                    echo $countCatPlatos;
                  ?>
               </h3>
              <p>CATEGORIAS DE PLATO</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo 'index.php?r=tbl-menus-categorias/create&id='.$model->id_restaurante; ?>" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner text-center">
              <h3>
                <?php 
                    echo $countHorario;
                ?>
              </h3>

              <p>HORARIOS</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo 'index.php?r=horario/create&id='.$model->id_restaurante; ?>" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      
      </div>
      <!-- /.row -->
    </section>
   
</div>


<h1>Horarios del Restaurante</h1>
<div>
        <?= GridView::widget([
        'dataProvider' => $dataProviderHorario,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'dia',
                'value' => function ($model) {
                    try {
                        $dia = ($model->dia == '1') ? 'Domingo' : 'Otro dia' ;
                        $dia = ($model->dia == '2') ? 'Lunes' : $dia ;
                        $dia = ($model->dia == '3') ? 'Martes' : $dia ;
                        $dia = ($model->dia == '4') ? 'Miércoles' : $dia ;
                        $dia = ($model->dia == '5') ? 'Jueves' : $dia ;
                        $dia = ($model->dia == '6') ? 'Viernes' : $dia ;
                        $dia = ($model->dia == '7') ? 'Sábado' : $dia ;

                        return $dia;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ], 
            'hora_inicio1',
            'hora_fin1',
            'hora_inicio2',
            'hora_fin2',

             [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=horario/update&id='.$model->idhorario.'&idrestaurante='.$model->idrestaurante;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
</div>

<h1>Platos del Restaurante</h1>
<div>
       <?= GridView::widget([
        'dataProvider' => $modelPlatos,
        'columns' => [
            
            'id_plato_movil',
            /*[
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    try {
                          return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],*/
            [
                'attribute' => 'id_categoria',
                'filter' => TblMenusCategorias::ListTblmenuscategorias(),
                'value' => function ($model) {
                    try {
                          return $model->tblmenuscategorias[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            'nombre:ntext',
            //'descripcion:ntext',
            //'estatus',
            // 'fotografia:ntext',
            // 'precio1',
            // 'idTipoPedido',
            'orden',


            [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil.'&idrestaurante='.$model->id_restaurante;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
</div>