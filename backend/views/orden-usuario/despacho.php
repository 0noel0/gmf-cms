<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdenUsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Despacho';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Audio de notificacion -->
<audio id="alarma" src="campana.mp3" preload="auto"></audio>

<div class="orden-usuario-index" id="grid">
    <?php Pjax::begin([
          'id'=>'pjax',
          'clientOptions' => ["push" => true,"replace" => false,"timeout" => 3000,"scrollTo" => false,"container" => "#grid"]
        ]); ?>
    <?= GridView::widget([
    	'id' => 'grid',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idordenUsuario',
            //'descripcion:ntext',
            'ticket',
            [
                'attribute' => 'idusuario_app',
                'value' => function ($model) {
                    try {
                        return htmlspecialchars(utf8_decode($model->idusuarioApp->name.' '.$model->idusuarioApp->last_name));
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            [
                'attribute' => 'estadoPedido',
                'value' => function ($model) {
                    try {
                        return htmlspecialchars(utf8_decode($model->catalogoestados->nombre));
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            [
                'attribute' => 'iddireccion',
                'value' => function ($model) {
                    try {
                        return htmlspecialchars(utf8_decode($model->direccion->direccion));
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            [
                'attribute' => 'totalOrden',
                'value' => function ($model) {
                    try {
                        return ('$'.$model->totalOrden);
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            //'idTipoPago',
            //'html',
            // 'token:ntext',
            // 'id',
            // 'respuestaRetorno:ntext',
            // 'fechaFinal',
            // 'fechaInicio',

             [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('Asignar', ['orden-usuario/asignar', 'id' => $model->idordenUsuario], ['class'=>'btn btn-primary']);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=orden-usuario/view&id='.$model->idordenUsuario;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php 

$this->registerJs(' 
    setInterval(function(){  
         $.pjax.reload({container:"#grid"});
}, 30000);', \yii\web\VIEW::POS_HEAD); 

?>

<script type="text/javascript">
     function listarNotificaciones() {
        $.get("index.php?r=orden-usuario/lista-notificaciones-despacho&idusuario=<? \Yii::$app->user->id; ?> ", function(data){
          console.log("Notifications: "+data.status);
          if (data.status == true) {
              for (var i = data.data.length - 1; i >= 0; i--) {
                //Mostramos la alerta de nueva orden
                //if (data.data[i].estadoOrden == 10) {
                    notifyMe(data.data[i].ticket, 'La orden ha sido tomada por el Motorista.');
                //}
            }
          }
        });
      }

      function listarOrdenes() {
            //Listamos las notificaciones si existiesen
            listarNotificaciones();
            //Mostramos la lista actualizada de tickets
            $.get( "index.php?r=orden-usuario/lista-tickets", function( data ) {
                //$( "#tickets" ).html( data );
                //console.log("Ticketssssssss");
                setTimeout(listarOrdenes, 2000);
              });
      }

      listarOrdenes();


      function notifyMe(ticket, titulo) {
        // Let's check if the browser supports notifications
        if (!("Notification" in window)) {
          alert("Este navegador no soporta notificaciones");
        }

        // Let's check whether notification permissions have already been granted
        else if (Notification.permission === "granted") {
          // If it's okay let's create a notification
           var options = {
              body: "Ticket: "+ticket,
              icon: 'http://dev.gmf.technology/gmf/frontend/web/uploads/no-image.jpg',
              sound: 'http://www.freesfx.co.uk/rx2/mp3s/6/17966_1464207269.mp3'
          }
          var notification = new Notification(titulo, options);
          document.getElementById('alarma').play();
          setTimeout(notification.close.bind(notification), 10000); 
        }

        // Otherwise, we need to ask the user for permission
        else if (Notification.permission !== "denied") {
          Notification.requestPermission(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
              var notification = new Notification(titulo);
            }
          });
        }

        // At last, if the user has denied notifications, and you 
        // want to be respectful there is no need to bother them any more.
      }
</script>
