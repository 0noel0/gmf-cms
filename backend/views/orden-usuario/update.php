<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenUsuario */

$this->title = 'Update Orden Usuario: ' . $model->idordenUsuario;
$this->params['breadcrumbs'][] = ['label' => 'Orden Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idordenUsuario, 'url' => ['view', 'id' => $model->idordenUsuario]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="orden-usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
