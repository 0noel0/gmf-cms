<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenUsuarioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orden-usuario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idordenUsuario') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'idusuario_app') ?>

    <?= $form->field($model, 'idTipoPago') ?>

    <?= $form->field($model, 'html') ?>

    <?php // echo $form->field($model, 'token') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'estatus') ?>

    <?php // echo $form->field($model, 'respuestaRetorno') ?>

    <?php // echo $form->field($model, 'ticket') ?>

    <?php // echo $form->field($model, 'estadoPedido') ?>

    <?php // echo $form->field($model, 'totalOrden') ?>

    <?php // echo $form->field($model, 'fechaFinal') ?>

    <?php // echo $form->field($model, 'fechaInicio') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
