<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenUsuario */

$this->title = $model->idordenUsuario;
$this->params['breadcrumbs'][] = ['label' => 'Orden Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orden-usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idordenUsuario], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idordenUsuario], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idordenUsuario',
            'descripcion:ntext',
            'idusuario_app',
            'idTipoPago',
            'html',
            'token:ntext',
            'id',
            'estatus',
            'respuestaRetorno:ntext',
            'ticket',
            'estadoPedido',
            'totalOrden',
            'fechaFinal',
            'fechaInicio',
        ],
    ]) ?>

</div>
