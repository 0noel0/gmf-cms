<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenUsuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orden-usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'idusuario_app')->textInput() ?>

    <?= $form->field($model, 'idTipoPago')->textInput() ?>

    <?= $form->field($model, 'html')->textInput() ?>

    <?= $form->field($model, 'token')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estatus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'respuestaRetorno')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ticket')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estadoPedido')->textInput() ?>

    <?= $form->field($model, 'totalOrden')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaFinal')->textInput() ?>

    <?= $form->field($model, 'fechaInicio')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
