<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdenUsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orden Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orden-usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Orden Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idordenUsuario',
            'descripcion:ntext',
            'idusuario_app',
            'idTipoPago',
            'html',
            // 'token:ntext',
            // 'id',
            // 'estatus',
            // 'respuestaRetorno:ntext',
            // 'ticket',
            // 'estadoPedido',
            // 'totalOrden',
            // 'fechaFinal',
            // 'fechaInicio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
