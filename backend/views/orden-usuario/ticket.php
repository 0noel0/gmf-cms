  <?php

use yii\helpers\Html;
use yii\grid\GridView;
use demogorgorn\ajax\AjaxSubmitButton;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdenUsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';

?>

 <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYkhtnCIFD6yWm3v1V78OKIwmUaPQqNvk">  
</script>
  <style>
      #map {
        width: 100%;
        height: 200px;
        background-color: white;
      }
     .loader {
        display:    none;
        position:   fixed;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .9) 
                    url('http://i.stack.imgur.com/FhHRx.gif') 
                    50% 50% 
                    no-repeat;
      }
  </style>
  <!-- Audio de notificacion -->
  <audio id="alarma" src="campana.mp3" preload="auto"></audio>
  <section class="content-header">
      <h1>
        Ordenes Pendientes <strong id="totalIniciadas"><?= $totalIniciadas?></strong>
      </h1>
    </section>
   <!-- Main content -->
    <section class="content" id="content">
    
      <div class="row">
        <div class="col-md-3" id="tickets">
          <?php 
          	foreach ($dataProvider->models as $model) {
              
          		echo '<a href="#" onclick="detalleOrden('.$model->idordenUsuario.')"> 
  			            <div class="info-box " style="background-color: #fff;">
  			              <div class="info-box-content-01 box-success" style="">
                        <span class="info-box-text text-black">Ticket: '.$model->ticket.'</span>
                        <span class="info-box-text text-black">'.htmlspecialchars(utf8_decode($model->idusuarioApp->name)).' '.htmlspecialchars(utf8_decode($model->idusuarioApp->last_name)).'</span>
                        <span class="info-box-text text-black">Tel: '.$model->idusuarioApp->phone.'</span>
                        <span class="info-box-text text-black">Cel: '.$model->idusuarioApp->mobile.'</span>
                        <span class="info-box-text text-black text-right">'.$model->fechaInicio.'</span>
                        <a class="btn btn-flat btn-block btn-sm" style="color: white; background-color: #'.$model->catalogoestados->color.';" onclick="detalleOrden('.$model->idordenUsuario.')"><strong>Tomar Orden</strong></a>
                        <span></span>
                      </div>
  			              <!-- /.info-box-content -->
  			            </div>
  			          </a>';
          	}
          ?>
        </div>
        <!-- /.col -->
        <div id="view_detalle" class="col-md-9">
          <div class="box box-danger">
            <div class="box-body no-padding">
             <div id="detalle_pedido">
                <h2 class="text-center" id="mensajeLoader">No ha seleccionado ninguna Orden</h2>
                 <div id="mapa" style="visibility:hidden;">
                  <div id="map">
                   <iframe
                      width="600"
                      height="450"
                      frameborder="0" style="border:0"
                      src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCYkhtnCIFD6yWm3v1V78OKIwmUaPQqNvk&q=144.980615,-37.866963" allowfullscreen>
                    </iframe>
                  </div>
                </div>
             </div>
             <!--Redireccionar para tomar otra orden-->
             
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

 <script type="text/javascript">
    function listarRetraso() {
        $.get("index.php?r=orden-usuario/retraso&idusuario=<? \Yii::$app->user->id; ?> ", function(data){
          console.log("Pedidos retrasados: "+data.status);
          if (data.status == true) {
              console.log(data.data);
          }
        });
      }
 </script>  



<script>
      function getTiempo(idorden, idsucursal) {
        
        tiempo =  $("#tiempo").val();

        var r = confirm("Si desea Enviar la Orden con el tiempo de "+tiempo+" Minutos Presione OK, Si lo desea Cancelar presione CANCELAR; si desea cambiar el tiempo cierre esta ventana en la x");
        if (r == true) {
            
            console.log("You pressed OK!");

            var url = "index.php?r=orden-usuario/aceptar-restaurante-sin&idorden="+idorden+"&idsucursal="+idsucursal+"&tiempo="+tiempo;

            console.log("URL: "+url);

            $.post(url, function(data) {
           
            console.log("orden tomada restaurante sin tablet...");
            console.log(data);
            $("#botonAceptar").show();
            $("#botonCancelar").hide();
           
          });
        } else {
            console.log("You pressed Cancel!");
             var url = "index.php?r=orden-usuario/rechazar-restaurante-sin&idorden="+idorden+"&idsucursal="+idsucursal+"&tiempo="+tiempo;

            console.log("URL: "+url);

            $.post(url, function(data) {
           
            console.log("orden rechazada restaurante sin tablet...");
            console.log(data);
            $("#botonAceptar").show();
            $("#botonCancelar").show();
           
          });
        }


        console.log("tiempo: "+ tiempo);
        console.log("Orden: "+ idorden);
        
      }
      function detalleOrden(idorden) {

        document.getElementById("loader").style.display = "block";

        document.getElementById("mensajeLoader").html = "Cargando datos del pedido ...";

        $( "#view_detalle" ).removeClass( "col-md-9" ).addClass( "col-md-12" );
        var mapa = $( "#mapa" ).width()
        console.log(mapa);
        $.post( "index.php?r=orden-usuario/detalle-orden&idorden="+idorden+"&mapa="+mapa, function( data ) {
            $( "#detalle_pedido" ).html( data );
          });

        
        $("#tickets").hide();

        setTimeout(function () {
            document.getElementById("loader").style.display = "none";
           
        }, 2000);
      }

      function cerrarOrden($id) {
        console.log($id);
        document.getElementById("loader").style.display = "block";

        var url = "index.php?r=orden-usuario/restablecer-orden&id="+$id;

        $.post(url, function(data) {
            location.reload();
          });
         setTimeout(function () {
            document.getElementById("loader").style.display = "none";
           
        }, 1000);
      }
      function tomarOrden($id) {
        console.log($id);
        document.getElementById("loader").style.display = "block";

        var url = "index.php?r=orden-usuario/aceptar-orden&id="+$id;

        $.post(url, function(data) {
            //Ocultar botones luego de seleecionar la accion.
            console.log("orden tomada. ...ocultando");
            $("#botonAceptar").hide();
            $("#botonCancelar").hide();
            $("#botonOtraOrden").show();
             //document.getElementById("seccionAceptar").style.display = "none";
             document.getElementById("ordenAceptada").style.display = "block";

          });
         setTimeout(function () {
            document.getElementById("loader").style.display = "none";
           
        }, 1000);
      }

      function rechazarOrden($id) {
        console.log($id);
        document.getElementById("loader").style.display = "block";

        var url = "index.php?r=orden-usuario/rechazar-orden&id="+$id;

        $.post(url, function(data) {
            //Ocultar botones luego de seleecionar la accion.
            console.log("orden tomada. ...ocultando");
            $("#botonAceptar").hide();
            $("#botonCancelar").hide();
            $("#botonOtraOrden").show();
             //document.getElementById("seccionAceptar").style.display = "none";
             document.getElementById("ordenRechazada").style.display = "block";
             location.reload();
          });
         setTimeout(function () {
            document.getElementById("loader").style.display = "none";
           
        }, 1000);
      }

      //Enviar orden a Restaurante
      function aceptarRestaurante($orden, $id, $boton) {
        console.log('sucursal: '+$id);
        console.log('orden: '+$orden);
        console.log('res: '+$boton);
        
        var url = "index.php?r=orden-usuario/aceptar-restaurante&id="+$id+"&orden="+$orden+"&restaurante="+$boton;

        $.post(url, function(data) {
            //Ocultar botones luego de seleecionar la accion.
            console.log("Enviado a restaurante ...");
             $('#modal-default'+$boton).modal('hide');
             $("#"+$boton).attr("disabled", true);
             $("#"+$boton).removeClass( "btn-success" ).addClass( "btn-gray-gmf" );
             $("#rechazar-"+$boton).attr("disabled", true);
             $("#rechazar-"+$boton).removeClass( "btn-warning" ).addClass( "btn-gray-gmf" );
             $("#botonAceptar").show();
             $("#botonCancelar").hide();
          });
         setTimeout(function () {
           
        }, 1000);
      }

      //Enviar orden a Restaurante cuando es solo 1 
      function aceptarRestaurante1($orden, $id, $boton) {
        console.log('sucursal: '+$id);
        console.log('orden: '+$orden);
        console.log('res: '+$boton);
        
        var url = "index.php?r=orden-usuario/aceptar-restaurante&id="+$id+"&orden="+$orden+"&restaurante="+$boton;

        $.post(url, function(data) {
            //Ocultar botones luego de seleecionar la accion.
            console.log("Enviado a restaurante ...");
             $('#modal-default'+$boton).modal('hide');
             $("#"+$boton).attr("disabled", true);
             $("#"+$boton).removeClass( "btn-success" ).addClass( "btn-gray-gmf" );
             $("#rechazar-"+$boton).attr("disabled", true);
             $("#rechazar-"+$boton).removeClass( "btn-warning" ).addClass( "btn-gray-gmf" );
             $("#botonAceptar").show();
             $("#botonCancelar").hide();
          });
         setTimeout(function () {
           
        }, 1000);
      }

      function cerrarModal($restaurante) {
        $("#modal-default"+$restaurante).modal('hide');
        $("#modal-rechazar"+$restaurante).modal('hide');
      }

      function rechazarRestaurante($orden, $id, $boton) {
        console.log('rechazar: '+$id);
        console.log('orden: '+$orden);
        console.log('boton: '+$boton);

        var url = "index.php?r=orden-usuario/rechazar-restaurante&id="+$id+"&orden="+$orden+"&restaurante="+$boton;

       $.post(url, function(data) {
            //Ocultar botones luego de seleecionar la accion.
            console.log("Rechazando a restaurante ...");
             $("#rechazar-"+$boton).attr("disabled", true);
             $("#rechazar-"+$boton).removeClass( "btn-warning" ).addClass( "btn-gray-gmf" );
             $("#"+$boton).attr("disabled", true);
             $("#"+$boton).removeClass( "btn-success" ).addClass( "btn-gray-gmf" );
             $("#botonAceptar").show();
             //$("#botonCancelar").hide();
          });
         setTimeout(function () {
           
        }, 1000);
      }
      //Rechazar restaurante single
      function rechazarRestaurante1($orden, $id, $boton) {
        console.log('rechazar: '+$id);
        console.log('orden: '+$orden);
        console.log('boton: '+$boton);

        var url = "index.php?r=orden-usuario/rechazar-restaurante&id="+$id+"&orden="+$orden+"&restaurante="+$boton;

       $.post(url, function(data) {
            //Ocultar botones luego de seleecionar la accion.
            console.log("Rechazando a restaurante ...");
             $("#rechazar-"+$boton).attr("disabled", true);
             $("#rechazar-"+$boton).removeClass( "btn-warning" ).addClass( "btn-gray-gmf" );
             $("#"+$boton).attr("disabled", true);
             $("#"+$boton).removeClass( "btn-success" ).addClass( "btn-gray-gmf" );
             $("#botonAceptar").hide();
             $("#botonCancelar").show();
          });
         setTimeout(function () {
           
        }, 1000);
      }



      function initMap(latitud, longitud) {
        var uluru = {lat: latitud, lng: longitud};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    
      function listarNotificaciones() {
        $.get("index.php?r=orden-usuario/lista-notificaciones&idusuario=<? \Yii::$app->user->id; ?> ", function(data){
          console.log("Total de notificaciones: "+data.status);
          if (data.status == true) {
              for (var i = data.data.length - 1; i >= 0; i--) {
                //Mostramos la alerta de nueva orden
                if (data.data[i].estadoOrden == 1) {
                    notifyMe(data.data[i].ticket, 'Nueva Orden', 'index.php?r=orden-usuario/ticket');
                }
                //Mostramos la alerta de Orden retrasada Nuevo estado 18 temp solo para notificaciones
                 if (data.data[i].estadoOrden == 18) {
                    notifyMe(data.data[i].ticket, 'Han pasaso 2 minutos desde envío a restaurante', 'index.php?r=historialpedido/2minutos');
                }
                //Mostramos la alerta de Orden Aceptada por restaurante
                if (data.data[i].estadoOrden == 7) {
                    notifyMe(data.data[i].ticket, 'Orden Aceptada por Restaurante', 'index.php?r=historialpedido/aprobados-restaurante');
                }
                 //Mostramos la alerta de Orden Rechazada por Restaurante
                if (data.data[i].estadoOrden == 8) {
                    notifyMe(data.data[i].ticket, 'Orden Rechazada por Restaurante', 'index.php?r=historialpedido/rechazados-restaurante');
                }
            }
          }
          /*for (var i = data.data.length - 1; i >= 0; i--) {
            
              notifyMe(data.data[i].ticket, 'Nueva Orden');
            
          }*/
        });
      }

      function listarOrdenes() {
            //Listamos las notificaciones si existiesen
            listarRetraso();
            listarNotificaciones();
            //Mostramos la lista actualizada de tickets
            $.get( "index.php?r=orden-usuario/lista-tickets", function( data ) {
                $( "#tickets" ).html( data );
                //console.log("Ticketssssssss");
                setTimeout(listarOrdenes, 2000);
              });
      }

      listarOrdenes();

      function ordenesTotalIniciadas() {
            $.get( "index.php?r=orden-usuario/total-iniciadas", function( data ) {
                $( "#totalIniciadas" ).html( data );
                //console.log("totalIniciadas");
                setTimeout(ordenesTotalIniciadas, 2000);
              });
          }
      ordenesTotalIniciadas();

     /*
      function notifyMe(ticket, titulo) {
        // Let's check if the browser supports notifications
        if (!("Notification" in window)) {
          alert("Este navegador no soporta notificaciones");
        }

        // Let's check whether notification permissions have already been granted
        else if (Notification.permission === "granted") {
          // If it's okay let's create a notification
           var options = {
              body: "Ticket: "+ticket,
              icon: 'http://dev.gmf.technology/gmf/frontend/web/uploads/no-image.jpg',
              sound: 'http://www.freesfx.co.uk/rx2/mp3s/6/17966_1464207269.mp3'
          }
          var notification = new Notification(titulo, options);
          //Sonido de notifiacion
          document.getElementById('alarma').play();
          console.log("Mostrando notificacion! :D")
          setTimeout(notification.close.bind(notification), 10000); 
        }

        // Otherwise, we need to ask the user for permission
        else if (Notification.permission !== "denied") {
          Notification.requestPermission(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
              var notification = new Notification(titulo);
            }
          });
        }

        // At last, if the user has denied notifications, and you 
        // want to be respectful there is no need to bother them any more.
      }
      */

      function makeNotification(ticket, titulo, url) {

          var options = {
              body: "Ticket: "+ticket,
              icon: 'http://dev.gmf.technology/gmf/frontend/web/uploads/no-image.jpg',
              sound: 'http://www.freesfx.co.uk/rx2/mp3s/6/17966_1464207269.mp3'
          }

          var notification = new Notification(titulo, options);
           //Sonido de notifiacion
          document.getElementById('alarma').play();

          notification.onclick = function () {
             window.open(url,"_self");
          };
      }

      function notifyMe(ticket, titulo, url) {
          // Let's check if the browser supports notifications
          if (!("Notification" in window)) {
          alert("Este navegador no soporta notificaciones");
          }

          // Let's check if the user is okay to get some notification
          else if (Notification.permission === "granted") {
          // If it's okay let's create a notification
          makeNotification(ticket, titulo, url);
          }

          // Otherwise, we need to ask the user for permission
          // Note, Chrome does not implement the permission static property
          // So we have to check for NOT 'denied' instead of 'default'
          else if (Notification.permission !== 'denied') {
              Notification.requestPermission(function (permission) {
                // If the user is okay, let's create a notification
                if (permission === "granted") {
                  makeNotification(ticket, titulo, url);
                }
              });
          }
      }
</script>

<div class="loader" id="loader" style="display: none;">
</div>


