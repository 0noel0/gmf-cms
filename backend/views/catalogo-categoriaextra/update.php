<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;


/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoCategoriaextra */

$this->title = 'Catálogo de Extras';

$GLOBALS['id'] = $restaurante->id_restaurante;

//$id = $restaurante->id_restaurante;

?>

<?php 
    echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
            [
                'label' => 'Restaurantes',
                'url' => ['tbl-restaurantes/index'],
                'template' => "<li><b>{link}</b></li>\n", // template for this link only
            ],
            [
                'label' => $restaurante->nombre, 
                'url' => ['tbl-restaurantes/update', 'id' => $restaurante->id_restaurante]
            ],
            [
                'label' => 'Catálogo de Extras', 
                'url' => ['catalogo-categoriaextra/create', 'id' => $restaurante->id_restaurante],
                'template' => "<li><b>{link}</b></li>\n", // template for this link only
            ],
            $model->nombre,
        ],
    ]);

 ?>
<div class="catalogo-categoriaextra-update">

    <h1><?= Html::encode($model->nombre) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'restaurante'  => $restaurante,

    ]) ?>

</div>

<div>
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">

            <div class="inner text-center">
              <h3>
                <?php 
                  echo $countExtra;
                 ?>
              </h3>

              <p>Extras</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo 'index.php?r=catalogo-extra/create&id='.$model->idcatalogocategoriaextra.'&idrestaurante='.$restaurante->id_restaurante; ?>" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
   
</div>

<h2>Listado de extras del catálogo</h2>

<div>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcatalogo_extra',
            'nombre',
            'precio',
            'estado',
            'idcatalogocategoriaextra',

            [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{update}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'update') {
                        $url ='index.php?r=catalogo-extra/update&id='.$model->idcatalogo_extra.'&idrestaurante='.$GLOBALS['id'].'&idcatalogo='.$model->idcatalogocategoriaextra;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],

    ]); ?>
    
</div>
