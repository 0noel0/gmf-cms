<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoCategoriaextra */
/* @var $form yii\widgets\ActiveForm */
$id = $restaurante->id_restaurante;

//$restaurante = $_GET['name'];
?>

 
<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
        $("#catalogo").on("pjax:end", function() {
            $.pjax.reload({container:"#catalogo_grid"});  //Reload GridView
        });
    });'
);
?>

<div class="catalogo-categoriaextra-form">

    <?php yii\widgets\Pjax::begin(['id' => 'catalogo']) ?>

        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'idrestaurante')->hiddenInput(['value' => $id])->label(false); ?>

            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'requerido')->dropDownList(['No'=>'No', 'Si'=>'Si'], ['maxlength' => true]) ?>

            <?= $form->field($model, 'multiple')->dropDownList(['No'=>'No', 'Si'=>'Si'], ['maxlength' => true]) ?>

            <?= $form->field($model, 'orden') ?>

            <?= $form->field($model, 'estado')->dropDownList(['1'=>'Activo','0'=>'Inactivo'], ['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>

</div>
