<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CatalogoCategoriaextraSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Catalogo Categoriaextras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogo-categoriaextra-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Catalogo Categoriaextra', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcatalogocategoriaextra',
            'idrestaurante',
            'requerido',
            'multiple',
            'nombre',
            // 'estado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
