<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;


/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoCategoriaextra */

$this->title = 'Agregar Catálogo de Extras';

$id = $restaurante->id_restaurante;
?>

<?php 
    echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
        [
            'label' => 'Restaurantes',
            'url' => ['tbl-restaurantes/index'],
            'template' => "<li><b>{link}</b></li>\n", // template for this link only
        ],
        [
            'label' => $restaurante->nombre, 
            'url' => ['tbl-restaurantes/update', 'id' => $restaurante->id_restaurante]
        ],
        'Catálogo de Extras',
        ],
    ]);

 ?>

<div class="catalogo-categoriaextra-create">


    <?= $this->render('_form', [
        'model' => $model,
        'restaurante' => $restaurante,

    ]) ?>

</div>


<h1>Catalogo de categorias de Extra del Restaurante</h1>

<div>
<?php Pjax::begin(['id' => 'catalogo_grid']) ?>
	 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcatalogocategoriaextra',
            'requerido',
            'multiple',
            'nombre',
            'orden',
            // 'estado',
 [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{update}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'update') {
                        $url ='index.php?r=catalogo-categoriaextra/update&id='.$model->idcatalogocategoriaextra.'&idrestaurante='.$model->idrestaurante;

                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>