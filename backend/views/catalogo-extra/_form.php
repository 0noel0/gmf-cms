<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Estatus;

/* @var $this yii\web\View */
/* @var $model backend\models\CatalogoExtra */
/* @var $form yii\widgets\ActiveForm */

$id = $_GET['id'];
?>

<div class="catalogo-extra-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'precio')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'estado')->dropDownList(Estatus::listEstatus()) ?>

    <?= $form->field($model, 'idcatalogocategoriaextra')->hiddenInput(['value' => $id])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
