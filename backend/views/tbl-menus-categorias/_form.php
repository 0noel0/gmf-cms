<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\TblRestaurantes;
use backend\models\Estatus;

/* @var $this yii\web\View */
/* @var $model backend\models\TblMenusCategorias */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="tbl-menus-categorias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_restaurante')->hiddenInput(['value' => $restaurante->id_restaurante])->label(false); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'orden')->textInput() ?>

    <?= $form->field($model, 'estatus')->dropDownList(['1'=>'Activo', '0'=>'Inactivo']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
