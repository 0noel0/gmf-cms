<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\TblRestaurantes;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblMenusCategoriasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categorías de plato';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-menus-categorias-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nueva Categoría de plato', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id_categoria',
            'nombre',
            [
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    try {
                        return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],  
            'orden',
            [
                'attribute' => 'estatus',
                'value' => function ($model) {
                    try {
                        return ($model->eestatus->estatus);
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ], 

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
w