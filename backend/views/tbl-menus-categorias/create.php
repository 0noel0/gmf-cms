<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\TblRestaurantes;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\models\TblMenusCategorias */
$id = $_GET['id'];
$this->title = $restaurante->nombre;

?>

<?php 
    echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
        [
            'label' => 'Restaurantes',
            'url' => ['tbl-restaurantes/index'],
            'template' => "<li><b>{link}</b></li>\n", // template for this link only
        ],
        [
            'label' => $restaurante->nombre, 
            'url' => ['tbl-restaurantes/update', 'id' => $restaurante->id_restaurante]
        ],
        'Categorías de Plato',
        ],
    ]);

 ?>

<div class="tbl-menus-categorias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'restaurante' => $restaurante,
    ]) ?>

</div>

<div>
	<h1>Categorias de plato</h1>
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id_categoria',
            'nombre',
            /*[
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    try {
                        return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],*/  
            'orden',
            [
                'attribute' => 'estatus',
                'value' => function ($model) {
                    try {
                        return ($model->eestatus->estatus);
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ], 

             [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=tbl-menus-categorias/update&id='.$model->id_categoria.'&idrestaurante='.$model->id_restaurante;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
</div>
