<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\models\TblRestaurantes;
use backend\models\MenusPlatosMovil;
use backend\models\Extra;
use backend\models\CategoriaPlato;

/* @var $this yii\web\View */
/* @var $model backend\models\Extra */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="extra-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
       $modelRestaurantes = TblRestaurantes::find()->all();
       $data = array();
       foreach ($modelRestaurantes as $mod){
            $data[$mod->id_restaurante] = $mod->nombre;   
       }

       echo $form->field($model, 'id_restaurante')->dropDownList(TblRestaurantes::listTblRestaurantes(), ['onchange' => '$.post("'.Yii::$app->urlManager->createUrl(["extra/showplatos"]).'",{idres: $(this).val()}, function(data){
                $("#renderPlatos").html(data);
       })'], array('prompt'=>'Seleccione un Restaurante'));
    ?>

    <div id="renderPlatos">    
     <?= $form->field($model, 'id_plato_movil')->dropDownList(MenusPlatosMovil::ListMenusplatosmovil(), ['onchange' => '$.post("'.Yii::$app->urlManager->createUrl(["extra/showcatsextra"]).'",{idplato: $(this).val()}, function(data){
                $("#renderCatsExtra").html(data);
       })'], array('prompt'=>'Seleccione un Plato'));
     ?>
    </div>

    <div id="renderCatsExtra">
    	<?php echo $form->field($model, 'idcategoria_plato')->dropDownList(CategoriaPlato::listCategoriaplato());  ?>
    </div>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'precio')->textInput() ?>

    <?= $form->field($model, 'estado')->dropDownList([0 => 'Inactivo', 1 => 'Activo']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

