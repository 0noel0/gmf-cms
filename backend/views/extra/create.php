<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Extra */

$this->title = 'Nuevo Extra';
$this->params['breadcrumbs'][] = ['label' => 'Extras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extra-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
