<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\TblRestaurantes;

/* @var $this yii\web\View */
/* @var $model backend\models\Extra */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Extras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extra-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idextra], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idextra], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que deseas eliminar el item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idextra',
            [
                'attribute' => 'id_restaurante',
                'value' => $model->tblrestaurantes[0]->nombre,
            ],
            [
                'attribute' => 'id_plato_movil',
                'value' => $model->menusplatomovil->nombre,
            ],
            [
                'attribute' => 'idcategoria_plato',
                'value' => $model->categoriaplato->nombre,
            ],
            'nombre',
            'precio',
            [
                'attribute' => 'estado',
                'value' => $model->eestatus->estatus,
            ],
        ],
    ]) ?>

</div>
