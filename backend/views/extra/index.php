<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\TblRestaurantes;
use backend\models\CategoriaPlato;
use backend\models\MenusPlatosMovil;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ExtraSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Extras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extra-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nuevo Extra', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'idextra',
            [
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    try {
                        return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                        echo "N/A";
                    }
                },
            ],
            [
                'attribute' => 'id_plato_movil',
                'filter' => MenusPlatosMovil::listMenusplatosmovil(),
                'value' => function ($model) {
                    try {
                        return $model->idplatoMovil->nombre;
                    } catch (Exception $e) {
                        echo "N/A";
                    }
                },
            ],
            [
                'attribute' => 'idcategoria_plato',
                'filter' => CategoriaPlato::listCategoriaplato(),
                'value' => function ($model) {
                    try {
                        return $model->categoriaplato->nombre;
                    } catch (Exception $e) {
                        echo "N/A";
                    }
                },
            ],
            'nombre',
            'precio',
            [
                'attribute' => 'estado',
                'value' => function ($model) {
                    try {
                        return ($model->eestatus->estatus);
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ], 

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
