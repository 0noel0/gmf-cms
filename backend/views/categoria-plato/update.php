<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\TblRestaurantes;
use backend\models\CategoriaPlato;
use backend\models\MenusPlatosMovil;


/* @var $this yii\web\View */
/* @var $model backend\models\CategoriaPlato */

$this->title = $model->tblrestaurantes[0]->nombre.' > '.$model->menusplatosmovil[0]->nombre.' > '.$model->nombre; 
/*$this->params['breadcrumbs'][] = ['label' => 'Categoria de Extras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->idcategoria_plato]];
$this->params['breadcrumbs'][] = 'Actualizar';
*/
?>
<div class="categoria-plato-update">

    <h1><?= Html::encode($model->nombre) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<h1>Extras de la categoría</h1>

<div>
	 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'idextra',
            /*[
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    try {
                        return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                        echo "N/A";
                    }
                },
            ],
            [
                'attribute' => 'id_plato_movil',
                'filter' => MenusPlatosMovil::listMenusplatosmovil(),
                'value' => function ($model) {
                    try {
                        return $model->idplatoMovil->nombre;
                    } catch (Exception $e) {
                        echo "N/A";
                    }
                },
            ],
            [
                'attribute' => 'idcategoria_plato',
                'filter' => CategoriaPlato::listCategoriaplato(),
                'value' => function ($model) {
                    try {
                        return $model->categoriaplato->nombre;
                    } catch (Exception $e) {
                        echo "N/A";
                    }
                },
            ],*/
            'nombre',
            'precio',
            [
                'attribute' => 'estado',
                'value' => function ($model) {
                    try {
                        return ($model->eestatus->estatus);
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ], 
            [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=extra/update&id='.$model->idextra;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
            
        ],
    ]); ?>
</div>
