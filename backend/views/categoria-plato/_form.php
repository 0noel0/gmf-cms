<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\MenusPlatosMovil;
use backend\models\TblRestaurantes;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoriaPlato */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categoria-plato-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
       $modelRestaurantes = TblRestaurantes::find()->all();
       $data = array();
       foreach ($modelRestaurantes as $mod){
            $data[$mod->id_restaurante] = $mod->nombre;   
       }

	   echo $form->field($model, 'id_restaurante')->dropDownList(TblRestaurantes::listTblRestaurantes(), ['onchange' => '$.post("'.Yii::$app->urlManager->createUrl(["categoria-plato/showplatos"]).'",{idres: $(this).val()}, function(data){
	            $("#renderPlatos").html(data);
       })'], array('prompt'=>'Seleccione un Restaurante'));
    ?>

    <div id="renderPlatos">

    
     <?= $form->field($model, 'id_plato_movil')->dropDownList(MenusPlatosMovil::ListMenusplatosmovil(), array('prompt'=>'Seleccione un Plato')) ?>
    
    </div>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado')->dropDownList(['0'=>'Inactivo', '1'=>'Activo'], ['maxlength' => true]) ?>

    <?= $form->field($model, 'orden') ?>

    <?= $form->field($model, 'multiple')->dropDownList(['No'=>'No', 'Si'=>'Si'], ['maxlength' => true]) ?>

    <?= $form->field($model, 'requerido')->dropDownList(['No'=>'No', 'Si'=>'Si'], ['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
