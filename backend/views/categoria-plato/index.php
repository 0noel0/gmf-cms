<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\MenusPlatosMovil;
use backend\models\TblRestaurantes;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriaPlatoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categoria de Extras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoria-plato-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nueva Categoria de Extras', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        
            'idcategoria_plato',
            [
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                //'filter'=>ArrayHelper::map(TblRestaurantes::find()->asArray()->all(), 'id_restaurante', 'nombre'),
                'value' => function ($model) {
                    try {
                        return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],     
            [
                'attribute' => 'id_plato_movil',
                'filter' => MenusPlatosMovil::listMenusplatosmovil(),
                'value' => function ($model) {
                    try {
                        return ($model->menusplatosmovil[0]->nombre);
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],
            'nombre',
            'estado',
            'multiple',
            'requerido',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
