<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\MenusPlatosMovil;
use backend\models\TblRestaurantes;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoriaPlato */

$this->title = 'Nueva Categoria de extras';
$this->params['breadcrumbs'][] = ['label' => 'Categoria de Extras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoria-plato-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<h1>Categorias de plato del Restaurante</h1>

<div>
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
        
            'idcategoria_plato',
            /*[
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                //'filter'=>ArrayHelper::map(TblRestaurantes::find()->asArray()->all(), 'id_restaurante', 'nombre'),
                'value' => function ($model) {
                    try {
                        return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],*/     
            [
                'attribute' => 'id_plato_movil',
                'filter' => MenusPlatosMovil::listMenusplatosmovil(),
                'value' => function ($model) {
                    try {
                        return ($model->menusplatosmovil[0]->nombre);
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],
            'nombre',
            'multiple',
            'requerido',
            'estado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
