<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\TblRestaurantes;
use backend\models\MenusPlatosMovil;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoriaPlato */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Categoria de Extras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoria-plato-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idcategoria_plato], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idcategoria_plato], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro que quiere eliminar el item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcategoria_plato',
            [
                'attribute' => 'id_restaurante',
                'value' => $model->tblrestaurantes[0]->nombre,
            ],
            [
                'attribute' => 'id_plato_movil',
                'value' => $model->menusplatosmovil[0]->nombre,
            ],
            'nombre',
            'multiple',
            'requerido',
            [
                'attribute' => 'estado',
                'value' => $model->estatus[0]->estatus,
            ],
        ],
    ]) ?>

</div>
