<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\MenusPlatosMovil;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DetalleCategoriaPlatoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Platos/Categoría Extra';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detalle-categoria-plato-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear nuevo Plato/Categoría Extra', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'iddetalle_categoria_plato',
            //'idcategoria_plato',
            'nombre_categoria',
            [
                'attribute' => 'id_plato_movil',
                'filter' => MenusPlatosMovil::listMenusplatosmovil(),
                'value' => function ($model) {
                    try {
                        return ($model->menusplatosmovil[0]->nombre);
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],  

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
