<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DetalleCategoriaPlato */

$this->title = 'Crear nuevo Plato/Categoría Extra';
$this->params['breadcrumbs'][] = ['label' => 'Detalle Categoria Platos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detalle-categoria-plato-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
