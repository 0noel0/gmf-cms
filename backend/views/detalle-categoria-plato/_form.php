<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\TblRestaurantes;
use backend\models\MenusPlatosMovil;
//use backend\models\DetalleCategoriaPlato;

/* @var $this yii\web\View */
/* @var $model backend\models\DetalleCategoriaPlato */
/* @var $form yii\widgets\ActiveForm */
?>

<?php //Detalle de categoria de plato ?>
<div class="detalle-categoria-plato-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group required has-success">
        <label class="control-label" for="detalle-categoria-plato-id_restaurante">Restaurante</label>
         <?= Html::dropDownList('id_restaurante',null, ArrayHelper::map(TblRestaurantes::find()->all(), 'id_restaurante', 'nombre'), ['onchange' => '$.post("'.Yii::$app->urlManager->createUrl(["detalle-categoria-plato/showplatos"]).'",{idres: $(this).val()}, function(data){
            $("#renderPlatos").html(data);
        })']) ?>
    </div>

    <div id="renderPlatos">

     <?= $form->field($model, 'id_plato_movil')->dropDownList(MenusPlatosMovil::filterPlatos(7)) ?>

    </div>

    <?= $form->field($model, 'idcategoria_plato')->textInput() ?>

    <?= $form->field($model, 'nombre_categoria')->textInput(['maxlength' => true]) ?>
   
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
