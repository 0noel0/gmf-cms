<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DetalleCategoriaPlato */

$this->title = $model->nombre_categoria;
$this->params['breadcrumbs'][] = ['label' => 'Platos/Categoría Extra', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detalle-categoria-plato-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->iddetalle_categoria_plato], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->iddetalle_categoria_plato], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'iddetalle_categoria_plato',
            'id_plato_movil',
            'idcategoria_plato',
            'nombre_categoria',
        ],
    ]) ?>

</div>
