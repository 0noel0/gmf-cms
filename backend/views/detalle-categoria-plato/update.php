<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DetalleCategoriaPlato */

$this->title = 'Actualizar Plato/Categoría Extra: ' . $model->iddetalle_categoria_plato;
$this->params['breadcrumbs'][] = ['label' => 'Detalle Categoria Platos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddetalle_categoria_plato, 'url' => ['view', 'id' => $model->iddetalle_categoria_plato]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="detalle-categoria-plato-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
