<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DetalleCategoriaPlatoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detalle-categoria-plato-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'iddetalle_categoria_plato') ?>

    <?= $form->field($model, 'id_plato_movil') ?>

    <?= $form->field($model, 'idcategoria_plato') ?>

    <?= $form->field($model, 'nombre_categoria') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
