<?php
use yii\helpers\Html;
use Yii AS Yii;
use common\components\Util;
use backend\models\OrdenUsuarioIniciadas;
use backend\models\OrdenesRechazadasRestaurante;
use backend\models\HistorialRechazadoRestauranteSearch;
use backend\models\HistorialRechazadoRestaurante;
use backend\models\Historial;
use demogorgorn\ajax\AjaxSubmitButton;
/* @var $this \yii\web\View */
/* @var $content string */
?>


<?php 

      $iniciadas = OrdenUsuarioIniciadas::find()->all();
      $totalIniciadas = count($iniciadas);

      $rechazadas = HistorialRechazadoRestaurante::find()->where('TIMESTAMPDIFF(MINUTE,fechacreacion,NOW()) < 1440')->all();
      $totalRechazadas = count($rechazadas)
 ?>

<style>
    .language-picker{
        float: right;
        margin-top: 20px;
        margin-right: 30px;
    }
    .kv-file-content img{
        width: 250px;
    }
    
</style>
<header class="main-header">
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

    <?= Html::a('<span class="logo-mini">GMF</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu " id="rechazadas">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                      <i class="fa fa-bell-o"></i>
                      <span class="label label-warning"><?= $totalRechazadas ?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header"><?= $totalRechazadas ?> Ordenes rechazadas</li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                          
                        <?php foreach ($rechazadas as $rechazada) {

                             $tiempo = $rechazada->fechacreacion;

                            date_default_timezone_set('America/El_Salvador');

                            $tiempo = strtotime ( '+6 hour' , strtotime ( $tiempo ) ) ;
                            $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );
                            //$tiempo = strtotime ( '-21 minutes' , strtotime ( $tiempo ) ) ;
                            //$tiempo = date ( 'Y-m-d H:m:s' , $tiempo );
                            $tiempo = ( $tiempo < 60*60*24*365)? Yii::$app->formatter->asRelativeTime($tiempo ): Yii::$app->formatter->asDate($tiempo );

                            echo '<li>
                                    <a href="index.php?r=orden-usuario/ticket">
                                      <i class="fa fa-warning text-yellow"></i>Ticket: '.$rechazada->orden->ticket.' <br/>Restaurante:  '.$rechazada->nombreRestaurante.' <br/> '.$tiempo.'
                                    </a>
                                  </li>';
                        } ?>
                        </ul>
                      </li>
                    
                    </ul>
                </li>
                <li class="dropdown notifications-menu " id="iniciadas">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                      <i class="fa fa-calendar-check-o"></i>
                      <span class="label label-success"><?= $totalIniciadas ?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header"><?= $totalIniciadas ?> Ordenes iniciadas</li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                          
                        <?php foreach ($iniciadas as $iniciada) {

                            $tiempo = $iniciada->orden->fechaInicio;

                             $tiempo = strtotime ( '+6 hour' , strtotime ( $tiempo ) ) ;
                            $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );
                            //$tiempo = strtotime ( '-21 minutes' , strtotime ( $tiempo ) ) ;
                            //$tiempo = date ( 'Y-m-d H:m:s' , $tiempo );
                            $tiempo = ( $tiempo < 60*60*24*365)? Yii::$app->formatter->asRelativeTime($tiempo ): Yii::$app->formatter->asDate($tiempo );

                            echo '<li>
                                    <a href="index.php?r=orden-usuario/ticket">
                                      <i class="fa fa-calendar-check-o text-yellow"></i> Ticket: '.$iniciada->orden->ticket.' - '.$tiempo.'.
                                    </a>
                                  </li>';
                        } ?>
                        </ul>
                      </li>
                    
                    </ul>
                </li>
             <!-- User Account: style can be found in dropdown.less -->
                
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="http://gmf.technology/gmf/frontend/web/uploads/no-image3.jpg" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="http://gmf.technology/gmf/frontend/web/uploads/no-image3.jpg" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?= Yii::$app->user->identity->username ?>
                                <small>Miembro desde <?= Yii::$app->user->identity->created_at ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a(Yii::t('app', 'Profile'), ['/user/update?id='.Yii::$app->user->getId()], ['class' => 'btn btn-default btn-flat']) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
                
                <li>
                    <?= \lajax\languagepicker\widgets\LanguagePicker::widget([
                        'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_DROPDOWN,
                        'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_LARGE,
                    ]);?>
                </li>
                
              </ul>
                     
            </ul>
        </div>
    </nav>
</header>

<script type="text/javascript">
  function ordenesIniciadas() {
        $.get( "index.php?r=orden-usuario/ordenes-iniciadas", function( data ) {
            $( "#iniciadas" ).html( data );
            //console.log("Success");
            setTimeout(ordenesIniciadas, 2000);
          });
      }
  ordenesIniciadas();
  
  function ordenesRechazadas() {
        $.get( "index.php?r=orden-usuario/ordenes-rechazadas", function( data ) {
            $( "#rechazadas" ).html( data );
            //console.log("Success rechazadas");
            setTimeout(ordenesRechazadas, 2000);
          });
      }
  ordenesRechazadas();
</script>
