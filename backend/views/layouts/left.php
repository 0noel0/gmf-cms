
<?php
    use common\components\Util;
    use common\models\User;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="http://gmf.technology/gmf/frontend/web/uploads/no-image3.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> En linea</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Informacion de App Móvil', 'options' => ['class' => 'header']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Categorías de Restaurante',
                        'icon' => 'fa fa-folder',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Listado', 'icon' => 'fa fa-list-ul', 'url' => ['/tbl-restaurantes-cats'],],
                            ['label' => 'Agregar', 'icon' => 'fa fa-plus-square', 'url' => ['/tbl-restaurantes-cats/create'],],
                        ],
                        //'visible' => Yii::$app->user->can("/user/*"),
                    ],
                    [
                        'label' => 'Restaurantes',
                        'icon' => 'fa fa-cutlery',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Listado', 'icon' => 'fa fa-list-ul', 'url' => ['/tbl-restaurantes'],],
                            ['label' => 'Agregar', 'icon' => 'fa fa-plus-square', 'url' => ['/tbl-restaurantes/create'],],
                        ],
                    ],
                    /*[
                        'label' => 'Restaurantes/Categorías',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Listado', 'icon' => 'fa fa-list-ul', 'url' => ['/rel-restaurantes-cats'],],
                            ['label' => 'Agregar', 'icon' => 'fa fa-plus-square', 'url' => ['/rel-restaurantes-cats/create'],],
                        ],
                    ],
                    [
                        'label' => 'Categorías de Plato',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Listado', 'icon' => 'fa fa-list-ul', 'url' => ['/tbl-menus-categorias'],],
                            ['label' => 'Agregar', 'icon' => 'fa fa-plus-square', 'url' => ['/tbl-menus-categorias/create'],],
                        ],
                    ],
                    [
                        'label' => 'Platos App Móvil',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Listado', 'icon' => 'fa fa-list-ul', 'url' => ['/menus-platos-movil'],],
                            ['label' => 'Agregar', 'icon' => 'fa fa-plus-square', 'url' => ['/menus-platos-movil/create'],],
                        ],
                    ],
                    [
                        'label' => 'Categorías de Extras',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Listado', 'icon' => 'fa fa-list-ul', 'url' => ['/categoria-plato'],],
                            ['label' => 'Agregar', 'icon' => 'fa fa-plus-square', 'url' => ['/categoria-plato/create'],],
                        ],
                    ],
                    [
                        'label' => 'Extras',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Listado', 'icon' => 'fa fa-list-ul', 'url' => ['/extra'],],
                            ['label' => 'Agregar', 'icon' => 'fa fa-plus-square', 'url' => ['/extra/create'],],
                        ],
                    ],*/
                    [
                        'label' => 'Usuarios App Móvil',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Listado', 'icon' => 'fa fa-list-ul', 'url' => ['/mobile-users'],],
                        ],
                    ],
                    /*
                    [
                        'label' => 'Usuarios',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Listado', 'icon' => 'fa fa-list-ul', 'url' => ['/user'],],
                            ['label' => 'Agregar', 'icon' => 'fa fa-plus-square', 'url' => ['/user/create'],],
                        ],
                    ],
                    
                        [
                        'label' => 'Perfil',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Profile', 'icon' => 'fa fa-user', 'url' => ['/user/view&id='.Yii::$app->user->getId()],],
                            ['label' => 'Change password', 'icon' => 'fa fa-plus-square', 'url' => ['/user/update&id='.Yii::$app->user->getId()],],
                        ],
                    ],
                    */
                    ['label' => 'Sistema de tickets', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Pantalla de Tickets', 
                        'icon' => 'fa fa-ticket', 
                        'url' => ['/orden-usuario/ticket'],
                    ],
                    [
                        'label' => 'Despacho',
                        'icon' => 'fa fa-motorcycle',
                        'url' => ['orden-usuario/despacho'],

                    ],
                    [
                        'label' => 'Historial       de       pedidos',
                        'icon' => 'fa fa-clock-o',
                        'url' => '#',
                        'items' => [
                                ['label' => 'Pendientes', 'icon' => 'fa fa-list-ul', 'url' => ['historialpedido/pendientes'],],
                                ['label' => 'Aprobados por Restaurante', 'icon' => 'fa fa-list-ul', 'url' => ['historialpedido/aprobados-restaurante'],],
                                ['label' => 'Rechazadas por Restaurante', 'icon' => 'fa fa-list-ul', 'url' => ['historialpedido/rechazados-restaurante'],],
                                ['label' => 'Recibidos por Motorista (En Camino)', 'icon' => 'fa fa-list-ul', 'url' => ['historialpedido/recibidos-motorista'],],
                                ['label' => 'Entregados', 'icon' => 'fa fa-list-ul', 'url' => ['historialpedido/entregados'],],
                                ['label' => 'Ordenes Perdidas', 'icon' => 'fa fa-list-ul', 'url' => ['historialpedido/ordenes-perdidas'],],
                                ['label' => 'Ordenes Atrasadas', 'icon' => 'fa fa-list-ul', 'url' => ['historialpedido/2minutos'],],
                                ['label' => 'Todos', 'icon' => 'fa fa-list-ul', 'url' => ['historialpedido/historial'],],
                        ],
                        //'visible' => Yii::$app->user->can("/user/*"),
                    ],
                    [
                        'label' => 'Usuarios',
                        'icon' => 'fa fa-phone-square',
                        'url' => ['user/index'],
                    ],
                    [
                        'label' => 'Motoristas',
                        'icon' => 'fa fa-user-circle-o',
                        'url' => ['motorista/index'],
                    ],
                    [
                        'label' => 'Detalle de Abono',
                        'icon' => 'fa fa-file-text-o',
                        'url' => ['ordenes-usuario-tarifa-precio/index'],
                    ],
                    [
                        'label' => 'Notificaciones Generales',
                        'icon' => 'fa fa-exclamation-circle',
                        'url' => ['notificacion-general/index'],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
