<?php

/* @var $this yii\web\View */

$this->title = '';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Sistema de administración de datos de GMF Móvil</h1>

        <p class="lead">En Get my food! nos dedicamos a ofrecer servicio a domicilio de los mejores restaurantes de El Salvador. Get My Food! puede ayudarte a encontrar y ordenar tu comida favorita desde cualquier lugar que te encuentres.

Busca tu restaurante favoritos, ordena tus platillos preferidos y nosotros entregamos tu comida a la puerta de tu casa u oficina.

Puedes buscar por tipo de cocina, platos y restaurantes. Nosotros te brindaremos recomendaciones de acuerdo a tus preferencias. Cuando encuentres lo que buscas, haces tu orden por teléfono. Además con nosotros tendrás acceso a ofertas especiales, entregas gratuitas y servicio al cliente con el cual podrás rastrear cada una de tus órdenes para asegurarte de recibir exactamente lo que ordenaste.</p>

    </div>

    <div class="body-content">

    </div>
</div>
