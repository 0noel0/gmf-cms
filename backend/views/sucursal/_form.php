<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Estatus;


/* @var $this yii\web\View */
/* @var $model backend\models\Sucursal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sucursal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'latitud')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitud')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zona')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'password_hash')->passwordInput() ?>

    <?= $form->field($model, 'id_restaurante')->hiddenInput(['value' => $restaurante->id_restaurante])->label(false) ?>
     <?= $form->field($model, 'estado')->dropDownList(Estatus::listEstatus()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
