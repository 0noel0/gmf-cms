<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\models\Sucursal */

$this->title = 'Editar Sucursal: ' . $model->zona;
?>
<?php 
    echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
            [
                'label' => 'Restaurantes',
                'url' => ['tbl-restaurantes/index'],
                'template' => "<li><b>{link}</b></li>\n", // template for this link only
            ],
            [
                'label' => $restaurante->nombre, 
                'url' => ['tbl-restaurantes/update', 'id' => $restaurante->id_restaurante]
            ],
            [
                'label' => 'Sucursales', 
                'url' => ['sucursal/create', 'id' => $restaurante->id_restaurante],
                'template' => "<li><b>{link}</b></li>\n", 
            ],
            $model->zona,
        ],
    ]);

 ?>
<div class="sucursal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'restaurante' => $restaurante,
    ]) ?>

</div>
