<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;


/* @var $this yii\web\View */
/* @var $model backend\models\Sucursal */

$this->title = 'Agregar Sucursal';
?>
<?php 
    echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
            [
                'label' => 'Restaurantes',
                'url' => ['tbl-restaurantes/index'],
                'template' => "<li><b>{link}</b></li>\n", // template for this link only
            ],
            [
                'label' => $restaurante->nombre, 
                'url' => ['tbl-restaurantes/update', 'id' => $restaurante->id_restaurante]
            ],
            'Sucursales',
        ],
    ]);

 ?>
<div class="sucursal-create">

    <?= $this->render('_form', [
        'model' => $model,
        'restaurante' => $restaurante,
    ]) ?>

</div>

<h1>Sucursales del Restaurante</h1>

<div>
	   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idsucursal',
            'zona',
            'latitud',
            'longitud',
            //'id_restaurante',
            [
                'attribute' => 'estado',
                'value' => function ($model) {
                    try {
                        if ($model->estado == 1) {
                            return 'Activo';
                        }else{
                            return 'Inactivo';
                        }
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
  			[
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{update}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    /*if ($action === 'view') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil.'&idrestaurante='.$model->id_restaurante;
                        return $url;
                    }*/

                    if ($action === 'update') {
                        $url ='index.php?r=sucursal/update&id='.$model->idsucursal.'&restaurante='.$model->id_restaurante;
                        return $url;
                    }/*
                    if ($action === 'delete') {
                        $url ='index.php?r=sucursal/delete&id='.$model->idsucursal;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
</div>
