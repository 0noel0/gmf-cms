<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\TblRestaurantes;
use backend\models\TblMenusCategorias;

/* @var $this yii\web\View */
/* @var $model backend\models\MenusPlatosMovil */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Platos App Móvil', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menus-platos-movil-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_plato_movil], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_plato_movil], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro que desea eliminar este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_plato_movil',
            [
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::ListTblRestaurantes(),
                'value' => $model->tblrestaurantes[0]->nombre,
            ],
            [
                'attribute' => 'id_categoria',
                'filter' => TblMenusCategorias::ListTblMenusCategorias(),
                'value' => $model->tblmenuscategorias[0]->nombre,
            ],
            'nombre:ntext',
            'descripcion:ntext',
            'precio1',
            'orden',
            'fotografia:ntext',
            [
                'attribute' => 'estatus',
                'value' => $model->eestatus->estatus,
            ],
            [
                'attribute' => 'idTipoPedido',
                'value' => $model->tipopedido->nombre,
            ],
        ],
    ]) ?>

</div>
<div>
    <label>Vista previa de Fotografía</label>
    <br>
    <img src="http://getmyfood.com.sv/img/products/<?php echo $model->fotografia; ?>">
    <br>
    <span><?php echo $model->fotografia; ?></span>
</div>
