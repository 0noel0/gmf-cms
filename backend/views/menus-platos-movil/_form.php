<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use backend\models\TblRestaurantes;
use backend\models\TblMenusCategorias;
use backend\models\Estatus;
use backend\models\TipoPedido;
use backend\models\CatalogoCategoriaextra;
use common\components\Util;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\MenusPlatosMovil */
/* @var $form yii\widgets\ActiveForm */


$id = $restaurante->id_restaurante;
?>

<div class="menus-platos-movil-form">
    <h4>Agregar nuevo plato</h4>

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
        ]);
    ?>

    <?= $form->field($model, 'id_restaurante')->hiddenInput(['value' => $restaurante->id_restaurante])->label(false) ?>
    
    <?= $form->field($model, 'id_categoria')->dropDownList(ArrayHelper::map(TblMenusCategorias::find()->where(['id_restaurante' => $restaurante->id_restaurante])->All(), 'id_categoria','nombre'))  ?>

    <?= $form->field($model, 'estatus')->dropDownList(['1'=>'Activo', '0'=>'Inactivo']) ?>

    <?= $form->field($model, 'orden')->textInput() ?>

    <?= $form->field($model, '_fotografia')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'allowedFileExtensions'=>['jpg', 'gif', 'png'],
                'initialPreview'=>[
                    Html::img(Util::getUrlPlato($model->fotografia))
                ],
                'overwriteInitial'=>true,
                'showUpload' => false,
                'showCaption' => false,
                'maxFileSize'=>2000
            ]
        ]);  
    ?>

    <?= $form->field($model, 'nombre')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'precio1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idTipoPedido')->dropDownList(TipoPedido::listTipoPedido()) ?>

    <h2>Seleccione las categorias de Extra para agregar al plato</h2>

    <?= $form->field($modelCatalogo, 'idcatalogocategoriaextra')->CheckboxList(ArrayHelper::map(CatalogoCategoriaextra::find()->where(['idrestaurante' => $id])->All(), 'idcatalogocategoriaextra',function($model, $defaultValue) {
        return $model['nombre'].'-'.$model['idcatalogocategoriaextra'];
    }));?>
   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

