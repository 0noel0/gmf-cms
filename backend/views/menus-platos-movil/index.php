<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\TblRestaurantes;
use backend\models\TblMenusCategorias;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MenusPlatosMovilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Platos App Móvil ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menus-platos-movil-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear nuevo plato', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            
            'id_plato_movil',
            [
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    try {
                          return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            [
                'attribute' => 'id_categoria',
                'filter' => TblMenusCategorias::ListTblmenuscategorias(),
                'value' => function ($model) {
                    try {
                          return $model->tblmenuscategorias[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            'nombre:ntext',
            //'descripcion:ntext',
            //'estatus',
            // 'fotografia:ntext',
            // 'precio1',
            // 'idTipoPedido',
            'orden',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
