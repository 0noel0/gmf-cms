<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\TblRestaurantes;
use backend\models\TblMenusCategorias;
use yii\widgets\Breadcrumbs;


/* @var $this yii\web\View */
/* @var $model backend\models\MenusPlatosMovil */

$this->title = 'Platos';
?>

<?php 
    echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
            [
                'label' => 'Restaurantes',
                'url' => ['tbl-restaurantes/index'],
                'template' => "<li><b>{link}</b></li>\n", // template for this link only
            ],
            [
                'label' => $restaurante->nombre, 
                'url' => ['tbl-restaurantes/update', 'id' => $restaurante->id_restaurante]
            ],
            'Platos',
        ],
    ]);

 ?>
<div class="menus-platos-movil-create">


    <?= $this->render('_form', [
        'model' => $model,
        'modelCatalogo' => $modelCatalogo,
        'restaurante' => $restaurante,
     ]) ?>

</div>

<h1>Platos del Restaurante</h1>

<div>
	 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            'id_plato_movil',
            /*[
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                'value' => function ($model) {
                    try {
                          return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],*/
            [
                'attribute' => 'id_categoria',
                'filter' => TblMenusCategorias::ListTblmenuscategorias(),
                'value' => function ($model) {
                    try {
                          return $model->tblmenuscategorias[0]->nombre;
                    } catch (Exception $e) {
                          echo "N/A";
                    }
                },
            ],
            'nombre:ntext',
            //'descripcion:ntext',
            //'estatus',
            // 'fotografia:ntext',
            // 'precio1',
            // 'idTipoPedido',
            'orden',


              [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil.'&idrestaurante='.$model->id_restaurante;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
</div>
<div id="footer"></div>
