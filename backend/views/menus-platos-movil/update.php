<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\MenusPlatosMovil;
use backend\models\TblRestaurantes;
use yii\widgets\Breadcrumbs;


/* @var $this yii\web\View */
/* @var $model backend\models\MenusPlatosMovil */

$this->title = $model->tblrestaurantes[0]->nombre;

?>

<?php 
    echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
    'links' => [
            [
                'label' => 'Restaurantes',
                'url' => ['tbl-restaurantes/index'],
                'template' => "<li><b>{link}</b></li>\n", // template for this link only
            ],
            [
                'label' => $restaurante->nombre, 
                'url' => ['tbl-restaurantes/update', 'id' => $restaurante->id_restaurante]
            ],
            [
                'label' => 'Platos', 
                'url' => ['menus-platos-movil/create', 'id' => $restaurante->id_restaurante],
                'template' => "<li><b>{link}</b></li>\n", // template for this link only
            ],
            $model->nombre,
        ],
    ]);

 ?>
<div class="menus-platos-movil-update">

    <h1><?= Html::encode($model->nombre) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelCatalogo' => $modelCatalogo,
        'restaurante' => $restaurante,
    ]) ?>

</div>

<h1>Categorias de Extra del Plato</h1>
<div>
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
        
            'idcategoria_plato',
            /*[
                'attribute' => 'id_restaurante',
                'filter' => TblRestaurantes::listTblRestaurantes(),
                //'filter'=>ArrayHelper::map(TblRestaurantes::find()->asArray()->all(), 'id_restaurante', 'nombre'),
                'value' => function ($model) {
                    try {
                        return $model->tblrestaurantes[0]->nombre;
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],     
            [
                'attribute' => 'id_plato_movil',
                'filter' => MenusPlatosMovil::listMenusplatosmovil(),
                'value' => function ($model) {
                    try {
                        return ($model->menusplatosmovil[0]->nombre);
                    } catch (Exception $e) {
                          //echo "N/A";
                    }
                },
            ],*/
            'nombre',
            'orden',
            'multiple',
            'requerido',
            [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=categoria-plato/update&id='.$model->idcategoria_plato;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
</div>