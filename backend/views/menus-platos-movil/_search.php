<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MenusPlatosMovilSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menus-platos-movil-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_plato_movil') ?>

    <?= $form->field($model, 'id_restaurante') ?>

    <?= $form->field($model, 'id_categoria') ?>

    <?= $form->field($model, 'estatus') ?>

    <?= $form->field($model, 'orden') ?>

    <?php // echo $form->field($model, 'fotografia') ?>

    <?php // echo $form->field($model, 'nombre') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'precio1') ?>

    <?php // echo $form->field($model, 'idTipoPedido') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
