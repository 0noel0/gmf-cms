<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Motorista */

$this->title = 'Update Motorista: ' . $model->idmotorista;
$this->params['breadcrumbs'][] = ['label' => 'Motoristas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idmotorista, 'url' => ['view', 'id' => $model->idmotorista]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="motorista-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
