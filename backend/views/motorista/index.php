<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MotoristaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Motoristas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="motorista-index row">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-sm-6">
    <p>
      <?= Html::a('Agregar Motorista', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
    <div class="col-sm-6">
    <p class="text-right">
      <?= Html::a('Cerrar Sesión de Motoristas', ['cerrar-sesion'], ['class' => 'btn btn-danger', 'data' => [
                'confirm' => Yii::t('app', '¿Está seguro que desea cerrar la sesión de todos los Motoristas?'),
                'method' => 'post',
            ]]) ?>
    </p>
    </div>
</div>
<div class="motorista-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idmotorista',
            'nombre',
            'apellido',
            'email:email',
            //'password',
            // 'identificacion',
            'telefono',
            // 'direccion',
            // 'documento',
            // 'creacion',
            // 'tokenSesion',
            // 'tokePush',
            // 'latitud',
            // 'longitud',
            'disponible',
            // 'idtipousuario',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
