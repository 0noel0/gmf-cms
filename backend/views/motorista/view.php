<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Motorista */

$this->title = $model->nombre.' '.$model->apellido;
$this->params['breadcrumbs'][] = ['label' => 'Motoristas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="motorista-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->idmotorista], ['class' => 'btn btn-primary']) ?>
    </p>

<div>
    <h3> Total de Pedidos en el día:  <?php echo count($total->models); ?></h3>
</div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idmotorista',
            'nombre',
            'apellido',
            'email:email',
            //'password',
            'identificacion',
            'telefono',
            'direccion',
            'documento',
            'creacion',
            'tokenSesion',
            'tokePush',
            'latitud',
            'longitud',
            'disponible',
            'idtipousuario',
        ],
    ]) ?>

</div>

