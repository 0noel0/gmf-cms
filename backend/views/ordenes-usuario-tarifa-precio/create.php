<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\OrdenesUsuarioTarifaPrecio */

$this->title = 'Create Ordenes Usuario Tarifa Precio';
$this->params['breadcrumbs'][] = ['label' => 'Ordenes Usuario Tarifa Precios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ordenes-usuario-tarifa-precio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
