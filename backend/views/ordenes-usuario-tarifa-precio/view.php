<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenesUsuarioTarifaPrecio */

$this->title = $model->idordenusuario;
$this->params['breadcrumbs'][] = ['label' => 'Ordenes Usuario Tarifa Precios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ordenes-usuario-tarifa-precio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idordenusuario], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idordenusuario], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idordenusuario',
            'fechaInicio',
            'nombreUsuario',
            'nombre',
            'totalOrden',
            'tarifa',
        ],
    ]) ?>

</div>
