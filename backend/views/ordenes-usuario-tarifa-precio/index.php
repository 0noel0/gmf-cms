<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdenesUsuarioTarifaPrecioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detalle de Abono';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ordenes-usuario-tarifa-precio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= 
        ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'exportConfig' => [
                ExportMenu::FORMAT_TEXT => false,
                //ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_EXCEL_X => false,
		ExportMenu::FORMAT_PDF => false,
                ],
            'columns' =>[
                ['class' => 'yii\grid\SerialColumn'],
                     'idordenusuario',
                    //'fechaInicio',
                    [
                        'attribute' => 'fechaInicio',
                        'format' => ['date', 'php:d/m/Y']
                    ], 
                    [
                        'attribute' => 'nombreUsuario',
                        'value' => function ($model) {
                            try {
                                return htmlspecialchars(utf8_decode($model->nombreUsuario));
                            } catch (Exception $e) {
                                  return "";
                            }
                        },
                    ],
                    'nombre',
                    'nombreEstado',
                    [
                        'attribute' => 'totalOrden',
                        'value' => function ($model) {
                            try {
                                return "$".$model->totalOrden;
                            } catch (Exception $e) {
                                  return "";
                            }
                        },
                    ],
                    [
                        'attribute' => 'tarifa',
                        'value' => function ($model) {
                            try {
                                return "$".$model->tarifa;
                            } catch (Exception $e) {
                                  return "";
                            }
                        },
                    ],
                    [
                        'label' => 'Subtotal Comercio',
                        'value' => function ($model) {
                            try {
                                return "$".number_format(((number_format($model->totalOrden, 2, '.', ''))-($model->tarifa)), 2, '.', '');
                            } catch (Exception $e) {
                                  return "";
                            }
                        },
                    ],
            ],
            'disabledColumns'=>[],
            
        ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idordenusuario',
            //'fechaInicio',
            'ticket',
            [
                'attribute' => 'fechaInicio',
                'format' => ['date', 'php:d/m/Y']
            ], 
            [
                'attribute' => 'nombreUsuario',
                'value' => function ($model) {
                    try {
                        return htmlspecialchars(utf8_decode($model->nombreUsuario));
                    } catch (Exception $e) {
                          return "";
                    }
                },
            ],
            'nombre',
            'nombreEstado',
            [
                'attribute' => 'totalOrden',
                'value' => function ($model) {
                    try {
                        return "$".$model->totalOrden;
                    } catch (Exception $e) {
                          return "";
                    }
                },
            ],
            [
                'attribute' => 'tarifa',
                'value' => function ($model) {
                    try {
                        return "$".$model->tarifa;
                    } catch (Exception $e) {
                          return "";
                    }
                },
            ],
            [
                'label' => 'Subtotal Comercio',
                'value' => function ($model) {
                    try {
                        return "$".number_format(((number_format($model->totalOrden, 2, '.', ''))-($model->tarifa)), 2, '.', '');
                    } catch (Exception $e) {
                          return "";
                    }
                },
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
