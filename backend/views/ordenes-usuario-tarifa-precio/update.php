<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenesUsuarioTarifaPrecio */

$this->title = 'Update Ordenes Usuario Tarifa Precio: ' . $model->idordenusuario;
$this->params['breadcrumbs'][] = ['label' => 'Ordenes Usuario Tarifa Precios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idordenusuario, 'url' => ['view', 'id' => $model->idordenusuario]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ordenes-usuario-tarifa-precio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
