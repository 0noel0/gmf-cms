<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;


/* @var $this yii\web\View */
/* @var $model backend\models\OrdenesUsuarioTarifaPrecioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ordenes-usuario-tarifa-precio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model,'fecha_inicio')->widget(DatePicker::className(),[
        'clientOptions' => ['defaultDate' => '2018-01-01'],
        'dateFormat' => 'yyyy-MM-dd',
    ]);
    ?>
    <?= $form->field($model,'fecha_fin')->widget(DatePicker::className(),[
        'clientOptions' => ['defaultDate' => '2018-01-01'],
        'dateFormat' => 'yyyy-MM-dd',
    ]);
    ?>

    <?= $form->field($model, 'nombre')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
