<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HistorialpedidoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Historialpedidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historialpedido-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Historialpedido', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive'=>true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idhistorialpedido',
            'idordenUsuario',
            'idusuario',
            'idtipousuario',
            'idestadoanterior',
            // 'idestadonuevo',
            // 'fechacreacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
