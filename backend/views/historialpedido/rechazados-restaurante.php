<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HistorialpedidoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Historial de Órdenes';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
            
              <li class="active"><a href="#tab_6" data-toggle="tab" aria-expanded="false">Rechazados por Restaurante</a></li>
              
            </ul>
            <div class="tab-content">
            	               <div class="tab-pane active" id="tab_6">
          
              		<div class="historialpedido-index">
					     <?= GridView::widget([
					        'dataProvider' => $dataProvider8,
					        'filterModel' => $searchModel,
					        'responsive'=>true,
					        'columns' => [
					            //'idhistorialpedido',
					           'teleoperador',
					           'ticket',
					           'nombreEstado',
					           [
					                'attribute' => 'usuario',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->usuario));
					                    } catch (Exception $e) {
					                          return "";
					                    }
					                },
					            ],
					            [
					                'attribute' => 'direccion',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->direccion));
					                    } catch (Exception $e) {
					                          return "";
					                    }
					                },
					            ],
					           'cobertura',
					           'mobile',
					           'email',					            //'idordenUsuario',
					            //'idusuario',
					            //'idtipousuario',
					            //'idestadoanterior',
					            // 'idestadonuevo',
					           'fechacreacion',
					         	[
					                'class'    => 'yii\grid\ActionColumn',
					                'template' => '{leadDelete}',
					                'buttons'  => [
					                    'leadDelete' => function ($url, $model) {
					                        $url = ['historialpedido/detalle', 'id' => $model->idordenUsuario];
					                        return Html::a('<span class="fa fa-eye"></span>', $url, [
					                            'title'        => 'Detalle',
					                            'data-method'  => 'post',
					                        ]);
					                    },
					                ],
					            ],
					     ],
					    ]); ?>
					</div>
               
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

       
      </div>
