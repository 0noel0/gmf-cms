<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Historialpedido */

$this->title = $model->idhistorialpedido;
$this->params['breadcrumbs'][] = ['label' => 'Historialpedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historialpedido-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idhistorialpedido], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idhistorialpedido], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idhistorialpedido',
            'idordenUsuario',
            'idusuario',
            'idtipousuario',
            'idestadoanterior',
            'idestadonuevo',
            'fechacreacion',
        ],
    ]) ?>

</div>
