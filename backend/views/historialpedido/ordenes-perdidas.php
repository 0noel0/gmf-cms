<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HistorialpedidoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Historial de Órdenes';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
       
              <li class="active"><a href="#tab_5" data-toggle="tab" aria-expanded="false">Ordenes Perdidas</a></li>
            </ul>
            <div class="tab-content">
            	<div class="tab-pane active" id="tab_5">
          
              		<div class="historialpedido-index">
					     <?= GridView::widget([
					        'dataProvider' => $dataProvider13,
					        'responsive'=>true,
					        'columns' => [
					            //'idhistorialpedido',
					           'idordenUsuario',
					           'ticket',
					           [
					                'attribute' => 'idusuario_app',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->idusuarioApp->name.' '.$model->idusuarioApp->last_name));
					                    } catch (Exception $e) {
					                          echo "N/A";
					                    }
					                },
					            ],
					            [
					                'attribute' => 'estadoPedido',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->catalogoestados->nombre));
					                    } catch (Exception $e) {
					                          echo "N/A";
					                    }
					                },
					            ],
					            [
					                'attribute' => 'iddireccion',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->direccion->direccion));
					                    } catch (Exception $e) {
					                          echo "N/A";
					                    }
					                },
					            ],
					            [
					                'attribute' => 'totalOrden',
					                'value' => function ($model) {
					                    try {
					                        return ('$'.$model->totalOrden);
					                    } catch (Exception $e) {
					                          echo "N/A";
					                    }
					                },
					            ],
					           //'direccion',
					           //'cobertura',
					           //'mobile',
					           //'email',					            //'idordenUsuario',
					            //'idusuario',
					            //'idtipousuario',
					            //'idestadoanterior',
					            // 'idestadonuevo',
					            //'fechacreacion',
					             [
					                'class'    => 'yii\grid\ActionColumn',
					                'template' => '{leadDelete}',
					                'buttons'  => [
					                    'leadDelete' => function ($url, $model) {
					                        $url = ['orden-usuario/habilitar', 'id' => $model->idordenUsuario];
					                        return Html::a('<span class="fa fa-check-circle-o"></span>', $url, [
					                            'title'        => 'Habilitar',
					                            'data-confirm' => Yii::t('yii', '¿Está seguro que desea Habilitar la Orden?'),
					                            'data-method'  => 'post',
					                        ]);
					                    },
					                ],
					            ],
					        ],
					    ]); ?>
					</div>
               
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

       
      </div>
