<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Historialpedido */

$this->title = 'Update Historialpedido: ' . $model->idhistorialpedido;
$this->params['breadcrumbs'][] = ['label' => 'Historialpedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idhistorialpedido, 'url' => ['view', 'id' => $model->idhistorialpedido]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="historialpedido-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
