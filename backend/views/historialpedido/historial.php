<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HistorialpedidoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Historial de Órdenes';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Pendientes</a></li>
              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Aprobados por Restaurante</a></li>
              <li class=""><a href="#tab_6" data-toggle="tab" aria-expanded="false">Rechazados por Restaurante</a></li>
              <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Recibido por Motorista (En camino)</a></li>
              <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Entregados</a></li>
              <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">Ordenes Perdidas</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
              	<div class="historialpedido-index">
               		<?php Pjax::begin(); ?>
					    <?= GridView::widget([
					        'dataProvider' => $dataProvider,
					        'filterModel' => $searchModel,
					        'responsive'=>true,
					        'columns' => [
					            //'idhistorialpedido',
					           'motorista',
					           'teleoperador',
					           'ticket',
					           'nombreEstado',
					           'usuario',
					           'direccion',
					           'cobertura',
					           'mobile',
					           'email',					            
					            //'idordenUsuario',
					            //'idusuario',
					            //'idtipousuario',
					            //'idestadoanterior',
					            // 'idestadonuevo',
					            'fechacreacion',
					           	[
					                'class'    => 'yii\grid\ActionColumn',
					                'template' => '{leadDelete}',
					                'buttons'  => [
					                    'leadDelete' => function ($url, $model) {
					                        $url = ['historialpedido/detalle', 'id' => $model->idordenUsuario];
					                        return Html::a('<span class="fa fa-eye"></span>', $url, [
					                            'title'        => 'Detalle',
					                            'data-method'  => 'post',
					                        ]);
					                    },
					                ],
					            ],
					        ],
					    ]); ?>
					 <?php Pjax::end(); ?>
					</div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
              		<div class="historialpedido-index">
					      <?= GridView::widget([
					        'dataProvider' => $dataProvider7,
					        'filterModel' => $searchModel,
					        'responsive'=>true,
					        'columns' => [
					            //'idhistorialpedido',
					           'motorista',
							   'teleoperador',
					           'ticket',
					           'nombreEstado',
					           'usuario',
					           'direccion',
					           'cobertura',
					           'mobile',
					           'email',					            //'idordenUsuario',
					            //'idusuario',
					            //'idtipousuario',
					            //'idestadoanterior',
					            // 'idestadonuevo',
					            'fechacreacion',
					           [
					                'class'    => 'yii\grid\ActionColumn',
					                'template' => '{leadDelete}',
					                'buttons'  => [
					                    'leadDelete' => function ($url, $model) {
					                        $url = ['historialpedido/detalle', 'id' => $model->idordenUsuario];
					                        return Html::a('<span class="fa fa-eye"></span>', $url, [
					                            'title'        => 'Detalle',
					                            'data-method'  => 'post',
					                        ]);
					                    },
					                ],
					            ],
					        ],
					    ]); ?>
					</div>
             
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
            
              		<div class="historialpedido-index">
					     <?= GridView::widget([
					        'dataProvider' => $dataProvider11,
					        'filterModel' => $searchModel,
					        'responsive'=>true,
					        'columns' => [
					            //'idhistorialpedido',
					           'motorista',
					           'teleoperador',
					           'ticket',
					           'nombreEstado',
					           'usuario',
					           'direccion',
					           'cobertura',
					           'mobile',
					           'email',					            //'idordenUsuario',
					            //'idusuario',
					            //'idtipousuario',
					            //'idestadoanterior',
					            // 'idestadonuevo',
					            'fechacreacion',
		         				[
					                'class'    => 'yii\grid\ActionColumn',
					                'template' => '{leadDelete}',
					                'buttons'  => [
					                    'leadDelete' => function ($url, $model) {
					                        $url = ['historialpedido/detalle', 'id' => $model->idordenUsuario];
					                        return Html::a('<span class="fa fa-eye"></span>', $url, [
					                            'title'        => 'Detalle',
					                            'data-method'  => 'post',
					                        ]);
					                    },
					                ],
					            ],					       
					        ],
					    ]); ?>
					</div>
               
              </div>
              <!-- /.tab-pane -->
               <div class="tab-pane" id="tab_4">
          
              		<div class="historialpedido-index">
					     <?= GridView::widget([
					        'dataProvider' => $dataProvider12,
					        'filterModel' => $searchModel,
					        'responsive'=>true,
					        'columns' => [
					            //'idhistorialpedido',
					           'motorista',
					           'teleoperador',
					           'ticket',
					           'nombreEstado',
					           'usuario',
					           'direccion',
					           'cobertura',
					           'mobile',
					           'email',					            //'idordenUsuario',
					            //'idusuario',
					            //'idtipousuario',
					            //'idestadoanterior',
					            // 'idestadonuevo',
					            'fechacreacion',
					            [
					                'class'    => 'yii\grid\ActionColumn',
					                'template' => '{leadDelete}',
					                'buttons'  => [
					                    'leadDelete' => function ($url, $model) {
					                        $url = ['historialpedido/detalle', 'id' => $model->idordenUsuario];
					                        return Html::a('<span class="fa fa-eye"></span>', $url, [
					                            'title'        => 'Detalle',
					                            'data-method'  => 'post',
					                        ]);
					                    },
					                ],
					            ],

					        ],
					    ]); ?>
					</div>
               
              </div>
                  <!-- /.tab-pane -->
               <div class="tab-pane" id="tab_6">
          
              		<div class="historialpedido-index">
					     <?= GridView::widget([
					        'dataProvider' => $dataProvider8,
					        'filterModel' => $searchModel,
					        'responsive'=>true,
					        'columns' => [
					            //'idhistorialpedido',
					           'nombreRestaurante',
					           'zona',
					           'usuario',
					           'comentarios',
					           'telefono',
					           'fechaCreacion',
					           [
					                'class'    => 'yii\grid\ActionColumn',
					                'template' => '{leadDelete}',
					                'buttons'  => [
					                    'leadDelete' => function ($url, $model) {
					                        $url = ['historialpedido/detalle', 'id' => $model->idordenUsuario];
					                        return Html::a('<span class="fa fa-eye"></span>', $url, [
					                            'title'        => 'Detalle',
					                            'data-method'  => 'post',
					                        ]);
					                    },
					                ],
					            ],					        
					       ],
					    ]); ?>
					</div>
               
              </div>
              <div class="tab-pane" id="tab_5">
          
              		<div class="historialpedido-index">
					     <?= GridView::widget([
					        'dataProvider' => $dataProvider13,
					        'filterModel' => $searchModelPerdidas,
					        'responsive'=>true,
					        'columns' => [
					            //'idhistorialpedido',
					           'idordenUsuario',
					           'ticket',
					           [
					                'attribute' => 'idusuario_app',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->idusuarioApp->name.' '.$model->idusuarioApp->last_name));
					                    } catch (Exception $e) {
					                          echo "N/A";
					                    }
					                },
					            ],
					            [
					                'attribute' => 'estadoPedido',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->catalogoestados->nombre));
					                    } catch (Exception $e) {
					                          echo "N/A";
					                    }
					                },
					            ],
					            [
					                'attribute' => 'iddireccion',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->direccion->direccion));
					                    } catch (Exception $e) {
					                          echo "N/A";
					                    }
					                },
					            ],
					            [
					                'attribute' => 'totalOrden',
					                'value' => function ($model) {
					                    try {
					                        return ('$'.$model->totalOrden);
					                    } catch (Exception $e) {
					                          echo "N/A";
					                    }
					                },
					            ],
					           //'direccion',
					           //'cobertura',
					           //'mobile',
					           //'email',					            //'idordenUsuario',
					            //'idusuario',
					            //'idtipousuario',
					            //'idestadoanterior',
					            // 'idestadonuevo',
					            //'fechacreacion',
					             [
					                'class'    => 'yii\grid\ActionColumn',
					                'template' => '{leadDelete}',
					                'buttons'  => [
					                    'leadDelete' => function ($url, $model) {
					                        $url = ['orden-usuario/habilitar', 'id' => $model->idordenUsuario];
					                        return Html::a('<span class="fa fa-check-circle-o"></span>', $url, [
					                            'title'        => 'Habilitar',
					                            'data-confirm' => Yii::t('yii', '¿Está seguro que desea Habilitar la Orden?'),
					                            'data-method'  => 'post',
					                        ]);
					                    },
					                ],
					            ],
					        ],
					    ]); ?>
					</div>
               
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

       
      </div>
