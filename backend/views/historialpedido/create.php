<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Historialpedido */

$this->title = 'Create Historialpedido';
$this->params['breadcrumbs'][] = ['label' => 'Historialpedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historialpedido-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
