<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use demogorgorn\ajax\AjaxSubmitButton;
use yii\widgets\DetailView;
use backend\models\OrdenUsuario;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdenUsuario */

$this->title = 'Ticket: '.strtoupper ($model->ticket);
$this->params['breadcrumbs'][] = ['label' => 'Detalle del pedido', 'url' => ['despacho']];
$this->params['breadcrumbs'][] = $this->title;
?>




<h3>Detalle del Contenido de la Orden</h3>
<div class="orden-usuario-view" id="detalle" class="row">
    
</div>


<script type="text/javascript">
    $( document ).ready(function() {
        console.log( "ready!" );
    });

    function detallePedido() {
        console.log('before');
        var result = '';
        $.get( "http://desarrollo.gmf.technology/getMyFood/ws_v0.2/getDetallePedido?id=<?php echo $model->idordenUsuario; ?>")
          .done(function( data ) {
            var response = JSON.parse(data);
            $("#detalle").append('<div class="row"><div class="col-sm-12">Comentario General: '+response.data[0].descripcion+'</div></div>');
            for (var i = response.data[0].restaurantes.length - 1; i >= 0; i--) {
                $("#detalle").append('<div class="row">');
                $( "#detalle" ).append('<div class="col-sm-12" style="background-color:white;"><div>' + response.data[0].restaurantes[i].nombre + '</div>');
                $("#detalle").append('</div>');

                    //Imprimiendo la lista de Platos
                for (var j = response.data[0].restaurantes[i].platos.length - 1; j >= 0; j--) {
                    $("#detalle").append('<div class="row">');
                    $("#detalle").append('<div class="col-sm-1"></div>');
                    $("#detalle").append('<div class="col-sm-5" style="padding-left:20px;">'+response.data[0].restaurantes[i].platos[j].cantidad+' ' + response.data[0].restaurantes[i].platos[j].nombre + ' ('+response.data[0].restaurantes[i].platos[j].comentario+')</div>');
                    $("#detalle").append('<div class="col-sm-3">$'+response.data[0].restaurantes[i].platos[j].totalPorPlato+ '</div>');
                    $("#detalle").append('<div class="col-sm-3"></div>');
                    $("#detalle").append('</div>');
                    //Imprimiendo la lista de Extras
                    for (var k = response.data[0].restaurantes[i].platos[j].extras.length - 1; k >= 0; k--) {
                        $("#detalle").append('<div class="row">');
                        $("#detalle").append('<div class="col-sm-2"></div>');
                        $("#detalle").append('<div class="col-sm-3" style="padding-left:20px;">'+response.data[0].restaurantes[i].platos[j].extras[k].nombre + '</div>');
                        $("#detalle").append('<div class="col-sm-2">$'+response.data[0].restaurantes[i].platos[j].extras[k].precio+ '</div>');
                        $("#detalle").append('<div class="col-sm-5"></div>');
                        $("#detalle").append(' </div>');
                    }
                }                
            }

            
          });
    }

    detallePedido();

</script>


<div class="orden-usuario-view">
<h3>Datos del pedido</h3>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idordenUsuario',
            //'descripcion:ntext',
            [
                'attribute' => 'ticket',
                'value' => strtoupper($model->ticket),
            ],
            [
                'attribute' => 'idusuario_app',
                'value' => htmlspecialchars(utf8_decode($model->idusuarioApp->name.' '.$model->idusuarioApp->last_name)),
            ],
            [
                'attribute' => 'estadoPedido',
                'value' => $model->catalogoestados->nombre,
            ],
            [
                'attribute' => 'totalOrden',
                'value' => '$'.$model->totalOrden,
            ],
            [
                'attribute' => 'id',
                'label' => 'Dirección',
                'value' => htmlspecialchars(utf8_decode($model->direccion->direccion)),
            ],
            [
                'attribute' => 'estadoPedido',
                'label' => 'Referencia',
                'value' => $model->direccion->referencia,
            ],
            [
                'attribute' => 'idusuarioApp',
                'label' => 'Teléfono',
                'value' => $model->idusuarioApp->mobile,
            ],
            [
                'attribute' => 'totalOrden',
                'value' => '$'.$model->totalOrden,
            ],
            [
                'attribute' => 'cambio',
                'value' => '$'.$model->cambio,
            ],           
            //'idTipoPago',
            //'html',
            //7'token:ntext',
            //'id',
            //'estatus',
            //'respuestaRetorno:ntext',
            //'fechaFinal',
            //'fechaInicio',
        ],
    ]) ?>

</div>





