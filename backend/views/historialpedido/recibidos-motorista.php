<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HistorialpedidoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Historial de Órdenes';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
             
              <li class="active"><a href="#tab_3" data-toggle="tab" aria-expanded="false">Recibido por Motorista (En camino)</a></li>
             
            </ul>
            <div class="tab-content">
            	  <div class="tab-pane active" id="tab_3">
            
              		<div class="historialpedido-index">
					     <?= GridView::widget([
					        'dataProvider' => $dataProvider11,
					        'filterModel' => $searchModel,
					        'responsive'=>true,
					        'columns' => [
					            //'idhistorialpedido',
					           'motorista',
					           'teleoperador',
					           'ticket',
					           'nombreEstado',
					            [
					                'attribute' => 'usuario',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->usuario));
					                    } catch (Exception $e) {
					                          return "";
					                    }
					                },
					            ],
					            [
					                'attribute' => 'direccion',
					                'value' => function ($model) {
					                    try {
					                        return htmlspecialchars(utf8_decode($model->direccion));
					                    } catch (Exception $e) {
					                          return "";
					                    }
					                },
					            ],
					           'cobertura',
					           'mobile',
					           'email',					            //'idordenUsuario',
					            //'idusuario',
					            //'idtipousuario',
					            //'idestadoanterior',
					            // 'idestadonuevo',
					            'fechacreacion',
					       		[
					                  'class' => 'yii\grid\ActionColumn',
					                  'header' => 'Actions',
					                  'headerOptions' => ['style' => 'color:#337ab7'],
					                  'template' => '{view}',
					                  'buttons' => [
					                    'view' => function ($url, $model) {
					                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
					                                    'title' => Yii::t('app', 'Detalle'),
					                        ]);
					                    },

					                    'update' => function ($url, $model) {
					                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
					                                    'title' => Yii::t('app', 'lead-update'),
					                        ]);
					                    },
					                    'delete' => function ($url, $model) {
					                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
					                                    'title' => Yii::t('app', 'lead-delete'),
					                        ]);
					                    }

					                  ],
					                  'urlCreator' => function ($action, $model, $key, $index) {
					                    if ($action === 'view') {
					                        $url ='index.php?r=orden-usuario/detalle&id='.$model->idordenUsuario;
					                        return $url;
					                    }

					                    /*if ($action === 'update') {
					                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
					                        return $url;
					                    }
					                    if ($action === 'delete') {
					                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
					                        return $url;
					                    }*/

					                  }
					            ],
					        ],
					    ]); ?>
					</div>
               
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

       
      </div>
