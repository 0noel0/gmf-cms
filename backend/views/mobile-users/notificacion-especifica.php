<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use backend\models\MobileUsers;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MobileUsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mobile Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mobile-users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(); ?>
  
    <div style="display:flex;">
        <?= $form->field($model, 'fechaInicio')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'en-US',
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>

        <?= $form->field($model, 'fechaFinal')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'es-US',
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>
    </div>  
     <!--<div class="form-group">
        <?= Html::submitButton('Consultar', ['actionNotificacionEspecifica', 'fechaInicio'=> $model->fechaInicio, 'fechaFinal'=>$model->fechaFinal], ['name' => 'button1','class'=>'btn btn-primary']) ?>
    </div>-->


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buscar' : 'Buscar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'idusuario_app',
                'idordenUsuario',
                /*[
                   'attribute' => 'idordenUsuario',
                   'filter' => MobileUsers::listMobileUsers(),
                   'value' => function ($model) {
                       try {
                             return $model->mobileusers;
                       } catch (Exception $e) {
                            echo $e;
                            echo "N/A";
                       }
                   },
                ],*/
                // 'idcountry',
                // 'phone',
                // 'mobile',
                // 'creation',
                // 'pin_cp',
                // 'tokenSesion',
                // 'tokenPush:ntext',
                // 'primerRegistro',
                

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
</div>
