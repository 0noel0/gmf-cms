<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MobileUsers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Mobile Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mobile-users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idmobileusr], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idmobileusr], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idmobileusr',
            [
                'attribute' => 'name',
                'value' => htmlspecialchars(utf8_decode($model->name)),
            ],
            [
                'attribute' => 'last_name',
                'value' => htmlspecialchars(utf8_decode($model->last_name)),
            ],      
            'email:email',
            'password',
            'idcountry',
            'phone',
            'mobile',
            'creation',
            'pin_cp',
            'tokenSesion',
            'tokenPush:ntext',
            'primerRegistro',
        ],
    ]) ?>

</div>
