<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NotificacionGeneral */

$this->title = 'Update Notificacion General: ' . $model->idnotificacion_general;
$this->params['breadcrumbs'][] = ['label' => 'Notificacion Generals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idnotificacion_general, 'url' => ['view', 'id' => $model->idnotificacion_general]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notificacion-general-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
