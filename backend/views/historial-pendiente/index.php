<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HistorialPendienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Despacho';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historial-pendiente-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idhistorialpedido',
            'ticket',
            'sucursal',
            'usuario',
            'mobile',
            'direccion:ntext',
            //'cobertura',
            // 'email:email',
            // 'idusuario',
            // 'idestadonuevo',
            // 'fechacreacion',
            // 'idordenUsuario',
            // 'nombreEstado',
            // 'nombreRestaurante',

            [
                  'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('Asignar', ['/historial-pendiente'], ['class'=>'btn btn-primary']);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                  ],
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=tbl-restaurantes/update&id='.$model->idhistorialpedido;
                        return $url;
                    }

                    /*if ($action === 'update') {
                        $url ='index.php?r=menus-platos-movil/update&id='.$model->id_plato_movil;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=menus-platos-movil/view&id='.$model->id_plato_movil;
                        return $url;
                    }*/

                  }
              ],
        ],
    ]); ?>
</div>
