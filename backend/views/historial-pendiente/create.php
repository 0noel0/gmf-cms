<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\HistorialPendiente */

$this->title = 'Create Historial Pendiente';
$this->params['breadcrumbs'][] = ['label' => 'Historial Pendientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historial-pendiente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
