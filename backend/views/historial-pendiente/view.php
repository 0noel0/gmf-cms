<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\HistorialPendiente */

$this->title = $model->idhistorialpedido;
$this->params['breadcrumbs'][] = ['label' => 'Historial Pendientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historial-pendiente-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idhistorialpedido], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idhistorialpedido], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idhistorialpedido',
            'ticket',
            'usuario',
            'direccion:ntext',
            'cobertura',
            'mobile',
            'email:email',
            'idusuario',
            'idestadonuevo',
            'fechacreacion',
            'idordenUsuario',
            'nombreEstado',
            'sucursal',
            'nombreRestaurante',
        ],
    ]) ?>

</div>
