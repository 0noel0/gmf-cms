<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\HistorialPendiente */

$this->title = 'Update Historial Pendiente: ' . $model->idhistorialpedido;
$this->params['breadcrumbs'][] = ['label' => 'Historial Pendientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idhistorialpedido, 'url' => ['view', 'id' => $model->idhistorialpedido]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="historial-pendiente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
