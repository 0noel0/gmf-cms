<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\HistorialPendiente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="historial-pendiente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idhistorialpedido')->textInput() ?>

    <?= $form->field($model, 'ticket')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direccion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cobertura')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idusuario')->textInput() ?>

    <?= $form->field($model, 'idestadonuevo')->textInput() ?>

    <?= $form->field($model, 'fechacreacion')->textInput() ?>

    <?= $form->field($model, 'idordenUsuario')->textInput() ?>

    <?= $form->field($model, 'nombreEstado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sucursal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombreRestaurante')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
