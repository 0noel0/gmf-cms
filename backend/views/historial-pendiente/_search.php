<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\HistorialPendienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="historial-pendiente-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idhistorialpedido') ?>

    <?= $form->field($model, 'ticket') ?>

    <?= $form->field($model, 'usuario') ?>

    <?= $form->field($model, 'direccion') ?>

    <?= $form->field($model, 'cobertura') ?>

    <?php // echo $form->field($model, 'mobile') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'idusuario') ?>

    <?php // echo $form->field($model, 'idestadonuevo') ?>

    <?php // echo $form->field($model, 'fechacreacion') ?>

    <?php // echo $form->field($model, 'idordenUsuario') ?>

    <?php // echo $form->field($model, 'nombreEstado') ?>

    <?php // echo $form->field($model, 'sucursal') ?>

    <?php // echo $form->field($model, 'nombreRestaurante') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
